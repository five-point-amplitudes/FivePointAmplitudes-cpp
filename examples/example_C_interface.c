/**
 * A simple example of C interface.
 *
 * Assuming that libFivePointAmplitudes was installed with prefix $HOME/local,
 * this example can be compiled simply with
 *
 * gcc -L"$HOME/local/lib64" -lFivePointAmplitudes -I"HOME/local/include" -o example_C_interface example_C_interface.c
 *
 * on some systems library path might differ, e.g. lib64 etc.
 *
 * For details of the input/output arguments see cInterface.h
 *
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <FivePointAmplitudes/ME2/C_interface.h>
#include <string.h>



int readFloatingPointFile(const char *filename, double numbers[]) {
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        printf("Error opening the file.\n");
        return 1;
    }

    int number_count = 0;
    char line[2000];

    while (fgets(line, sizeof(line), file)) {
        char *token = strtok(line, " ");

        while (token != NULL) {
            numbers[number_count] = atof(token);
            token = strtok(NULL, " ");
            number_count++;
        }
    }

    fclose(file);
    return 0; // Success
}

void swap_momenta(double* momenta, int m1, int m2) {
    for (int i = 0; i < 4; ++i) {
        double tmp;
        tmp = momenta[4 * m2 + i];
        momenta[4 * m2 + i] = momenta[4*m1 +i];
        momenta[4 * m1 + i] = tmp;
    }
}

void permute_momenta(double* momenta, int n_particles, int permutation[]) {
    double* momenta_copy = (double *)malloc(sizeof(double) * n_particles * 4);
    if (momenta_copy == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    for (int i = 0; i < n_particles*4; ++i) {
        momenta_copy[i] = momenta[i];
    }

    for (int pi = 0; pi < n_particles; ++pi) {
        for (int i = 0; i < 4; ++i) {
            momenta[4 * (permutation[pi]-1) + i] = momenta_copy[4*pi +i];
        }
    }

    free(momenta_copy);
}

int main(void) {

    int particles_pdg[] = {2,-1, -5, 5, -11, 12};
    int n_particles = 6;

    double input_parameters[] = {
        0,        /* enable complex mass scheme */
        80.419,   /*M_W*/  
        2.06,     /*G_W*/  
        91.188,   /*M_Z*/ 
        2.49,     /*G_Z*/ 
    };


    /*const char* filename = "/home/ttp/vsotni/workspace/Vjj-LC-sq/misc/LL_test_point.txt";*/

    const char* filename = "/home/ttp/vsotni/workspace/Vjj-LC-sq/misc/LL_test_point2.txt";
    double mu = 3868.56951320012;

    /*double mu = 1;*/

    int permutation[] = {1,2,3,4,5,6};




    double *momenta = (double *)malloc(sizeof(double) * n_particles * 4);
    if (momenta == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    {
        int result = readFloatingPointFile(filename, momenta);

        if (result != 0) {
            free(momenta); // Free allocated memory in case of an error
            return result; // Handle the error
        }

        permute_momenta(momenta, n_particles, permutation);
    }
    printf("PS point:\n");
    for (int i =0; i < n_particles; ++i) {
        for (int j =0; j < 4; ++j) {
            printf("%.16e ", momenta[4*i + j]);
        }
        printf("\n");
    }
    printf("mu = %.16e\n", mu);

    // {include 1-loop^2, 0-Catani, 1-MS}
    int int_options[] = {0,1};
    double* double_options = NULL;
    int use_rescue = 1;

    int pid = initialize_process(n_particles, particles_pdg, use_rescue, input_parameters, int_options, double_options);

    if(pid==0) {
        printf("error in initializion!\n");
        exit(1);
    }

    printf("initialization finished\n");



    double result[6];
    // during first evaluation the construction phase will be perfored, so might take longer
    int error_code = evaluate_process(pid, momenta,mu,result);

    // check the returned error code to make sure evaluation went fine
    if (error_code != 0) {
        printf("There was an error in evaluation, code %i\n", error_code);
    }

    /*// in subsequent calls construction phase will not be repeated*/
    /*error_code = h2__g_g__g_g_g__eval(sij,musq,result);*/

    printf("H(0)    : %.16e\n", result[0]);

    printf("H(1)[0] : %.16e\n", result[1]);
    printf("H(1)[1] : %.16e\n", result[2]);

    printf("H(2)[0] : %.16e\n", result[3]);
    printf("H(2)[1] : %.16e\n", result[4]);
    printf("H(2)[2] : %.16e\n", result[5]);

    printf("\n\nH(1) : %.16e\n", 3/2. * ( result[1] + 4/3.*result[2])/* - M_PI*M_PI/2*/);
    printf("H(2) : %.16e\n", 9/4. * ( result[3] + 4/3.*result[4] + 16/9.*result[5]));

    free(momenta);

    return 0;
}
