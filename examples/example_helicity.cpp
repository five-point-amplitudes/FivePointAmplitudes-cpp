#include <iostream>
#include <iomanip>
#include <complex>

#include "Spinor.h"
#include "remainders/2q3y/RemaindersList.h"

/**
 * Here we demonstrate how to use the library to evaluate helicity finite remainders individually.
 */

int main () {

    using namespace FivePointAmplitudes;

    // Perform evaluations in double precision in this program.
    // Can be switched to dd_real if the support of higher precision is compiled
    using T = double;

    // The construction phase is performed once

    remainder_2q3y_1l__A_1__pmmpp<T> r1_mpp{{1,2,3,4,5}};     // R-++(1)
    remainder_2q3y_1l__A_1__pmppp<T> r1_ppp{{1,2,3,4,5}};     // R+++(1)

    remainder_2q3y_2l__A_2_0__pmmpp<T> r_mpp{{1,2,3,4,5}};     // R-++(2,0)
    remainder_2q3y_2l__A_2_0__pmppp<T> r_ppp{{1,2,3,4,5}};     // R+++(2,0)
    remainder_2q3y_2l__A_2_nf__pmmpp<T> r_mpp_nf{{1,2,3,4,5}};  // R-++(2,Nf)
    remainder_2q3y_2l__A_2_nf__pmppp<T> r_ppp_nf{{1,2,3,4,5}};  // R+++(2,Nf)

    // Now evaluate

    // Define kinematics by the values of {s12, s23, s34, s45, s15}

    mom_conf<T> mc =  {{{stof<T>("-0.574999999999999955591079014993738383054733276367187500000000000"),stof<T>("0"),stof<T>("0"),stof<T>("0.574999999999999955591079014993738383054733276367187500000000000")},{stof<T>("-0.574999999999999955591079014993738383054733276367187500000000000"),stof<T>("0"),stof<T>("0"),stof<T>("-0.574999999999999955591079014993738383054733276367187500000000000")},{stof<T>("0.45885823968105455223951504464562806958693650420058276367177156"),stof<T>("0.173625421322128045569993366802796604718063159535558972787634203"),stof<T>("0.126119250441211062637070691054180922310832158655938882592005931"),stof<T>("-0.40558480230656660231509115949703756078242481151848338445516792")},{stof<T>("0.231129408692477966544213885815185964305533863851683941727478329"),stof<T>("0.167601601155691461572984001585952871123560045143595291456989481"),stof<T>("-0.126119250441211062637070691054180922310832158655938882592005931"),stof<T>("0.097079562837551525076087163267678495560188479532638727860796988")},{stof<T>("0.46001235162646739239842909952666273221699618468210829460075011"),stof<T>("-0.341227022477819507142977368388749475841623204679154264244623685"),stof<T>("0"),stof<T>("0.308505239469015077239003996229359065222236331985844656594370931")}}};
        ;
    
    mc = lorentz_boost(mc, {0.1,0.2,0.07}).parity_conjugate();

    std::cout << std::scientific << std::setprecision(17);

    // Evaluate R-++(2,0)
    // The first argument is kinematics, the second argument is the sign of Im[tr5], and the last is the renormalization scale.
    // The scale can be set to anything.
    auto r1_mpp_val = r1_mpp.eval(mc,1);
    auto r1_ppp_val = r1_ppp.eval(mc,1);

    auto r_mpp_val = r_mpp.eval(mc,1);
    auto r_ppp_val = r_ppp.eval(mc,1);
    auto r_mpp_nf_val = r_mpp_nf.eval(mc,1);
    auto r_ppp_nf_val = r_ppp_nf.eval(mc,1);

    std::cout << "R-++(1) : " << r1_mpp_val << std::endl;
    std::cout << "R-++(2,0) : " << r_mpp_val << std::endl;
    std::cout << "R-++(2,Nf) : " << r_mpp_nf_val << std::endl;

    std::cout << "\n";

    std::cout << "R+++(1) : " << r1_ppp_val << std::endl;
    std::cout << "R+++(2,0) : " << r_ppp_val << std::endl;
    std::cout << "R+++(2,Nf) : " << r_ppp_nf_val << std::endl;

    return 0;
}
