#include <FivePointAmplitudes_config.h>
#include <iostream>
#include <iomanip>
#include <complex>

#ifdef FivePointAmplitudes_2q3y_ENABLED 
#include "ME2/h2__q_qb__y_y_y.hpp"
#endif // FivePointAmplitudes_2q3y_ENABLED

#ifdef FivePointAmplitudes_3j_ENABLED 
#include "ME2/H2Evaluator.h"
#endif // FivePointAmplitudes_3j_ENABLED



/**
 * Here we demonstrate how to use the library to evaluate the squared helicity/color summed finite remainder for the qq->yyy process.
 */

int main () {

    using namespace FivePointAmplitudes;

    // Perform evaluations in double precision in this program.
    // Can be switched to dd_real if the support of higher precision is compiled
    using T = double;


    /********
     * First we initialize (construct) the needed process. 
     ****/
    // This only has to be done once.
    // Depending on the process and the filesystem speed this can take some time.

    // Construct double-precision evaluator for H2 of ( qbar q -> q qbar g ) process
    h2__q_qb__y_y_y<T> H2{};

    // For all pp->3j functions we use the same class H2EvaluatorM0, e.g. 
    // for g g -> g g g  in full color
    //jjj::H2EvaluatorM0<T> H2{"g_g__g_g_g"};
    // for g g -> g g g  in leading color
    //jjj::H2EvaluatorM0<T> H2{"u_ub__g_g_g.lc"};

     


    /********
     * Now evaluate with any amount of phase-space points
     ****/

    // Define momenta
    mom_conf<T> mc =  {{{stof<T>("-3.603374986905501290538872256673345218282719141305325437829291691181672"),stof<T>("3.554959493321561468672129534150575066853012966645589099553430374698111"),stof<T>("0.03393756079543256772002032968162840159286885887012495560432869092790560"),stof<T>("0.5877265852982872128950076236306106505973258220762968583516232590979646")},{stof<T>("-3.577999106716025929337894283034800533646925344535569624886831890539547"),stof<T>("3.533369706271889426801051102216240174745387597706201315707007271335318"),stof<T>("0.03373145304316839548258760002115742410258126584922387890889744411775960"),stof<T>("-0.5623507051088118516940296499920659659615320253065410454091634584558402")},{stof<T>("1.796761945554363909813723593896521677103707273945760955250422021565820"),stof<T>("-1.745455182012848177596863096864259957496535668186029775411048047797068"),stof<T>("0.1077986718890880404806542047073347699126679671757008205599657768147102"),stof<T>("-0.4124550192636122576236453066127384937809766431776234007185260334318605")},{stof<T>("0.4155498351615072153676410600858981941606521159525364621486918761256585"),stof<T>("-0.3825927984080759624563628349146754344957881731447293881108541642343746"),stof<T>("-0.1313717029430140023177932675505093109169082779001059083685070657461859"),stof<T>("0.09510989314937564280799369238223982537155300391227580669832895066746395")},{stof<T>("4.969062312905656094695401885725725880665285095942597645317009684029740"),stof<T>("-4.960281219172526755419954704587879849606076723021031251738535434001987"),stof<T>("-0.04409598278467500136546886685961128469120981399494374670468484611418951"),stof<T>("0.2919692459247612536146736405919539837736298424955917810777372821222721")}}};


    std::cout << std::scientific << std::setprecision(17);

    // Evaluate H2, the second argument is the renormalization scale. Here we set it to sqrt(s12) as an example.
    std::vector<std::vector<T>> h2_val = H2(mc, sqrt(mc.s(1,2)));

    // The output is a vector of vectors of numbers. It is gauge-invaraint components of H2 as defined in the respective publications.
    // The outer vector is orders in alpha_s, starting from the leading order.
    // The meaning of the inner vector depends is process dependend.
    // It is left to the user to combine it with appropriate color factors.


    // For the uub -> yyy we have:
    std::cout << "H(1)[0] : " << h2_val[1][0] << std::endl;

    std::cout << "H(2)[0] : " << h2_val[2][0] << std::endl;
    std::cout << "H(2)[1] : " << h2_val[2][1] << std::endl;
    std::cout << "H(2)[Nf] : " << h2_val[2][2] << std::endl;
    std::cout << "H(2)[Nfyy] : " << h2_val[2][3] << std::endl;

    return 0;
}
