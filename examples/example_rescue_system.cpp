#include "CheckedEvaluator.h"

#include "ME2/h2__g_g__g_g_g.hpp"
#include <iomanip>

template <typename T> void print_H2(const std::vector<std::vector<T>>& result) {
    std::cout << "H(0) : " << result.at(0).at(0) << "\n";
    std::cout << "H(1)[0] : " << result.at(1).at(0) << "\n";
    if (result.at(1).size() > 1){
        std::cout << "H(1)[1] : " << result.at(1).at(1) << "\n";
    }
    std::cout << "H(2)[0] : " << result.at(2).at(0) << "\n";
    std::cout << "H(2)[1] : " << result.at(2).at(1) << "\n";
    if (result.at(2).size() > 2){
        std::cout << "H(2)[2] : " << result.at(2).at(2) << "\n";
    }
}

int main() {
    using namespace FivePointAmplitudes;
    using std::cout;

    const std::array<double, 5> v_stable = {
        3.999999999999999999999999999999999999999999999999999999999999996, -3.006758406913465117628015516362971428567038003147465273035685144,
        0.7999140756419657187720358988637915024483807430466958155350213772, 0.8079426804796202596280322421322752940461770937438510890558148344,
        -2.673104664679939231277495704234996849995206612285217042955531827};

    double musq_stable = 2.;

    const std::array<double, 5> v_barely_stable = {348854.64034707111, -166818.20758405351, 168278.64314939897, 13696.819951702666, -56094.489902916655};
    auto musq_barely_stable = 80790.507048444833;

    const std::array<double, 5> v_unstable = {37131681.290471099, -6930397.4041646505, 176797.10118507687, 2034663.3069283664, -6931379.6715752771};
    auto musq_unstable = 5661779.9775189465;

    cout << "constructing...\n";

    CheckedEvaluator<h2__g_g__g_g_g> h2_checked;

    h2__g_g__g_g_g<double> h2_plain;

    cout << std::scientific << std::setprecision(17);

    // first evaluate on a stable point
    auto result = h2_checked(v_stable, musq_stable);

    cout << "Result without check on unstable point:\n";
    result = h2_plain(v_unstable, musq_unstable);

    print_H2(result);

    result = h2_checked(v_unstable, musq_unstable);
    cout << "Result WITH check on unstable point:\n";
    print_H2(result);

    cout << "estimated accuracy of double precision for the unstable point: " << h2_checked.last_accuracy_estimate << "\n";

    // also evaluted on a point which is a candidate for instability (fails letters check), but deemed to be sufficiently good after the scaling test
    h2_checked(v_barely_stable, musq_barely_stable);

    cout << "estimated accuracy of double precision for the barely stable point: " << h2_checked.last_accuracy_estimate << "\n";

    cout << "\nCheck statistics of the rescued points:\n";

    cout << "total point evaluated: " << h2_checked.n_evaluated_points << "\n";
    cout << "point failed the letters check: " << h2_checked.n_failed_letters_threshold << "\n";
    cout << "point failed the accuracy check (and evaluate in quad pecision): " << h2_checked.n_failed_rescue_threshold << "\n";
}
