#pragma once

#include "FunctionsManager.h"
#include "Kinematics.h"
#include "Timing.h"

namespace FivePointAmplitudes {

namespace detail {

template <typename T> std::array<T, 26> get_inverse_pentagon_letters(const std::array<T, 5>& v, const T& ImSqrtDelta) {
    std::array<T, 26> result = {v[0],
                                v[1],
                                v[2],
                                v[3],
                                v[4],
                                v[2] + v[3],
                                v[3] + v[4],
                                v[0] + v[4],
                                v[0] + v[1],
                                v[1] + v[2],
                                v[0] - v[3],
                                v[1] - v[4],
                                -v[0] + v[2],
                                -v[1] + v[3],
                                -v[2] + v[4],
                                v[0] + v[1] - v[3],
                                v[1] + v[2] - v[4],
                                -v[0] + v[2] + v[3],
                                -v[1] + v[3] + v[4],
                                v[0] - v[2] + v[4],
                                -v[0] - v[1] + v[2] + v[3],
                                -v[1] - v[2] + v[3] + v[4],
                                v[0] - v[2] - v[3] + v[4],
                                v[0] + v[1] - v[3] - v[4],
                                -v[0] + v[1] + v[2] - v[4],
                                ImSqrtDelta};

    for (auto& c : result) { c = 1 / c; }

    // we square and add minus, which if from (I)^2
    result[result.size() - 1] *= (-result[result.size() - 1]);

    return result;
}

template <size_t N> inline int permutation_signature(const std::array<int,N>& p) {
    if (N == 1) return 1;
    size_t count = 0;

    for (size_t i = 0; i < N; ++i)
        for (size_t j = i + 1; j < N; ++j)
            if (p[i] > p[j]) ++count;
    return 1 - 2*int(count % 2);
}

inline int permutation_signature(const std::vector<int>& p) {
    const size_t N = p.size();
    if (N == 1) return 1;
    size_t count = 0;

    for (size_t i = 0; i < N; ++i)
        for (size_t j = i + 1; j < N; ++j)
            if (p[i] > p[j]) ++count;
    return 1 - 2*int(count % 2);
}



} // namespace detail

template <size_t NLETTERS, typename T, typename Tm, typename Tc_basis, size_t NCOEFFS, typename Tf_basis, size_t NFUNCS>
std::complex<T> eval_remainder(std::array<T, 5> v_in, int str5, T musq, const std::array<Tc_basis, NCOEFFS>& coeff_basis, const std::array<Tf_basis,NFUNCS>& functions_basis,
                               const Tm& coefficients, std::shared_ptr<FunctionsManager> f_manager, T tr5norm, const std::array<int,5>& permutation) {
    using TC = std::complex<T>;
    using std::array;

    // if we are evauating on a permuted, we adjust the sign of tr5
    str5 *= detail::permutation_signature(permutation);

    // The coefficients of all functions in all remainders are by construction dimensionless,
    // so for better numerical stability we rescale them.
    // We could choose the scale ourselves here, but we can also assume that the renormalization
    // scale has been chosen naturally, so we use it instead.
    for (auto& v : v_in) { v /= musq; }

    const std::array<T,5> permuted_v = sij5<T>{v_in}.permute(permutation);

    // TODO: 
    // here hard-coded normalziation of tr5 by s12^2 permuted according to permutation,
    // change to allow more flexible
    tr5norm = permuted_v[0];
    tr5norm *= tr5norm;

    Kin<T> kin{v_in};

#ifdef TIMING_ON
    ::timing::_rational_functions_timing.start();
#endif // TIMING_ON
    array<T, NLETTERS> iW = detail::get_inverse_pentagon_letters(permuted_v, kin.SqrtDelta.imag());

    array<T, NCOEFFS> coeffs;

    for (size_t i = 0; i < NCOEFFS; ++i) { coeffs[i] = coeff_basis[i](permuted_v, iW); }

#ifdef TIMING_ON
    ::timing::_rational_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON
    ::timing::_transcendental_functions_timing.start();
#endif // TIMING_ON
    const auto& function_values = f_manager->eval(kin);
#ifdef TIMING_ON
    ::timing::_transcendental_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON
    ::timing::_summations_timing.start();
#endif // TIMING_ON
    array<TC, NFUNCS> functions_basis_values;

    for (size_t i = 0; i < NFUNCS; ++i) {
        TC c{1};
        for (const auto& el : functions_basis[i]) {
            switch (el.first) {
                case f_type::f: c *= function_values.at(el.second); break;
                case f_type::bc: c *= bc<T>.at(el.second); break;
                case f_type::bci: c *= TC(0, bc<T>.at(el.second)); break;
                case f_type::str5: c *= str5; break;
                case f_type::isqrt: // we don't use this here
                case f_type::one: break;
            }
        }
        functions_basis_values[i] = std::move(c);
    }


    // Neumaier summation (for improved numerical stability) [doi:10.1002/zamm.19740540106]
    auto sum_coeff = [&coeffs](const auto& values) {
        using std::get;
        
        // trivial cases 
        switch (values.size()) {
            case 0: return T(0);
            case 1: return coeffs[get<int>(values[0])] * get<T>(values[0]);
            case 2: return coeffs[get<int>(values[0])] * get<T>(values[0]) + coeffs[get<int>(values[1])] * get<T>(values[1]);
            default: break;
        }

        T sum{0};
        T c{0};

        for (const auto& it : values) {
            using std::abs;
            using std::get;

            auto si = (coeffs[get<int>(it)] * get<T>(it));

            auto t = sum + si;

            if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
            else {
                c += ((si - t) + sum);
            }
            sum = t;
        }
        sum += c;
        return sum;
    };

    TC sum{0};
    TC c{0};

    for (size_t i = 0; i < NFUNCS; ++i){
        using std::abs;

        T re = sum_coeff(coefficients[i].first);
        T im = sum_coeff(coefficients[i].second);

        im *= str5;
        im *= kin.SqrtDelta.imag();
        im /= tr5norm;
        TC si{re, im};

        si *= functions_basis_values[i];

        auto t = sum + si;
        if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
        else {
            c += ((si - t) + sum);
        }
        sum = t;
    }
    sum += c;

#ifdef TIMING_ON
    ::timing::_summations_timing.stop();
#endif // TIMING_ON

    return sum;
}

template <size_t NLETTERS, typename T, typename Tm, typename Tc_basis, size_t NCOEFFS, typename Tf_basis, size_t NFUNCS>
std::complex<T> eval_remainder(std::array<T, 5> v_in, int str5, T musq, const std::array<Tc_basis, NCOEFFS>& coeff_basis, const std::array<Tf_basis,NFUNCS>& functions_basis,
                               const Tm& coefficients, std::shared_ptr<FunctionsManager> f_manager, T tr5norm) {
    constexpr static const std::array<int,5> std_permutation = {1,2,3,4,5};
    return eval_remainder<NLETTERS,T,Tm,Tc_basis,NCOEFFS,Tf_basis,NFUNCS>(v_in, str5, musq, coeff_basis, functions_basis, coefficients, f_manager, tr5norm, std_permutation);
}

template <size_t NLETTERS, typename T, typename Tm, typename Tc_basis, size_t NCOEFFS, typename Tf_basis, size_t NFUNCS>
std::complex<T> eval_remainder(std::array<T, 5> v_in, int str5, T musq, const std::array<Tc_basis, NCOEFFS>& coeff_basis,
                               const std::array<Tf_basis, NFUNCS>& functions_basis, const Tm& coefficients, std::shared_ptr<FunctionsManager> f_manager) {
    return eval_remainder(v_in, str5, musq, coeff_basis, functions_basis, coefficients, f_manager, musq);
}

} // namespace FivePointAmplitudes

