#pragma once

#include "FivePointAmplitudes_config.h"

#ifdef __cplusplus
namespace FivePointAmplitudes {
#endif

struct EW_Input_Parameters {
    int enable_complex_mass_scheme; /// if set to 1, will use complex masses in computation of the Weinberg angle

    double M_W;
    double G_W;

    double M_Z;
    double G_Z;
};

#ifdef __cplusplus
} // namespace FivePointAmplitudes

extern "C" {
typedef ::FivePointAmplitudes::EW_Input_Parameters EW_Input_Parameters;
}
#else
typedef struct EW_Input_Parameters EW_Input_Parameters;
#endif

