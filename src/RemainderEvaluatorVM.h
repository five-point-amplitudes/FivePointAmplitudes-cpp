#pragma once

#include "FivePointAmplitudes_config.h"

#include "FunctionsManager.h"
#include "Kin.h"
#include "Kinematics.h"
#include "MathList.h"
#include "PrecisionCast.h"
#include "Timing.h"

#include "Spinor.h"

#include "Utilities.h"
#include "VirtualMachine.h"

#ifdef FivePointAmplitudes_QD_ENABLED
#include "qd/qd_real.h"
#endif // FivePointAmplitudes_QD

#include <limits>
#include <memory>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <functional>
#include <filesystem>

//#include <dbg.h>

namespace FivePointAmplitudes {


template <typename T> using VariableEvaluatorType = std::vector<T>(*) (const mom_conf<T>&);
template <typename T> using VariableEvaluatorTypeSpinor = std::vector<add_complex_t<T>>(*) (const mom_conf<T>&);
template <typename T> using InverseDenominatorsEvaluatorType = std::vector<T>(*) (const std::vector<T>&);

namespace detail {

template <KinType KT, typename T> auto get_sijs(const mom_conf<T>& mc) {
    if constexpr (KT == KinType::m0) { return std::array{mc.s(1, 2), mc.s(2, 3), mc.s(3, 4), mc.s(4, 5), mc.s(1, 5)}; }
    else if constexpr (KT == KinType::m1) {
        return std::array{mc.s(1), mc.s(1, 2), mc.s(2, 3), mc.s(3, 4), mc.s(4, 5), mc.s(1, 5)};
    }
}

template <KinType KT, typename T> T get_bc(int ind) {
    // special constants
    if (ind >= 10000) {
        switch (ind) {
            case 10000: return ::constants::pi<T>;
            case 10001: return ::constants::zeta3<T>;
        }
        throw std::runtime_error("Unknown constant");
    }

    if constexpr (false) {}
#ifdef PENTAGON_FUNCTIONS_M0_ENABLED
    else if constexpr (KT == KinType::m0) {
        return ::PentagonFunctions::bc<T>.at(ind);
    }
#endif
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED
    else if constexpr (KT == KinType::m1) {
        return ::PentagonFunctions::m1_set::bc<T>.at(ind);
    }
#endif // PENTAGON_FUNCTIONS_M1_ENABLED
}


template <typename T> remove_complex_t<T> norm1 (const T& x) {
    using std::fabs;
    if constexpr (is_complex_v<T>) return std::max(fabs(x.real()), fabs(x.imag()));
    else {
        return fabs(x);
    }
}

} // namespace detail

struct VMCoefficientID {
    //std::string path;
    std::string name;
    std::vector<int> indices;

    std::string get_file_name() const {
        //std::string fname = path+"/"+name;
        std::string fname = name;

        for (auto ii : indices) {
            fname += "_";
            fname += std::to_string(ii);
        }
        return fname;
    };
};

inline bool operator==(const VMCoefficientID& vmi1, const VMCoefficientID& vmi2) { return vmi1.name == vmi2.name && vmi1.indices == vmi2.indices; }

} // namespace FivePointAmplitudes

namespace std {

template <> struct hash<FivePointAmplitudes::VMCoefficientID> {
    size_t operator()(const FivePointAmplitudes::VMCoefficientID& vmi) const {
        size_t result = hash<std::string>{}(vmi.name);
        FivePointAmplitudes::hash_combine(result, vmi.indices);
        return result;
    }
};

} // namespace std

namespace  FivePointAmplitudes {


template <typename T> class VM_Cache {
  private:
    std::unordered_map<VMCoefficientID, size_t> _evaluator_indices;
    std::vector<vm::VirtualMachine<T>> _evaluators;

    /***
     * This cache is by argument values. This is safe, but might not fire due to non-assoicativity of floating point arithmetics.
     */
    std::unordered_map<std::pair<size_t, std::vector<T>>, T> _cached_values;
    /***
     * Cache by permutations, more careful external management needed.
     */
    std::unordered_map<std::pair<size_t, Permutation>, T> _cached_values_permutation;

  public:
    VM_Cache() = default;
    VM_Cache(const VM_Cache& other) = default;
    VM_Cache(VM_Cache&& other) = default;
    VM_Cache& operator=(VM_Cache&) =  default;
    VM_Cache& operator=(VM_Cache&&) =  default;

    /**
     * Add a function to the manager
     */
    int add(const VMCoefficientID&);
    /**
     * Evaluate with arguments to pass to the VMs 
     *
     * The size of argument MUST corespond to what VM expects.
     *
     * All evaluations are cached until are not recomputed until flush() is called.
     */
    T eval(size_t index, const std::vector<T>& args);
    /**
     * Evaluate with arguments to pass to the VMs 
     *
     * The size of argument MUST corespond to what VM expects.
     *
     * Compared to above, all evaluations are cached by the given Permutation.
     * It must then be arranged externally that this makes sense.
     */
    T eval(size_t index, const std::vector<T>& args, const Permutation& perm);

    /**
     * Clear cash
     */
    void flush() {
        _cached_values.clear();
        _cached_values_permutation.clear();
    }

    size_t size() const { return _evaluators.size(); }

  private:
    bool locked{false}; /// will not allow adding more functions once locked after the first evaluation
    mutable std::mutex _mtx;
};


template <typename T> int VM_Cache<T>::add(const VMCoefficientID & fid) {

    std::lock_guard<std::mutex> _lock(_mtx);

    if (locked) { throw std::logic_error("VM_Cache does not accept new functions after the first evaluation!"); }

    auto find = _evaluator_indices.find(fid);

    if (find != _evaluator_indices.end()) return find->second;


    std::string filename = std::string(FivePointAmplitudes_DATADIR)+"/"+ fid.get_file_name() + ".dat";
    auto f = std::ifstream(filename);

    if (!f.is_open()) { throw std::runtime_error("Could not open file " + filename); }

    //_evaluators.push_back(f);
    _evaluators.emplace_back(filename, f);

    size_t new_id = _evaluators.size() - 1;

    //dbg(fid.indices,new_id);

    _evaluator_indices[fid] = new_id;

    return new_id;
}

template <typename T> T VM_Cache<T>::eval(size_t index, const std::vector<T>& args) {
    locked = true;
    auto find = _cached_values.find(std::pair{index, args});
    if (find != _cached_values.end()) return find->second;

    //dbg(index);

#ifdef TIMING_ON
        ::timing::_rational_functions_timing.start();
#endif // TIMING_ON

    auto r = _evaluators.at(index)(args);

#ifdef TIMING_ON
        ::timing::_rational_functions_timing.stop();
#endif // TIMING_ON

    _cached_values[{index, args}] = r;

    //dbg(index,r);


    return r;
}

template <typename T> T VM_Cache<T>::eval(size_t index, const std::vector<T>& args, const Permutation& perm) {
    locked = true;
    auto find = _cached_values_permutation.find(std::pair{index, perm});
    if (find != _cached_values_permutation.end()) return find->second;

    //dbg(index, perm);

#ifdef TIMING_ON
        ::timing::_rational_functions_timing.start();
#endif // TIMING_ON

    auto r = _evaluators.at(index)(args);

#ifdef TIMING_ON
        ::timing::_rational_functions_timing.stop();
#endif // TIMING_ON

    _cached_values_permutation[{index, perm}] = r;

    //dbg(index,r);


    return r;
}


template <KinType KT, typename T, typename Tcoeffs = T> class RemainderEvaluatorVM_sij {
  public:
    using TC = std::complex<T>;

    std::string name;
    std::vector<int> indices;

    Permutation permutation;

    TC operator()(mom_conf<T> mc, T mu);

    RemainderEvaluatorVM_sij() = delete;
    RemainderEvaluatorVM_sij (const RemainderEvaluatorVM_sij&) = default;
    RemainderEvaluatorVM_sij (RemainderEvaluatorVM_sij&&) = default;
    RemainderEvaluatorVM_sij& operator= (const RemainderEvaluatorVM_sij&) = default;
    RemainderEvaluatorVM_sij& operator= (RemainderEvaluatorVM_sij&&) = default;

    RemainderEvaluatorVM_sij(
            const std::string& set_name, 
            const std::vector<int>& set_indices, 
            const Permutation& set_permutation, 
            VariableEvaluatorType<Tcoeffs> set_variable_evaluator,
            InverseDenominatorsEvaluatorType<Tcoeffs> set_inverse_denominators_evaluator,
            std::shared_ptr<FunctionsManagerG<KT>> set_f_manager,
            std::shared_ptr<VM_Cache<Tcoeffs>> set_c_manager
    ) : 
        name{set_name}, 
        indices{set_indices}, 
        permutation{set_permutation}, 
        variable_evaluator{set_variable_evaluator}, 
        inverse_denominators_evaluator{set_inverse_denominators_evaluator}, 
        f_manager{set_f_manager}, 
        c_manager{set_c_manager}
    {
        read_embedding_map();
        construct_functions_basis();
        construct_coefficient_list();
    };

  private:
    std::vector<std::vector<std::pair<f_type,int>>> functions_basis;

    using FLC = std::vector<std::pair<int,Tcoeffs>>;
    using TCoeffList = std::vector<std::pair<FLC, FLC>>;

    TCoeffList coefficients;
    /**
     * Pointer to a function that evaluates variables.
     */
    VariableEvaluatorType<Tcoeffs> variable_evaluator; 
    /**
     * Pointer to a function that evaluates inverse denominators.
     * This is joined together with variables and passed as an argument to the VM.
     */
    InverseDenominatorsEvaluatorType<Tcoeffs> inverse_denominators_evaluator;


    std::shared_ptr<FunctionsManagerG<KT>> f_manager;
    std::shared_ptr<VM_Cache<Tcoeffs>> c_manager;

    /**
     * Momenta map for pentagon functions.
     */
    EmbeddingMap embedding_map;

    /**
     * Indecies in VM_Cache which corresponds to an overall factor
     * {a, b} 
     * where the normalization is a + tr5 b
     */
    std::pair<size_t,size_t> normalization_indices;

    std::string get_filename();
    void read_embedding_map();
    void construct_functions_basis();
    void construct_coefficient_list();
};

template <KinType KT, typename T, typename Tcoeffs> std::string RemainderEvaluatorVM_sij<KT, T, Tcoeffs>::get_filename() {
    std::string r = name;

    for(auto i : indices) {
        r += "_";
        r += std::to_string(i);
    }

    return r;
}

template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM_sij<KT, T, Tcoeffs>::read_embedding_map() {
    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_embedding_map.dat"));

    for(MathList l : input) {
        std::vector<int> indices;
        for (auto ind : l) {
            indices.push_back(std::stoi(ind));
        }

        embedding_map.push_back(indices);
    }

    // TODO: write a check
    //if (embedding_map.size() != 5) {
        //throw std::logic_error("Embedding map is not valid");
    //};
}


template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM_sij<KT, T, Tcoeffs>::construct_functions_basis() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;

    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_fmonomials.dat"));

    functions_basis.resize(input.size());

    size_t n = 0;

    for (MathList mm : input) {
        for (MathList pp : mm) {
            f_type t = from_string(pp.head);
            switch (t) {
                case f_type::one:
                case f_type::str5: functions_basis.at(n).emplace_back(t, 1); break;
                case f_type::bc:
                case f_type::bci:
                case f_type::isqrt: functions_basis.at(n).emplace_back(t, std::stoul(pp[0])); break;
                case f_type::f: {
                    int id = 0;
                    if (pp.size() >= 3) { id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1]), std::stoi(pp[2])}); }
                    else if (pp.size() == 2) {
                        id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1])});
                    }
                    functions_basis.at(n).emplace_back(t, id);
                }
            }
        }
        ++n;
    }
}

template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM_sij<KT, T, Tcoeffs>::construct_coefficient_list() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;
    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_mmatrix.dat"));

    size_t n = 0;

    coefficients.resize(input.size());

    for (MathList ppair : input) {
        std::vector<FLC> nentry;
        for (MathList lc : ppair) {
            if (lc.size() == 0) {
                nentry.push_back({});
            }
            else{
                FLC coeff;
                for(MathList prod : lc ){
                    auto extra_index = indices;
                    extra_index.push_back(std::stoi(prod[0]));
                    int index = c_manager -> add({name, extra_index}); 
                    coeff.emplace_back(index, get_rational_number<Tcoeffs>(prod[1]));
                }
                nentry.push_back(coeff);
            }
        }
        coefficients.at(n).first = nentry.at(0);
        coefficients.at(n).second = nentry.at(1);
        ++n;
    }


    // construct normalziation function

    normalization_indices.first = c_manager -> add({get_filename()+"_norm", {0}});
    normalization_indices.second = c_manager -> add({get_filename()+"_norm", {1}});

}


template <KinType KT, typename T, typename Tcoeffs> std::complex<T> RemainderEvaluatorVM_sij<KT, T, Tcoeffs>::operator()(mom_conf<T> mc, T mu) {
    // The coefficients of all functions in all remainders are by construction dimensionless,
    // so for better numerical stability we rescale them.
    // We could choose the scale ourselves here, but we can also assume that the renormalization
    // scale has been chosen naturally, so we use it instead.
    if (mu != T(1)) {
        mc = mc.rescale(mu);
    }

    mom_conf<T> mc_F = mc;

    if (!embedding_map.empty()) { mc_F = mc.embed_into(embedding_map); }

    Kin<T, KT> kin{detail::get_sijs<KT>(mc_F)};

    /// TODO: change_precision
    mom_conf<Tcoeffs> permuted_mc = increase_precision<Tcoeffs>(mc);
    if (!permutation.empty()) { permuted_mc = permuted_mc.permute(permutation); }

    Tcoeffs tr5_imag = permuted_mc.get_tr5().imag();
    int str5 = 1;
    if (tr5_imag < 0) str5 *= -1;


    auto vars  = variable_evaluator(permuted_mc);

    std::vector<Tcoeffs> args(vars.begin(), vars.end());
    {
        auto qs  = inverse_denominators_evaluator(vars);
        args.insert(args.end(), qs.begin(), qs.end());
    }

    //for (size_t i = 0; i < args.size(); ++i) {
        //dbg(i,args.at(i));
    //}


#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.start();
#endif // TIMING_ON
        const auto& function_values = f_manager->eval(kin);
#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON 
        ::timing::_summations_timing.start();
#endif // TIMING_ON

    std::vector<TC> functions_basis_values;
    functions_basis_values.reserve(function_values.size());

    for (auto& mono: functions_basis) {
        TC c{1};
        for (const auto& el : mono) {
            switch (el.first) {
                case f_type::f: c *= function_values.at(el.second); break;
                case f_type::bc: c *= detail::get_bc<KT,T>(el.second); break;
                case f_type::bci: c *= TC(0, detail::get_bc<KT,T>(el.second)); break;
                // Note we take the kinematics of pentagon functions for this:
                case f_type::isqrt: c /= kin.W[el.second]; break;
                case f_type::str5: c *= str5; break;
                case f_type::one: break;
            }
        }
        functions_basis_values.push_back(std::move(c));
    }


    // Neumaier summation (for improved numerical stability) [doi:10.1002/zamm.19740540106]
    auto sum_coeff = [this, &args](const auto& values) {
        using std::get;
        
        // trivial cases 
        switch (values.size()) {
            case 0: return Tcoeffs(0);
            case 1: return c_manager->eval(get<int>(values[0]), args) * get<Tcoeffs>(values[0]);
            case 2: return c_manager->eval(get<int>(values[0]), args) * get<Tcoeffs>(values[0]) + c_manager->eval(get<int>(values[1]), args) * get<Tcoeffs>(values[1]);
            default: break;
        }

        Tcoeffs sum{0};
        Tcoeffs c{0};

        for (const auto& it : values) {
            using std::abs;
            using std::get;

            auto si = (c_manager->eval(get<int>(it), args) * get<Tcoeffs>(it));

            auto t = sum + si;

            if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
            else {
                c += ((si - t) + sum);
            }
            sum = t;
        }
        sum += c;
        return sum;
    };

    add_complex_t<Tcoeffs> sum{0};
    add_complex_t<Tcoeffs> c{0};

    for (size_t i = 0; i < functions_basis.size(); ++i){
        using std::abs;

        Tcoeffs re = sum_coeff(coefficients[i].first);
        Tcoeffs im = sum_coeff(coefficients[i].second);

        im *= tr5_imag;

        //dbg(i,re,im);

        add_complex_t<Tcoeffs> si{re, im};

        si *= functions_basis_values[i];

        auto t = sum + si;
        if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
        else {
            c += ((si - t) + sum);
        }
        sum = t;
    }
    sum += c;


    // normalize
    Tcoeffs norm_0 = c_manager->eval(normalization_indices.first, args);
    Tcoeffs norm_1 = c_manager->eval(normalization_indices.second, args);
    norm_1 *= tr5_imag;
    sum /= std::complex{norm_0, norm_1};

#ifdef TIMING_ON 
        ::timing::_summations_timing.stop();
#endif // TIMING_ON

    /// FIXME: make this cleaner and facrored out
    if constexpr (std::is_same_v<T, Tcoeffs>) {
        return sum;
    }
    else if constexpr (std::is_same_v<T, double>) {
        return to_double(sum);
    }
#ifdef FivePointAmplitudes_QD_ENABLED 
    else if constexpr (std::is_same_v<T, dd_real>) {
        return {to_dd_real(sum.real()), to_dd_real(sum.imag())};
    }
#endif // FivePointAmplitudes_QD_ENABLED
    else {
        return sum;
    }

}





















template <KinType KT, typename T, typename Tcoeffs = T> class RemainderEvaluatorVM2_sij {
  public:
    using TC = std::complex<T>;
    using TCcoeffs = std::complex<Tcoeffs>;

    std::string name;
    std::vector<int> indices;

    Permutation permutation;

    TC operator()(mom_conf<T> mc, T mu);

    RemainderEvaluatorVM2_sij() = delete;
    RemainderEvaluatorVM2_sij (const RemainderEvaluatorVM2_sij&) = default;
    RemainderEvaluatorVM2_sij (RemainderEvaluatorVM2_sij&&) = default;
    RemainderEvaluatorVM2_sij& operator= (const RemainderEvaluatorVM2_sij&) = default;
    RemainderEvaluatorVM2_sij& operator= (RemainderEvaluatorVM2_sij&&) = default;

    RemainderEvaluatorVM2_sij(
            const std::string& set_name, 
            const std::vector<int>& set_indices, 
            const Permutation& set_permutation, 
            VariableEvaluatorType<Tcoeffs> set_variable_evaluator,
            InverseDenominatorsEvaluatorType<Tcoeffs> set_inverse_denominators_evaluator,
            std::shared_ptr<FunctionsManagerG<KT>> set_f_manager,
            std::shared_ptr<VM_Cache<Tcoeffs>> set_c_manager
    ) : 
        name{set_name}, 
        indices{set_indices}, 
        permutation{set_permutation}, 
        variable_evaluator{set_variable_evaluator}, 
        inverse_denominators_evaluator{set_inverse_denominators_evaluator}, 
        f_manager{set_f_manager}, 
        c_manager{set_c_manager}
    {
        read_embedding_map();
        construct_functions_basis();
        construct_coefficient_list();
    };

    bool large_cancellations_flag{false};

  private:
    std::vector<std::pair<f_type,int>> fbasis;
    std::vector<std::pair<int, int>> cbasis;
    /**
     * Pointer to a function that evaluates variables.
     */
    VariableEvaluatorType<Tcoeffs> variable_evaluator; 
    /**
     * Pointer to a function that evaluates inverse denominators.
     * This is joined together with variables and passed as an argument to the VM.
     */
    InverseDenominatorsEvaluatorType<Tcoeffs> inverse_denominators_evaluator;


    std::shared_ptr<FunctionsManagerG<KT>> f_manager;
    std::shared_ptr<VM_Cache<Tcoeffs>> c_manager;

    std::shared_ptr<vm::VirtualMachine<TCcoeffs>> sum_evaluator;

    /**
     * Momenta map for pentagon functions.
     */
    EmbeddingMap embedding_map;

    /**
     * Indecies in VM_Cache which corresponds to an overall factor
     * {a, b} 
     * where the normalization is a + tr5 b
     */
    std::pair<size_t,size_t> normalization_indices;

    std::string get_filename();
    void read_embedding_map();
    void construct_functions_basis();
    void construct_coefficient_list();
};

template <KinType KT, typename T, typename Tcoeffs> std::string RemainderEvaluatorVM2_sij<KT, T, Tcoeffs>::get_filename() {
    std::string r = name;

    for(auto i : indices) {
        r += "_";
        r += std::to_string(i);
    }

    return r;
}

template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM2_sij<KT, T, Tcoeffs>::read_embedding_map() {
    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_embedding_map.dat"));

    for(MathList l : input) {
        std::vector<int> indices;
        for (auto ind : l) {
            indices.push_back(std::stoi(ind));
        }

        embedding_map.push_back(indices);
    }

    // TODO: write a check
    //if (embedding_map.size() != 5) {
        //throw std::logic_error("Embedding map is not valid");
    //};
}


template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM2_sij<KT, T, Tcoeffs>::construct_functions_basis() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;

    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_fbasis.dat"));

    fbasis.reserve(input.size());

    for (MathList pp : input) {
        f_type t = from_string(pp.head);
        switch (t) {
            case f_type::one:
            case f_type::str5: fbasis.emplace_back(t, 1); break;
            case f_type::bc:
            case f_type::bci:
            case f_type::isqrt: fbasis.emplace_back(t, std::stoul(pp[0])); break;
            case f_type::f: {
                int id = 0;
                if (pp.size() >= 3) { id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1]), std::stoi(pp[2])}); }
                else if (pp.size() == 2) {
                    id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1])});
                }
                fbasis.emplace_back(t, id);
            }
        }
    }

}

template <KinType KT, typename T, typename Tcoeffs> void RemainderEvaluatorVM2_sij<KT, T, Tcoeffs>::construct_coefficient_list() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;
    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_cbasis.dat"));

    cbasis.reserve(input.size());

    for (MathList ppair : input) {
        cbasis.emplace_back(-1,-1);

        if(int ind = std::stoi(ppair[0]); ind > 0) {
            auto extra_index = indices;
            extra_index.push_back(ind);
            cbasis.back().first = c_manager -> add({name, extra_index}); 
        }

        if(int ind = std::stoi(ppair[1]); ind > 0) {
            auto extra_index = indices;
            extra_index.push_back(ind);
            cbasis.back().second = c_manager -> add({name, extra_index}); 
        }
    }

    {
        std::string filename = std::string(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_sum.dat");
        auto f = std::ifstream(filename);
        if (!f.is_open()) { throw std::runtime_error("Could not open file " + filename); }

        sum_evaluator = std::make_shared<vm::VirtualMachine<TCcoeffs>>(filename, f);
    }

    // construct normalziation function

    normalization_indices.first = c_manager -> add({get_filename()+"_norm", {0}});
    normalization_indices.second = c_manager -> add({get_filename()+"_norm", {1}});

}


template <KinType KT, typename T, typename Tcoeffs> std::complex<T> RemainderEvaluatorVM2_sij<KT, T, Tcoeffs>::operator()(mom_conf<T> mc, T mu) {
    // The coefficients of all functions in all remainders are by construction dimensionless,
    // so for better numerical stability we rescale them.
    // We could choose the scale ourselves here, but we can also assume that the renormalization
    // scale has been chosen naturally, so we use it instead.
    if (mu != T(1)) {
        mc = mc.rescale(mu);
    }


    // We collect in this variable the largest coefficient encountered.
    // We will then compare it to the sum to detect potential large cancellations.
    Tcoeffs max_abs_coeff{};

    //dbg::g_floating_point_format << std::setprecision(std::numeric_limits<T>::digits10);

    mom_conf<T> mc_F = mc;

    if (!embedding_map.empty()) { mc_F = mc.embed_into(embedding_map); }

    Kin<T, KT> kin{detail::get_sijs<KT>(mc_F)};

    /// TODO: change_precision
    mom_conf<Tcoeffs> permuted_mc = increase_precision<Tcoeffs>(mc);
    if (!permutation.empty()) { permuted_mc = permuted_mc.permute(permutation); }

    Tcoeffs tr5_imag = permuted_mc.get_tr5().imag();
    int str5 = 1;
    if (tr5_imag < 0) str5 *= -1;


    auto vars  = variable_evaluator(permuted_mc);

    std::vector<Tcoeffs> args(vars.begin(), vars.end());
    {
        auto qs  = inverse_denominators_evaluator(vars);
        args.insert(args.end(), qs.begin(), qs.end());
    }

    //for (size_t i = 0; i < args.size(); ++i) {
        //dbg(i,args.at(i));
    //}


#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.start();
#endif // TIMING_ON
        const auto& function_values = f_manager->eval(kin);
#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON 
        ::timing::_summations_timing.start();
#endif // TIMING_ON

    std::vector<TCcoeffs> functions_basis_values;
    functions_basis_values.reserve(fbasis.size()+cbasis.size());

    for (auto& el: fbasis) {
        TC c{1};
        switch (el.first) {
            case f_type::f: c = function_values.at(el.second); break;
            case f_type::bc: c = detail::get_bc<KT,T>(el.second); break;
            case f_type::bci: c = TC(0, detail::get_bc<KT,T>(el.second)); break;
            // Note we take the kinematics of pentagon functions for this:
            case f_type::isqrt: c /= kin.W[el.second]; break;
            case f_type::str5: c = str5; break;
            case f_type::one: break;
        }
        functions_basis_values.push_back(c);
        if(auto norm = detail::norm1(c); norm > max_abs_coeff) max_abs_coeff=norm;
    }


    auto downcast = [](auto x) {
        /// FIXME: make this cleaner and facrored out
        if constexpr (std::is_same_v<T, Tcoeffs>) {
            return x;
        }
        else if constexpr (std::is_same_v<T, double>) {
            return to_double(x);
        }
#ifdef FivePointAmplitudes_QD_ENABLED 
        else if constexpr (std::is_same_v<T, dd_real>) {
            return std::complex{to_dd_real(x.real()), to_dd_real(x.imag())};
        }
#endif
        else {
            return x;
        }
    };

    for(auto [rei, imi] : cbasis) {
        Tcoeffs re{}; 
        Tcoeffs im{};

        /**
         * WARNING EXTREME DANGER: 
         * this assumes that the evaluations with each index will differ ONLY by permutation. 
         * If the cash is not flushed externally when the point actually changes this is wrong obviously.
         */
        if (rei >= 0)  re = c_manager->eval(rei, args, permutation); 
        if (imi >= 0)  im = c_manager->eval(imi, args, permutation); 

        im *= tr5_imag;

        //dbg(re,im);

        functions_basis_values.push_back(std::complex{re,im});

        if(auto norm = detail::norm1(functions_basis_values.back()); norm > max_abs_coeff) max_abs_coeff=norm;
    }

    auto sum = sum_evaluator->operator()(functions_basis_values);

    using std::fabs;
    if (fabs(sum) < max_abs_coeff*Tcoeffs{large_cancellations_threshold}) large_cancellations_flag = true;
    else large_cancellations_flag = false;

    // normalize
    Tcoeffs norm_0 = c_manager->eval(normalization_indices.first, args, permutation);
    Tcoeffs norm_1 = c_manager->eval(normalization_indices.second, args, permutation);
    norm_1 *= tr5_imag;
    sum /=std::complex{norm_0, norm_1};

#ifdef TIMING_ON 
        ::timing::_summations_timing.stop();
#endif // TIMING_ON

    return downcast(sum);
}

enum class str5Type { global, permutation };

template <KinType KT, typename T, typename Tcoeffs = T, str5Type str5_type = str5Type::global> class RemainderEvaluatorVM2_spinor {
  public:
    using TC = std::complex<T>;
    using TCcoeffs = std::complex<Tcoeffs>;
    using ReturnType = TC;

    std::string name; //! name of the partial remainder
    std::string rat_basis_name{}; //! name of the rational functions basis set
    std::vector<int> indices;

    // Permutation applied to the whole remainder.
    Permutation permutation;
    // Previous permutation composed with an additional permutation 
    // applied to rational basis only to map it onto a standard basis with the fixed helicity weight.
    Permutation to_rb_permutation{};

    TC operator()(mom_conf<T> mc, T mu);

    RemainderEvaluatorVM2_spinor() = delete;
    RemainderEvaluatorVM2_spinor (const RemainderEvaluatorVM2_spinor&) = default;
    RemainderEvaluatorVM2_spinor (RemainderEvaluatorVM2_spinor&&) = default;
    RemainderEvaluatorVM2_spinor& operator= (const RemainderEvaluatorVM2_spinor&) = default;
    RemainderEvaluatorVM2_spinor& operator= (RemainderEvaluatorVM2_spinor&&) = default;

    RemainderEvaluatorVM2_spinor(
            const std::string& set_name, 
            const std::vector<int>& set_indices, 
            const Permutation& set_permutation, 
            VariableEvaluatorTypeSpinor<Tcoeffs> set_variable_evaluator,
            InverseDenominatorsEvaluatorType<TCcoeffs> set_inverse_denominators_evaluator,
            std::shared_ptr<FunctionsManagerG<KT>> set_f_manager = std::make_shared<FunctionsManagerG<KT>>(),
            std::shared_ptr<VM_Cache<TCcoeffs>> set_c_manager = std::make_shared<VM_Cache<TCcoeffs>>()
    ) : 
        name{set_name}, 
        indices{set_indices}, 
        permutation{set_permutation}, 
        variable_evaluator{set_variable_evaluator}, 
        inverse_denominators_evaluator{set_inverse_denominators_evaluator}, 
        f_manager{set_f_manager}, 
        c_manager{set_c_manager}
    {
        read_embedding_map();
        construct_functions_basis();
        construct_coefficient_list();
    };

    bool large_cancellations_flag{false};

  private:
    std::vector<std::pair<f_type,int>> fbasis;
    std::vector<int> cbasis;
    /**
     * Pointer to a function that evaluates variables.
     */
    VariableEvaluatorTypeSpinor<Tcoeffs> variable_evaluator; 
    /**
     * Pointer to a function that evaluates inverse denominators.
     * This is joined together with variables and passed as an argument to the VM.
     */
    InverseDenominatorsEvaluatorType<TCcoeffs> inverse_denominators_evaluator;


    std::shared_ptr<FunctionsManagerG<KT>> f_manager;
    std::shared_ptr<VM_Cache<TCcoeffs>> c_manager;

    std::shared_ptr<vm::VirtualMachine<TCcoeffs>> sum_evaluator;

    /**
     * Momenta map for pentagon functions.
     */
    EmbeddingMap embedding_map{};

    /**
     * Index in VM_Cache which corresponds to an overall factor.
     * It is optional. If it is provided, it must make the remainder dimensionless!
     */
    size_t normalization_index{0};

    std::string get_filename();
    void read_embedding_map();
    void construct_functions_basis();
    void construct_coefficient_list();
};

template <KinType KT, typename T, typename Tcoeffs, str5Type str5_type> std::string RemainderEvaluatorVM2_spinor<KT, T, Tcoeffs, str5_type>::get_filename() {
    std::string r = name;

    for(auto i : indices) {
        r += "_";
        r += std::to_string(i);
    }

    return r;
}

template <KinType KT, typename T, typename Tcoeffs, str5Type str5_type> void RemainderEvaluatorVM2_spinor<KT, T, Tcoeffs, str5_type>::read_embedding_map() {
    MathList input;
    try {
        input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR) + "/" + get_filename() + "_embedding_map.dat"));
    }
    catch (...) {
        return;
    }

    for(MathList l : input) {
        std::vector<int> indices;
        for (auto ind : l) {
            indices.push_back(std::stoi(ind));
        }

        embedding_map.push_back(indices);
    }

    // TODO: write a check
    //if (embedding_map.size() != 5) {
        //throw std::logic_error("Embedding map is not valid");
    //};
}


template <KinType KT, typename T, typename Tcoeffs, str5Type str5_type> void RemainderEvaluatorVM2_spinor<KT, T, Tcoeffs, str5_type>::construct_functions_basis() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;

    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_fbasis.dat"));

    fbasis.reserve(input.size());

    for (MathList pp : input) {
        f_type t = from_string(pp.head);
        switch (t) {
            case f_type::one:
            case f_type::str5: fbasis.emplace_back(t, 1); break;
            case f_type::bc:
            case f_type::bci:
            case f_type::isqrt: fbasis.emplace_back(t, std::stoul(pp[0])); break;
            case f_type::f: {
                int id = 0;
                if (pp.size() >= 3) { id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1]), std::stoi(pp[2])}); }
                else if (pp.size() == 2) {
                    id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1])});
                }
                fbasis.emplace_back(t, id);
            }
        }
    }

}

template <KinType KT, typename T, typename Tcoeffs, str5Type str5_type> void RemainderEvaluatorVM2_spinor<KT, T, Tcoeffs, str5_type>::construct_coefficient_list() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;
    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_cbasis.dat"));

    if (input.size() >= 1) {
        rat_basis_name = input[1];
        for (auto ind : MathList(input[2])) { to_rb_permutation.push_back(std::stoi(ind)); }
        if (to_rb_permutation.size() != permutation.size()) throw std::runtime_error("Inconsistent permutations in RemainderEvaluatorVM2_spinor");
    }

    cbasis.reserve(input[0].size());

    for (auto ind : MathList(input[0])) {
        auto extra_index = indices;
        extra_index.push_back(std::stoi(ind));
        if(rat_basis_name.empty()) {
            cbasis.push_back(c_manager -> add({name, extra_index})); 
        }
        else {
            cbasis.push_back(c_manager -> add({rat_basis_name, extra_index})); 
        }
    }

    {
        std::string filename = std::string(std::string(FivePointAmplitudes_DATADIR)+"/"+get_filename()+"_"+perm_string.str()+"_sum.dat");
        auto f = std::ifstream(filename);
        if (!f.is_open()) { throw std::runtime_error("Could not open file " + filename); }

        sum_evaluator = std::make_shared<vm::VirtualMachine<TCcoeffs>>(filename, f);
    }

    // construct normalziation function

    if (VMCoefficientID norm_id{get_filename() + "_norm", {}};
        std::filesystem::exists(std::string(FivePointAmplitudes_DATADIR) + "/" + norm_id.get_file_name() + ".dat")) {
        normalization_index = c_manager->add(norm_id);
    }
}


template <KinType KT, typename T, typename Tcoeffs, str5Type str5_type> std::complex<T> RemainderEvaluatorVM2_spinor<KT, T, Tcoeffs, str5_type>::operator()(mom_conf<T> mc, T mu) {
    // The coefficients of all functions in all remainders are by construction dimensionless,
    // so for better numerical stability we rescale them.
    // We could choose the scale ourselves here, but we can also assume that the renormalization
    // scale has been chosen naturally, so we use it instead.
    if (mu != T(1)) {
        mc = mc.rescale(mu);
    }


    // We collect in this variable the largest coefficient encountered.
    // We will then compare it to the sum to detect potential large cancellations.
    Tcoeffs max_abs_coeff{};

    //dbg::g_floating_point_format << std::setprecision(std::numeric_limits<T>::digits10);

    mom_conf<T> mc_F = mc;

    if (!embedding_map.empty()) { mc_F = mc.embed_into(embedding_map); }

    Kin<T, KT> kin{detail::get_sijs<KT>(mc_F)};

    /// TODO: change_precision
    mom_conf<Tcoeffs> permuted_mc = increase_precision<Tcoeffs>(mc);
    if (!to_rb_permutation.empty()) { permuted_mc = permuted_mc.permute(to_rb_permutation); }

    auto vars  = variable_evaluator(permuted_mc);

    std::vector<TCcoeffs> args(vars.begin(), vars.end());
    {
        auto qs  = inverse_denominators_evaluator(vars);
        args.insert(args.end(), qs.begin(), qs.end());
    }


#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.start();
#endif // TIMING_ON
        const auto& function_values = f_manager->eval(kin);
#ifdef TIMING_ON
        ::timing::_transcendental_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON 
        ::timing::_summations_timing.start();
#endif // TIMING_ON

    std::vector<TCcoeffs> functions_basis_values;
    functions_basis_values.reserve(fbasis.size()+cbasis.size());

    {
        int str5 = 1;
        if constexpr (str5_type == str5Type::permutation) {
            if (mc.permute(permutation).get_tr5().imag() < 0) str5 = -1;
        }
        else {
            if (mc.get_tr5().imag() < 0) str5 = -1;
        }

        for (auto& el: fbasis) {
            TC c{1};
            switch (el.first) {
                case f_type::f: c = function_values.at(el.second); break;
                case f_type::bc: c = detail::get_bc<KT,T>(el.second); break;
                case f_type::bci: c = TC(0, detail::get_bc<KT,T>(el.second)); break;
                                  // Note we take the kinematics of pentagon functions for this:
                case f_type::isqrt: c /= kin.W[el.second]; break;
                case f_type::str5: c = str5; break;
                case f_type::one: break;
            }
            functions_basis_values.push_back(c);
            if(auto norm = detail::norm1(c); norm > max_abs_coeff) max_abs_coeff=norm;
        }
    }


    auto downcast = [](auto x) {
        /// FIXME: make this cleaner and facrored out
        if constexpr (std::is_same_v<T, Tcoeffs>) {
            return x;
        }
        else if constexpr (std::is_same_v<T, double>) {
            return to_double(x);
        }
#ifdef FivePointAmplitudes_QD_ENABLED 
        else if constexpr (std::is_same_v<T, dd_real>) {
            return std::complex{to_dd_real(x.real()), to_dd_real(x.imag())};
        }
#endif
        else {
            return x;
        }
    };

    for(auto& ci : cbasis) {
        /**
         * WARNING EXTREME DANGER: 
         * this assumes that the evaluations with each index will differ ONLY by permutation. 
         * If the cash is not flushed externally when the point actually changes this is wrong obviously.
         */
        functions_basis_values.push_back(c_manager->eval(ci, args, to_rb_permutation));

        if(auto norm = detail::norm1(functions_basis_values.back()); norm > max_abs_coeff) max_abs_coeff=norm;
    }

    auto sum = sum_evaluator->operator()(functions_basis_values);

    using std::fabs;
    if (fabs(sum) < max_abs_coeff*Tcoeffs{large_cancellations_threshold}) large_cancellations_flag = true;
    else large_cancellations_flag = false;

    if (normalization_index > 0) {
        // normalize
        auto norm = c_manager->eval(normalization_index, args, permutation);
        sum /= norm;
    }
    else {
        // if no normalization, restore scale
        using std::pow;
        T scale_restore = pow(mu, mc.size()-4);
        sum /= scale_restore;
    }

#ifdef TIMING_ON 
        ::timing::_summations_timing.stop();
#endif // TIMING_ON

    return downcast(sum);
}











} // namespace FivePointAmplitudes


