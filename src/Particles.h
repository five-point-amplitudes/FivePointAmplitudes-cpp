#pragma once

#include "FivePointAmplitudes_config.h"
#include "Parameters.h"

#include <cmath>
#include <complex>
#include <matchit/matchit.h>

namespace FivePointAmplitudes {

template <typename T> constexpr T get_QED_charge(int pdg_code) {
    using namespace matchit;

    int sign = pdg_code < 0 ? -1 : 1;
    pdg_code = abs(pdg_code);

    return sign * match(pdg_code) (        
            pattern | or_(2,4,6) = T{2}/T{3},
            pattern | or_(1,3,5) = T{-1}/T{3},
            pattern | or_(11,13,15,24) = T{1},
            pattern | _ = T{0}
            );
}

inline EW_Input_Parameters read_EW_Input_Parameters(double* input_parameters) {
    EW_Input_Parameters params;

    params.enable_complex_mass_scheme = 0;

    if (input_parameters[0] == 1.) params.enable_complex_mass_scheme = 1;

    params.M_W = input_parameters[1];
    params.G_W = input_parameters[2];
    params.M_Z = input_parameters[3];
    params.G_Z = input_parameters[4];

    return params;
}


inline std::complex<double> compute_cos_theta(const EW_Input_Parameters& params) {
    using C = std::complex<double>;
    using std::sqrt;

    if(params.enable_complex_mass_scheme == 1) {
        C mu_W = sqrt(C{params.M_W*params.M_W, -params.M_W*params.G_W});
        C mu_Z = sqrt(C{params.M_Z*params.M_Z, -params.M_Z*params.G_Z});

        return mu_W/mu_Z;
    }
    else {
        return params.M_W/params.M_Z;
    }
}

    
} // FivePointAmplitudes
