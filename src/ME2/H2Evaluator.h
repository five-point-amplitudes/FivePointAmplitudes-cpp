#pragma once
#include "FivePointAmplitudes_config.h"

#include "MathList.h"
#include "Spinor.h"
#include "Utilities.h"
#include "VirtualMachine.h"

#include "RemainderEvaluatorVM.h"
#include "matchit/matchit.h"
#include "remainders/3j/functions.hpp"

#include "CheckedEvaluator.h"
#include "Parameters.h"
#include "Particles.h"
#include "FiniteRenormalizationOperator.h"

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

//#include <dbg.h>
namespace FivePointAmplitudes {
namespace jjj {

template <KinType kin_type, typename T, typename Tcoeffs = T, str5Type str5_type = str5Type::global> class H2Evaluator {
  public:
    using C = std::complex<T>;
    using ReturnType = std::vector<std::vector<T>>;
    using TreeEvaluatorType = std::function<C(const mom_conf<T>&)>;

    std::string process;

    bool large_cancellations_flag{false};
    std::function<std::vector<C>(const mom_conf<T>& mc, const T& mu, std::vector<C> args)> finite_renormalization = nullptr;

    /**
     * Evaluate the hard function
     * @param momenta
     * @param renormalization scale
     */
    ReturnType operator()(mom_conf<T> mc, T mu); 
    /**
     * Constructor from a process name, e.g. 
     * "g_g__g_g_g"
     */
    H2Evaluator(std::string process_name, decltype(finite_renormalization) _f = nullptr);

    std::vector<Permutation> get_permutations() const {return permutations;}


    C Nc{3};

  private:
    std::vector<Permutation> permutations;
    std::vector<C> input_parameters;
    std::vector<TreeEvaluatorType> tree_evaluators;
    std::vector<RemainderEvaluatorVM2_spinor<kin_type, T, Tcoeffs, str5_type>> remainder_evaluators;

    std::vector<int> return_shape;

    std::shared_ptr<vm::VirtualMachine<C,std::vector<C>>> assembly;

    std::shared_ptr<VM_Cache<add_complex_t<Tcoeffs>>> c_cache;
    std::shared_ptr<FunctionsManagerG<kin_type>> f_cache;

};

template <KinType kin_type, typename T, typename Tcoeffs, str5Type str5_type> H2Evaluator<kin_type,T,Tcoeffs,str5_type>::H2Evaluator(std::string process_name, decltype(finite_renormalization) _f) {

    c_cache = std::make_shared<VM_Cache<add_complex_t<Tcoeffs>>>();
    f_cache = std::make_shared<FunctionsManagerG<kin_type>>(true);

    
    std::string filename = std::string(FivePointAmplitudes_DATADIR) + "/h2__" + process_name + ".dat";

    std::istringstream str_stream(read_file(filename));

    auto read_permutation = [](MathList perm_in) {
        std::vector<int> perm;
        for(auto i: perm_in) {
            perm.push_back(std::stoi(i));
        }
        return perm;
    };

    std::string line;

    // read trees
    str_stream >> std::ws;
    while (std::getline(str_stream, line) && !line.empty()) {
        MathList input(line);

        using namespace matchit;
        if (input.head == "tree") {
            auto f = match(input[0]) (
                    pattern | "ggggg_pppmm_0L_Nc0_Nf0" = tree_5g_pppmm<T>,
                    pattern | "ggggg_ppmpm_0L_Nc0_Nf0" = tree_5g_ppmpm<T>,

                    pattern | "uubggg_pmppm_0L_Nc0_Nf0" = tree_2q3g_pmppm<T>,
                    pattern | "uubggg_pmpmp_0L_Nc0_Nf0" = tree_2q3g_pmpmp<T>,
                    pattern | "uubggg_pmmpp_0L_Nc0_Nf0" = tree_2q3g_pmmpp<T>,

                    pattern | "uubddbg_pmpmp_0L_Nc0_Nf0" = tree_4q1g_pmpmp<T>,
                    pattern | "uubddbg_pmmpp_0L_Nc0_Nf0" = tree_4q1g_pmmpp<T>,
                    pattern | "uubddbg_mppmp_0L_Nc0_Nf0" = tree_4q1g_mppmp<T>,

                    pattern | "uubddbg_pmpmp_0L_Ncm1_Nf0" = tree_4q1g_m1_pmpmp<T>,
                    pattern | "uubddbg_pmmpp_0L_Ncm1_Nf0" = tree_4q1g_m1_pmmpp<T>,

                    pattern | _ = nullptr
            );

            if (f == nullptr) { throw std::runtime_error("Tree " + input[0] + " not known"); }

            tree_evaluators.emplace_back([perm = read_permutation(input[1]),f](const mom_conf<T>& mc){return f(mc,perm);});
        }
        else if (input.head == "param") {
            auto pvalue = match(input[0]) (
                    pattern | "Nc" = Nc,

                    pattern | _ = C{}
            );

            if (pvalue == C{}) {
                throw std::runtime_error("Unsupported input parameter " + input[0]); 
            }

            input_parameters.push_back(pvalue);
        }
        else if (input.head == "remainder") {
            //TODO: read extra indices from input[1]
            std::vector<int> indices{};

            Permutation perm = read_permutation(input[1]);

            remainder_evaluators.emplace_back(input[0], indices, perm, get_spinor_variables<Tcoeffs>, get_inverse_denominators<Tcoeffs>, f_cache, c_cache);
        }
    }
    // read shape
    str_stream >> std::ws;
    std::getline(str_stream, line);
    return_shape = read_permutation(line);

    // read assebly
    str_stream >> std::ws;
    assembly = std::make_shared<vm::VirtualMachine<C,std::vector<C>>>(filename, str_stream);
}

template <KinType kin_type, typename T, typename Tcoeffs, str5Type str5_type> typename H2Evaluator<kin_type,T,Tcoeffs,str5_type>::ReturnType H2Evaluator<kin_type,T,Tcoeffs,str5_type>::operator()(mom_conf<T> mc, T mu) {
    large_cancellations_flag = false;

    using std::pow;
    T scale_restore = pow(mu, 2*mc.size()-8);

    // The hard function is dimensionless by construction, so we first rescale the input momenta
    if (mu != T(0)) {
        mc = mc.rescale(mu);
        // We do not need to rescale EW parameters, because they enter as dimensionaless ratios only
    }
    mu = T(1); // we already rescaled, so set to 1


    if (!mc.is_onshell_q()) throw std::runtime_error("The input phase-space spoint appears to be corrupted (failed onshell check)!");

    // Perform a random boost such that there are no exact zeroes in momenta components (this is not nice for spinor states)
    static const std::array<T,3> beta_boost = {T(0.21), T(0.19),T(0.23)}; 
    mc = lorentz_boost(mc, beta_boost);

    c_cache -> flush();
    f_cache -> flush();


    ReturnType result;

    {
        std::vector<C> args;
        args.reserve(input_parameters.size() + tree_evaluators.size() + remainder_evaluators.size());

        for(auto& it: input_parameters) {
            args.push_back(it);
        }

        for(auto& it: tree_evaluators) {
            args.push_back(it(mc));
        }

        for(auto& ri: remainder_evaluators) {
            args.push_back(ri(mc,mu));
            large_cancellations_flag = large_cancellations_flag || ri.large_cancellations_flag;
        }

        auto c_result = assembly->operator()(args);

        //dbg(c_result);

        if(finite_renormalization) {
            auto add_result = finite_renormalization(mc,mu, args);
            for (size_t i = 0; i < c_result.size(); ++i) {
                c_result[i] += add_result[i];
            }
        }

        size_t i = 0;
        for(auto entry : return_shape) {
            result.emplace_back();
            for (int j= 0; j < entry; ++j) {
                result.back().push_back(c_result.at(i).real());
                ++i;
            }
        }
    }

    c_cache -> flush();

    {
        mc = mc.parity_conjugate();

        std::vector<C> args;
        args.reserve(input_parameters.size() + tree_evaluators.size() + remainder_evaluators.size());

        for(auto& it: input_parameters) {
            args.push_back(it);
        }

        for(auto& it: tree_evaluators) {
            args.push_back(it(mc));
        }

        for(auto& ri: remainder_evaluators) {
            args.push_back(ri(mc,mu));
            large_cancellations_flag = large_cancellations_flag || ri.large_cancellations_flag;
        }

        auto c_result = assembly->operator()(args);

        //dbg(c_result);

        if(finite_renormalization) {
            auto add_result = finite_renormalization(mc,mu, args);
            for (size_t i = 0; i < c_result.size(); ++i) {
                c_result[i] += add_result[i];
            }
        }

        size_t ii = 0;
        for (size_t i = 0; i < result.size(); ++i) {
            for (size_t j = 0; j < result.at(i).size(); ++j) {
                result.at(i).at(j) += c_result.at(ii).real();
                ++ii;
            }
        }
    }

    T born = result[0][0];

    for(auto& asi : result){
        for(auto& ri : asi){
            ri /= born;
        }
    }

    born /= scale_restore;
    result[0][0] = born;

    return result;
}

template <typename T> using H2EvaluatorM0 = H2Evaluator<KinType::m0, T>;



inline H_EvaluatorFunctionType_Interface h2__p_p__j_j_j_construct(const std::vector<int>& pdg_codes, int use_checked, double* input_parameters, int* int_options, double* double_options) {

    // here we transform 
    auto from_pdg_code = [](int code) -> std::string {
        switch(code) {
            case 21: case -21: return "g";
            case 1: return "u";
            case -1: return "ub";
            case 2: return "d";
            case -2: return "db";
            default: throw std::runtime_error("Particle code " + std::to_string(code) + " not recognized in " + std::string(__FUNCTION__));
        }
    };

    std::string h2name{};

    try {
        // we convert here to all outgoing convention
        h2name += from_pdg_code(-pdg_codes.at(0)) + "_" + from_pdg_code(-pdg_codes.at(1)) + "_";

        for (size_t i = 2; i < pdg_codes.size(); ++i) {
            h2name += "_";
            h2name += from_pdg_code(pdg_codes[i]);
        }

        bool include_one_loop_squared = static_cast<bool>(int_options[0]);

        if(!include_one_loop_squared) throw std::runtime_error(std::string(__FUNCTION__) + " does not support not including 1-loop squared contributions");

        int remainders_convention = int_options[1]; /// 0 - Catani, 1- MSbar
        int color_mode = int_options[2]; /// 0 - full color, 1- only leading

        if (color_mode == 1) { h2name += ".lc"; }

        if (use_checked) {
            auto evaluator_f = CheckedEvaluator<H2EvaluatorM0>{h2name};
            if (remainders_convention == 0) evaluator_f.add_finite_renormalization<FiniteRenormalizationOperator_Add>("h2__"+h2name+".MS_to_Catani.dat");
            return evaluator_f;
        }
        else {
            auto evaluator_f = H2EvaluatorM0<double>{h2name};
            if (remainders_convention == 0) evaluator_f.finite_renormalization = FiniteRenormalizationOperator_Add<double>("h2__"+h2name+".MS_to_Catani.dat");
            return evaluator_f;
        }
    }
    catch (std::exception& e) {
        std::cerr << "Encountered an error during construction of " << h2name << ":\n" <<  e.what() << std::endl;
        return nullptr;
    }
}


} // namespace jjj
} // namespace FivePointAmplitudes

