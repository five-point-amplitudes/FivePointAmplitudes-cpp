#include "FivePointAmplitudes_config.h"

#include "Parameters.h"
#include "PrecisionCast.h"
#include "h2__q_qb__bb_b_eb_e.hpp"

#include "FiniteRenormalizationOperator.h"

#include "CheckedEvaluator.h"
#include "Particles.h"

#include <ChangePrecision.h>


#include <iostream>
#include <memory>
#include <stdexcept>

namespace FivePointAmplitudes {

template <typename T> using h2__q_qb__bb_b_eb_e_default = h2__q_qb__bb_b_eb_e<T, ::PentagonFunctions::raise_precision_t<T>>;
    

H_EvaluatorFunctionType_Interface h2__q_qb__bb_b_eb_e_construct(const std::vector<int>& pdg_codes, int use_checked, double* input_parameters, int* int_options, double* double_options) {
    try {
        auto params = read_EW_Input_Parameters(input_parameters);

        // NOTE: this relies on the exact order of input particles
        double Q_b = get_QED_charge<double>(pdg_codes.at(3));
        double Q_q = get_QED_charge<double>(pdg_codes.at(0));

        bool include_one_loop_squared = static_cast<bool>(int_options[0]);
        int remainders_convention = int_options[1]; /// 0 - Catani, 1- MSbar

        static const std::string IOp_Catani_to_MSbar_filename = FivePointAmplitudes_DATADIR+std::string("/Vjj_IOp_Catani_to_MSbar_WqQQq.dat");

        if (use_checked) {
            auto evaluator_f = CheckedEvaluator<h2__q_qb__bb_b_eb_e_default>{params, Q_b, Q_q};

            if(!include_one_loop_squared) {
                evaluator_f.h_double.include_one_loop_squared= false;
#ifdef FivePointAmplitudes_QD_ENABLED
                evaluator_f.h_hp.include_one_loop_squared= false;
#endif 
            }

            switch (remainders_convention) {
                case 1: {
                    evaluator_f.h_double.finite_renormalization = Vjj::FiniteRenormalizationOperator<double>{IOp_Catani_to_MSbar_filename};
#ifdef FivePointAmplitudes_QD_ENABLED
                    evaluator_f.h_hp.finite_renormalization = Vjj::FiniteRenormalizationOperator<dd_real>{IOp_Catani_to_MSbar_filename};
#endif // FivePointAmplitudes_QD_ENABLED
                    break;
                }
                default: break;
            }

            evaluator_f.letters_threshold = 5e-3;
            evaluator_f.large_result_threshold = 2e3;

            return evaluator_f;

        }
        else {
            
            auto evaluator_f = h2__q_qb__bb_b_eb_e_default<double>{params, Q_b, Q_q};

            if(!include_one_loop_squared) {
                evaluator_f.include_one_loop_squared= false;
            }

            switch (remainders_convention) {
                case 1: {
                    evaluator_f.finite_renormalization = Vjj::FiniteRenormalizationOperator<double>{IOp_Catani_to_MSbar_filename};
                    break;
                }
                default: break;
            }

           return evaluator_f;
        }
    }
    catch (std::exception& e) {
        std::cerr << "Encountered an error during construction of h2__g_g__bb_b_eb_e:\n" << e.what() << std::endl;
        return nullptr;
    }
}



} // FivePointAmplitudes
