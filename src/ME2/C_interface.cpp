#include "C_interface.h"

#include "Spinor.h"
#include "CheckedEvaluator.h"

//TODO: generate these includes at configuration stage

#ifdef FivePointAmplitudes_2q3y_ENABLED 
#include "ME2/h2__q_qb__y_y_y.hpp"
#endif // FivePointAmplitudes_2q3y_ENABLED
#ifdef FivePointAmplitudes_Vjj_ENABLED
#include "ME2/h2__g_g__bb_b_eb_e.hpp"
#include "ME2/h2__q_qb__bb_b_eb_e.hpp"
#endif // FivePointAmplitudes_Vjj

#ifdef FivePointAmplitudes_3j_ENABLED 
#include "ME2/H2Evaluator.h"
#endif // FivePointAmplitudes_3j_ENABLED


#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <matchit/matchit.h>

namespace FivePointAmplitudes {


static std::unordered_map<int, std::pair<std::vector<int>, H_EvaluatorFunctionType_Interface>> initialized_processes; 
static int new_process_id = 1;


template <typename... Ts> auto get_evaluator_from_pdg_list(const std::vector<int>& particles_pdg, Ts&& ...args) {
    using namespace matchit;

    using std::vector;

    /// Each process must implement a function which returns a function object that evaluates it
    return match(particles_pdg) (
#ifdef FivePointAmplitudes_2q3y_ENABLED 
pattern | vector{1,-1,22,22,22} = h2__q_qb__y_y_y_construct,
#endif // FivePointAmplitudes_2q3y_ENABLED
#ifdef FivePointAmplitudes_Vjj_ENABLED 
pattern | vector{21,21, -5, 5, -11, 11} = h2__g_g__bb_b_eb_e_construct,
pattern | vector{1,-1, -5, 5, -11, 11} = h2__q_qb__bb_b_eb_e_construct,
pattern | vector{2,-2, -5, 5, -11, 11} = h2__q_qb__bb_b_eb_e_construct,
#endif // FivePointAmplitudes_Vjj_ENABLED
#ifdef FivePointAmplitudes_3j_ENABLED 
pattern | vector{21, 21, 21, 21, 21} = jjj::h2__p_p__j_j_j_construct,

pattern | vector{-1, 1, 21, 21, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, 21, -1, 21, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{21, 21, 1, -1, 21} = jjj::h2__p_p__j_j_j_construct,

pattern | vector{-1, 1, 2, -2, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, 2, 2, -1, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, -2, -2, -1, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, 21, -1, 2, -2} = jjj::h2__p_p__j_j_j_construct,

pattern | vector{-1, 1, 1, -1, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, -1, -1, -1, 21} = jjj::h2__p_p__j_j_j_construct,
pattern | vector{-1, 21, -1, 1, -1} = jjj::h2__p_p__j_j_j_construct,
#endif // FivePointAmplitudes_3j_ENABLED
            pattern | _ = std::function<H_EvaluatorFunctionType_Interface(const std::vector<int>&, Ts...)>{[](const std::vector<int>&,Ts...){return nullptr;}}
            )(particles_pdg, std::forward<Ts>(args)...);
}


}



extern "C" {

int initialize_process(int n_particles, int* particles_pdg, int use_rescue, double* input_parameters, int* int_options, double* double_options) {

    using namespace FivePointAmplitudes;

    std::vector<int> pdg_codes;
    std::string process_str{};

    for (int i = 0; i < n_particles; ++i) {
        pdg_codes.push_back(particles_pdg[i]);
        process_str += std::to_string(particles_pdg[i])+" ";
        if(i==1) process_str += "-> ";
    }

    process_str.pop_back();


    std::cout << "FivePointAmplitudes++ interface: initializing process " << process_str << "\n";

    auto f = get_evaluator_from_pdg_list(pdg_codes, use_rescue, input_parameters, int_options, double_options);

    if (!f) {
        std::cerr << "initialization failed!" << std::endl;
        return 0;
    }

    initialized_processes[new_process_id] = {pdg_codes, f};

    return new_process_id++;
}

int evaluate_process(int process_id, double* momenta_components, double mu, double* result_out) {
    using namespace FivePointAmplitudes;

    if (initialized_processes.count(process_id) == 0) {
        std::cerr << "Process with ID " << process_id << " does not exist!" << std::endl;
        return 1;
    }

    const auto& [pro, f] = initialized_processes.at(process_id);

    mom_conf<double> mc;

    try {
        for (size_t i = 0; i < pro.size(); ++i) {
            mc.momenta.emplace_back(momenta_components[4 * i + 0], momenta_components[4 * i + 1], momenta_components[4 * i + 2], momenta_components[4 * i + 3]);
        }

        //dbg::g_floating_point_format<<std::setprecision(17);

        auto res = f(mc, mu);

        std::vector<double> result_flat;

        for(auto& ii : res) {
            for(auto& jj : ii) {
                result_flat.push_back(jj);
            }
        }

        //dbg(result_flat);

        for (size_t i = 0; i < result_flat.size(); ++i) {
            result_out[i] = result_flat[i];
        }

        return 0;
    }
    catch (std::exception& e) {
        std::cerr << "Encountered an error during evaluation of process ID " << process_id << "\n" << e.what() << std::endl;
        return 1;
    }


    return 0;
}


void deinitialize_process(int process_id) {
    FivePointAmplitudes::initialized_processes.erase(process_id);
}

}
