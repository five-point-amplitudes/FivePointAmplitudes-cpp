/******************************************************************************************************
 * Bare-bones C interface for FivePointAmplitudes++
 *
 * Suitable from cross-language linkage, see e.g. the fortran module.
 ******************************************************************************************************
 *
 * To evaluate a process, it first must be initialized through the function `initialize_process`.
 * The initialization might take much longer than the evaluation and must be performed only once.
 * The function returns a positive integer `process id`, which the user should keep to refer to the requested process. 
 * If 0 is returned, an error has occurred.
 *
 * To actually evaluate the process, use the `evaluate_process` function.
 *
 * NOTE: the user is responsible for all memory allocation (and deallocations) required to communicate with the interface. 
 * There user also must ensure that *all* options that the needed process requires are indeed passed to the interface. 
 *
 * Generally PDG codes are used to identify the process, assuming the SM particles.
 * 1001,1002,... are containers for QCD quarks that are reserved for when their EW charges are either not important, or should be set through options.
 *
 */
#pragma once
#include "../FivePointAmplitudes_config.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/**
 * Initialize process
 *
 * @param[in] n_particles     Number of particles in the process.
 * @param[in] particles_pdg     PDG(ish) list of particle codes for the process.
 *                                 It must match exactly one of the ordered lists of particles implemented in the library.
 *                                 Mapping different flavor classes to the template process, 
 *                                 or taking appropriate permutations of momenta should be done on the user's side.
 * @param[in] use_rescue     Enable precision rescue system (requires `lqd`).
 * @param[in] input_parameters     Scheme input parameters, can be empty if not required for the process. 
 *                                     Should be given in the order as declared in the struct EW_Input_Parameters.
 * @param[in] int_options     Process-specific integer options (see process implementation). 
 * @param[out] double_options     Process-specific double options (see process implementation).
 *
 * @return Process ID. Returns 0 if an error has occurred.
 */
int initialize_process(int n_particles, int* particles_pdg, int use_rescue, double* input_parameters, int* int_options, double* double_options);

/**
 * Evaluate process
 * 
 * @param[in] process_id     The process ID received from `initialize_process`.
 * @param[in] momenta_components     Momenta of all particles in the process, 
 *     given consecutively in one array in exactly the same order as the process was initialized.
 * @param[in] mu     Regularization scale (not squared)
 * @param[out] eval_result     Evaluation results will be written to this array, 
 *     the exact meaning of the coefficients is process-dependent.
 * 
 * @return a positive integer if an error has occurred during the evaluation, 0 otherwise.
 */
int evaluate_process(int process_id, double* momenta_components, double mu, double* result);

/**
 * Deinitialize process (free memory)
 *
 * @param process_id The process ID received from `initialize_process`
 */
void deinitialize_process(int process_id);

#ifdef __cplusplus
}
#endif
