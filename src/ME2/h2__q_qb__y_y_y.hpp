/**
 * Squared helicity/color summed process h2__q_qb__y_y_y
 */
#pragma once
#include "FivePointAmplitudes_config.h"

#include "../FunctionsManager.h"
#include "remainders/2q3y/RemaindersList.h"
#include "../RemainderEvaluatorSpinor.h"

#include "../PrecisionCast.h"

#include "CheckedEvaluator.h"
#include "aux.hpp"

#include <exception>
#include <memory>

namespace FivePointAmplitudes{


template <typename T> class h2__q_qb__y_y_y {
  public:
    using C = std::complex<T>;

    static constexpr KinType kin_type = KinType::m1;

  private:
    std::shared_ptr<FunctionsManager> f_manager;

    using f_norm_sqType = T (*)(const sij5<T>&);

    struct contribution {
        int tree_norm;
        f_norm_sqType f_norm_sq;
        std::vector<int> perm;
        std::vector<std::shared_ptr<SpinorRemainderBase<T>>> r_1l;
        std::vector<std::shared_ptr<SpinorRemainderBase<T>>> r_2l;
    };

    std::vector<contribution> contributions;

  public:
    using ReturnType = std::vector<std::vector<T>>;
    h2__q_qb__y_y_y(const std::array<bool, 4>& include_2l_contributions = {true, true, true, true}) : h2__q_qb__y_y_y(include_2l_contributions, std::make_shared<FunctionsManager>()) {}
    h2__q_qb__y_y_y(const h2__q_qb__y_y_y<T>&) = default;
    h2__q_qb__y_y_y(h2__q_qb__y_y_y<T>&&) = default;
    h2__q_qb__y_y_y(const std::array<bool, 4>& include_2l_contributions, std::shared_ptr<FunctionsManager> _f);
    /**
     * Evaluate from momenta mc and scale mu.
     * NOTE: the scale has energy dimension 1, not 2.
     * If the scale is not given, it will be set to sqrt(s_12).
     *
     * NOTE: see the publication to understand for definition of what the contributions are.
     * They do not include any color factors or couplings.
     * Some minor assembly is still required to construct the complete hard function.
     */
    ReturnType operator()(mom_conf<T> mc, T mu = 0);

    std::vector<Permutation> get_permutations() const {
        std::set<Permutation> perms;
        for(auto& it : contributions) {
            perms.insert(it.perm);
        }
        return {perms.begin(), perms.end()};
    }

};

template <typename T>
h2__q_qb__y_y_y<T>::h2__q_qb__y_y_y(const std::array<bool, 4>& include_2l_contributions, std::shared_ptr<FunctionsManager> _f) : f_manager(_f) {
    using sp = std::shared_ptr<SpinorRemainderBase<T>>;

    std::vector<std::vector<int>> perms;

    perms = {{1, 2, 3, 4, 5}, {2, 1, 3, 4, 5}};

    for (auto perm : perms) {
        contributions.push_back({0, qp_qm_p_p_p_norm_squared<T>, perm, {sp{new remainder_2q3y_1l__A_1__pmppp<T>(perm, f_manager)}}, {}});

        if (include_2l_contributions[0])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_0__pmppp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[1])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_1__pmppp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[2])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_nf__pmppp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[3])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_nfqq__pmppp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);
    }

    perms = {{1, 2, 3, 4, 5}, {1, 2, 4, 3, 5}, {1, 2, 5, 3, 4}, {2, 1, 3, 4, 5}, {2, 1, 4, 3, 5}, {2, 1, 5, 3, 4}};

    for (auto perm : perms) {
        contributions.push_back({1, qp_qm_m_p_p_tree_squared<T>, perm, {sp{new remainder_2q3y_1l__A_1__pmmpp<T>(perm, f_manager)}}, {}});

        if (include_2l_contributions[0])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_0__pmmpp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[1])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_1__pmmpp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[2])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_nf__pmmpp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);

        if (include_2l_contributions[3])
            contributions.back().r_2l.emplace_back(new remainder_2q3y_2l__A_2_nfqq__pmmpp<T>(perm, f_manager));
        else
            contributions.back().r_2l.emplace_back(nullptr);
    }
}

template <typename T> typename h2__q_qb__y_y_y<T>::ReturnType h2__q_qb__y_y_y<T>::operator()(mom_conf<T> mc, T mu) {

    // The hard function is dimensionless by construction, so we first rescale the input momenta
    if (mu != T(0)) {
        mc = mc.rescale(mu);
    }
    else {
        using std::sqrt;
        mu = sqrt(mc.s(1,2));
        mc = mc.rescale(mu);
    }

    using std::pow;
    T scale_restore = pow(mu, 2*mc.size()-8);

    mu = T(1); // we already rescaled, so set to 1
    
    if (!mc.is_onshell_q()) throw std::runtime_error("The input phase-space spoint appears to be corrupted (failed onshell check)!");

    // Perform a random boost such that there are no exact zeroes in momenta components (this is not nice for spinor states)
    static const std::array<T,3> beta_boost = {T(0.21), T(0.19),T(0.23)}; 
    mc = lorentz_boost(mc, beta_boost);

    // Finally we correct the momentum configuration in case it was not on-shell good enough for double precision.
    mc = increase_precision<T>(mc);

    std::vector<std::vector<std::vector<C>>> results;
    results.reserve(contributions.size() * 2);

    sij5<T> sij(mc);

    auto eval_r = [&](auto ri, auto& v) {
        if (ri)
            v.push_back(ri->eval(mc, mu));
        else
            v.emplace_back();
    };

    auto compute_contributions = [&]() {
        for (auto [norm, f_norm_sq, perm, r_1l, r_2l] : contributions) {
            auto sij_perm = sij.permute(perm);

            std::vector<C> a_0{static_cast<C>(norm), f_norm_sq(sij_perm)};
            std::vector<C> a_1;
            std::vector<C> a_2;

            for (auto ri : r_1l) eval_r(ri, a_1);
            for (auto ri : r_2l) eval_r(ri, a_2);

            results.push_back({a_0, a_1, a_2});
        }
    };

    compute_contributions();

    // same but parity conjugated
    mc = mc.parity_conjugate();
    compute_contributions();

    std::vector<std::vector<T>> result{{0}, {0}, {0, 0, 0, 0}};

    for (auto& a : results) {
        using namespace auxiliary;
        result[0][0] += abs2(a[0][0]) * real(a[0][1]);
    }

    for (auto& a : results) { a[0][1] /= result[0][0]; }

    for (auto& a : results) {
        using namespace auxiliary;
        result[1][0] += real(a[0][1]) * real(a[0][0] * conj(a[1][0])) * T(2);
        result[2][0] += real(a[0][1]) * (abs2(a[1][0]) + real(a[0][0] * conj(a[2][0])) * T(2));
        result[2][1] += real(a[0][1]) * (abs2(a[1][0]) + real(a[0][0] * conj(a[2][1])) * T(2));
        result[2][2] += real(a[0][1]) * real(a[0][0] * conj(a[2][2])) * T(2);
        result[2][3] += real(a[0][1]) * real(a[0][0] * conj(a[2][3])) * T(2);
    }

    result[0][0] /= scale_restore;

    return result;
}

H_EvaluatorFunctionType_Interface h2__q_qb__y_y_y_construct(const std::vector<int>& pdg_codes, int use_checked, double* i_c, int* int_options, double* double_options);

} // FivePointAmplitudes
