#include "FivePointAmplitudes_config.h"

#include "h2__q_qb__y_y_y.hpp"

#include <iostream>
#include <memory>
#include <stdexcept>


namespace FivePointAmplitudes {

H_EvaluatorFunctionType_Interface h2__q_qb__y_y_y_construct(const std::vector<int>& pdg_codes, int use_checked, double* input_parameters, int* i_c, double* double_options) {
    try {
        if (use_checked) {
            return CheckedEvaluator<h2__q_qb__y_y_y>{std::array{(bool)i_c[0], (bool)i_c[1], (bool)i_c[2], (bool)i_c[3]}};
        }
        else {
            return h2__q_qb__y_y_y<double>{std::array{(bool)i_c[0], (bool)i_c[1], (bool)i_c[2], (bool)i_c[3]}};
        }
    }
    catch (std::exception& e) {
        std::cerr << "Encountered an error during construction of h2__q_qb__y_y_y:\n" << e.what() << std::endl;
        return nullptr;
    }
}

}
