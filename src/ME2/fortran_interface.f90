!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Fortran interface for FivePointAmplitudes++
!
! Provides module `FivePointAmplitudes`
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Compilation of the module can be done with the command  
! ```
! gfortran fortran_interface.f90 -c
! ```
!
! To link with your program add you will need to add the following arguments to your link command
! ```
! fortran_interface.o -lstdc++ -lqd -lFivePointAmplitudes -lPentagonFunctions -lLi2++
! ```
! and make sure that those libraries can be found at runtime (e.g. through setting `$LD_LIBRARY_PATH`).
!
! The functions in the module match the C interface of FivePointAmplitudes++ defined in the file `C_interface.h`.
! See the documentation therein for details.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module FivePointAmplitudes
  implicit none 

  interface

    integer(c_int) function &
        initialize_process(n_particles, particles_pdg, use_rescue, input_parameters, int_options, double_options) bind(c)
      use, intrinsic :: iso_c_binding
      integer(c_int), value :: n_particles
      integer(c_int) :: particles_pdg(*)
      integer(c_int), value :: use_rescue
      real(c_double) :: input_parameters(*)
      integer(c_int) :: int_options(*)
      real(c_double) :: double_options(*)
    end function initialize_process

    integer(c_int) function &
        evaluate_process(process_id, momenta_components, mu, eval_result) bind(c)
      use, intrinsic :: iso_c_binding
      integer(c_int), value :: process_id
      real(c_double) :: momenta_components(*)
      real(c_double), value :: mu
      real(c_double), dimension(*), intent(out) :: eval_result
    endfunction evaluate_process

    subroutine deinitialize_process(process_id) bind(c)
      use, intrinsic :: iso_c_binding
      integer(c_int), value :: process_id
    end subroutine deinitialize_process


  end interface

end module FivePointAmplitudes
