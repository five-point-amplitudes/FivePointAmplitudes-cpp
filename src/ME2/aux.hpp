#pragma once
#include <complex>
#include "../Kinematics.h"

namespace FivePointAmplitudes {
namespace auxiliary {

template <typename T> T abs2(const std::complex<T>& z) { return (conj(z) * z).real(); }

template <typename T, typename F1> T eval_h1_h(F1 F1l, const sij5<T>& sij) {
    auto me2_h = F1l(sij, 1).real() + F1l(sij, -1).real();
    me2_h *= 2;

    return me2_h;
}

template <typename T, typename F1, typename F2> T eval_h2_h(F1 F1l, F2 F2l, const sij5<T>& sij) {
    auto me2_h = F2l(sij, 1).real() + F2l(sij, -1).real();
    me2_h *= 2;

    me2_h += abs2(F1l(sij, 1));
    me2_h += abs2(F1l(sij, -1));

    return me2_h;
}

} // namespace auxiliary
    
} // FivePointAmplitudes
