#pragma once
#include "FivePointAmplitudes_config.h"

#include "Spinor.h"
#include "remainders/Vjj/functions.hpp"

#include "CheckedEvaluator.h"
#include "Parameters.h"
#include "Particles.h"

namespace FivePointAmplitudes {

template <typename T, typename Tcoeffs> struct h2__g_g__bb_b_eb_e {
  public:
    using C = std::complex<T>;
    using ReturnType = std::vector<std::vector<T>>;

    static constexpr KinType kin_type = KinType::m1;

  private:

    struct Contribution {
        std::string v_L;
        std::string v_q;
        std::string hel;
        Permutation perm;
    };

    using ContributionType = std::vector<Vjj::Vjj_remainder<T, Tcoeffs>>;

    const std::vector<Contribution> contributions_e_L = {
        Contribution{"e_L", "q_L", "gluonspp", Permutation{3,2,1,4,5,6}}, //  ++ +- +-
        Contribution{"e_R", "q_R", "gluonspp", Permutation{4,1,2,3,6,5}}, //  ++ +- +-
        Contribution{"e_L", "q_L", "gluonsmp", Permutation{3,2,1,4,5,6}}, //  +- +- +-
        Contribution{"e_L", "q_L", "gluonspm", Permutation{3,2,1,4,5,6}}, //  -+ +- +-

        // 1<->2 gluons 
        Contribution{"e_L", "q_L", "gluonspp", Permutation{3,1,2,4,5,6}}, //  ++ +- +-
        Contribution{"e_R", "q_R", "gluonspp", Permutation{4,2,1,3,6,5}}, //  ++ +- +-
        Contribution{"e_L", "q_L", "gluonsmp", Permutation{3,1,2,4,5,6}}, //  +- +- +-
        Contribution{"e_L", "q_L", "gluonspm", Permutation{3,1,2,4,5,6}}, //  -+ +- +-
    };


    std::vector<std::vector<ContributionType>> remainders;

    EW_Input_Parameters params;
    T Q_b;

    std::shared_ptr<VM_Cache<Tcoeffs>> c_cache;
    std::shared_ptr<FunctionsManagerG<kin_type>> f_cache;
    
  public:
    /**
     * Set this function to a function object to apply finite renormalization at the level of each partial helicity remainder, before squaring them.
     * Note that this function should be implemented as if it would be applied to the partials (1,2,3,4,5,6) defined in eqs. (2.1,2.2,2.5,2.6,2.11) of [arXiv:2110.07541v3]
     * The permuations five to it must be then use to map the kinematics appropriately.
     *
     * The function must return the finite renormalization as a double series in `alpha_s` and `Nf/Nc` (see eq. (2.17))
     *
     * The arguments are 
     * mc - original momenta
     * mu - renormalization scale
     * perm - permuation that defineds the partial amplitude
     */
    std::function<std::vector<std::vector<C>>(const mom_conf<T>& mc, const T& mu, const Permutation& perm)> finite_renormalization = nullptr;

    bool include_one_loop_squared = true;

    bool large_cancellations_flag{false};

    h2__g_g__bb_b_eb_e(EW_Input_Parameters _params, T _Q_b, decltype(finite_renormalization) _f = nullptr) : params(_params), Q_b(_Q_b), finite_renormalization(_f) {
        remainders.emplace_back();
        remainders.emplace_back(2);
        remainders.emplace_back(3);

        c_cache = std::make_shared<VM_Cache<Tcoeffs>>();
        f_cache = std::make_shared<FunctionsManagerG<kin_type>>(true);

        for(auto& a : contributions_e_L) {
        for (size_t li = 1; li <= 2; ++li) {
            for (size_t i = 0; i <= li; ++i) {
                std::string nfi = std::to_string(i);
                    remainders[li][i].push_back({"Vjj_"+std::to_string(li)+"l_"+a.hel + nfi, a.perm, c_cache, f_cache});
                }
            }
        }

        for(auto& ri : remainders){
            for (auto& it : ri) {
                for (auto& jt : it) {
                    jt.construct();
                }
            }
        }
    }

    ReturnType operator()(mom_conf<T> mc, T mu) {

        using namespace ::FivePointAmplitudes::Vjj;

        large_cancellations_flag = false;
        //TODO: check large cancellations between different helicity and color contributions 

        C cw = compute_cos_theta(params);
        C propf = get_V_over_photon_propagtor<T>(params.M_Z, params.G_Z, mc.s(5,6));


        using std::pow;
        T scale_restore = pow(mu, 2*mc.size()-8);
        // The hard function is dimensionless by construction, so we first rescale the input momenta
        if (mu != T(0)) {
            mc = mc.rescale(mu);
            // We do not need to rescale EW parameters, because they enter as dimensionaless ratios only
        }
        mu = T(1); // we already rescaled, so set to 1


        if (!mc.is_onshell_q()) throw std::runtime_error("The input phase-space spoint appears to be corrupted (failed onshell check)!");

        // Perform a random boost such that there are no exact zeroes in momenta components (this is not nice for spinor states)
        static const std::array<T,3> beta_boost = {T(0.21), T(0.19),T(0.23)}; 
        mc = lorentz_boost(mc, beta_boost);

        using std::vector;

        vector<vector<T>> result = {{T{0}}, {T{0},T{0}}, {T{0},T{0},T{0}}};
        c_cache -> flush();
        f_cache -> flush();

        for (size_t ii = 0; ii < contributions_e_L.size(); ++ii) {
            auto& a = contributions_e_L.at(ii);

            auto add_contributions = [&](const auto& mc, auto f_l, auto f_q) {
                vector<vector<C>> contribution = {{C{0}}, {C{0},C{0}}, {C{0},C{0},C{0}}};
                auto mc_perm = mc.permute(a.perm);

                contribution[0][0] =
                    (-Q_b + Vjj::get_v_coupling(f_l(a.v_L), C(Q_b), cw) * Vjj::get_v_coupling(f_q(a.v_q), C(Q_b), cw) * propf) * get_tree("Vjj_1l_" + a.hel + "0", mc_perm);

                for (size_t li = 1; li <= 2; ++li) {
                    for (size_t nfi = 0; nfi <= li; ++nfi) {
                        contribution[li][nfi] = contribution[0][0] * remainders[li][nfi].at(ii)(mc, mu); 
                        large_cancellations_flag = large_cancellations_flag || remainders[li][nfi].at(ii).large_cancellations_flag;
                    }
                }

                if (finite_renormalization) { contribution = finite_renormalize(contribution, finite_renormalization(mc, mu, a.perm),2); }

                add_squared_contribution(result, contribution, include_one_loop_squared);

            };

            add_contributions(mc, id, id);
            add_contributions(mc.parity_conjugate(), Vjj::swap_v_L, Vjj::swap_v_q);
            add_contributions(mc.permute({1, 2, 3, 4, 6, 5}), Vjj::swap_v_L, id);
            add_contributions(mc.permute({1, 2, 3, 4, 6, 5}).parity_conjugate(), id, Vjj::swap_v_q);
        }


        for(auto& it : result[1]) it /= result[0][0];
        for(auto& it : result[2]) it /= result[0][0];

        result[0][0] /= scale_restore;

        return result;
    }

    std::vector<Permutation> get_permutations() const {
        std::set<Permutation> perms;
        for(auto& it : contributions_e_L) {
            perms.insert(it.perm);
        }
        return {perms.begin(), perms.end()};
    }

};

H_EvaluatorFunctionType_Interface h2__g_g__bb_b_eb_e_construct(const std::vector<int>& pdg_codes, int use_checked, double* input_parameters, int* int_options, double* double_options);

}
