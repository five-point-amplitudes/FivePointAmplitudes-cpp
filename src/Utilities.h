#pragma once

#ifdef FivePointAmplitudes_QD_ENABLED 
#include <qd/qd_real.h>
#endif // FivePointAmplitudes_QD_ENABLED


#include <complex>
#include <limits>
#include <type_traits>
#include <vector>

namespace FivePointAmplitudes {

/**
 * Make non-complex type if it was complex
 */
template <typename T> struct is_complex : std::false_type {};
template <typename T> struct is_complex<std::complex<T>> : std::true_type {};
template <typename T> static constexpr bool is_complex_v = is_complex<T>::value;

template <typename T, typename = void> struct remove_complex;
template <typename T> struct remove_complex<T, typename std::enable_if<!is_complex<T>::value>::type> { typedef T type; };
template <typename T> struct remove_complex<T, typename std::enable_if<is_complex<T>::value>::type> { typedef typename T::value_type type; };
template <typename T> using remove_complex_t = typename remove_complex<T>::type;

template <typename T, typename = void> struct add_complex;
template <typename T> struct add_complex<T, typename std::enable_if<is_complex<T>::value>::type> { typedef T type; };
template <typename T> struct add_complex<T, typename std::enable_if<!is_complex<T>::value>::type> { typedef typename std::complex<T> type; };
template <typename T> using add_complex_t = typename add_complex<T>::type;

template <typename T> std::complex<T> operator+(int i, const std::complex<T>& z) { return {z.real() + i, z.imag()}; }
template <typename T> std::complex<T> operator+(const std::complex<T>& z, int i) { return {z.real() + i, z.imag()}; }
template <typename T> std::complex<T> operator*(int i, const std::complex<T>& z) { return {z.real() * i, z.imag() * i}; }
template <typename T> std::complex<T> operator*(const std::complex<T>& z, int i) { return {z.real() * i, z.imag() * i}; }
template <typename T> std::complex<T> operator/(int i, const std::complex<T>& z) { return static_cast<T>(i)/z;}
template <typename T> std::complex<T> operator/(const std::complex<T>& z, int i) { return {z.real() / i, z.imag() / i}; }

template <typename T>
static const T root_epsilon = sqrt(std::numeric_limits<T>::epsilon());

using std::complex;

/**
 * Snippet taken from boost:
 * https://www.boost.org/doc/libs/1_55_0/doc/html/hash/reference.html#boost.hash_combine
 */
template <typename T>
inline void hash_combine(std::size_t& seed, const T& v) {
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

/**
 * Just a function object which is identity.
 */
inline auto id (std::string x) -> std::string {return x;}


} // namespace FivePointAmplitudes


namespace std {
    template<typename A, typename B>
      struct hash<pair<A,B>>{
        size_t operator() (const pair<A,B>& x) const{
	    hash<std::remove_cv_t<A>> a_hasher;
	    size_t result = a_hasher(x.first);
	    ::FivePointAmplitudes::hash_combine(result, x.second);
	    return result;
	}

    };
    template<typename T>
    struct hash<vector<T>>{
	size_t operator() (const vector<T>& xs) const{
	    hash<size_t> hasher;
	    size_t result = hasher(xs.size());
	    for (auto& x : xs){
	      ::FivePointAmplitudes::hash_combine(result, x);
	    }
	    return result;
	}

    };

#ifdef FivePointAmplitudes_QD_ENABLED
    template <> struct hash<dd_real> {
        size_t operator()(const dd_real& y) const {
            size_t result = 1;
            ::FivePointAmplitudes::hash_combine(result, y.x[0]);
            ::FivePointAmplitudes::hash_combine(result, y.x[1]);
            return result;
        }
    };
    template <> struct hash<qd_real> {
        size_t operator()(const qd_real& y) const
        {
            size_t result = 1;
            ::FivePointAmplitudes::hash_combine(result, y.x[0]);
            ::FivePointAmplitudes::hash_combine(result, y.x[1]);
            ::FivePointAmplitudes::hash_combine(result, y.x[2]);
            ::FivePointAmplitudes::hash_combine(result, y.x[3]);
            return result;
        }
    };
#endif // FivePointAmplitudes_QD_ENABLED

} // std
