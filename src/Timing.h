#pragma once
#include <chrono>
#include <iostream>
#include <string>

#ifdef TIMING_ON

//! start (create if necessary) counter
#define START_TIMER(NAME)                                                                                                                                      \
    static timing::counter __timing_counter_##NAME(#NAME);                                                                                                     \
    __timing_counter_##NAME.start();

#define STOP_TIMER(NAME) __timing_counter_##NAME.stop();

#define TIMING(what, expr)                                                                                                                                     \
    {                                                                                                                                                          \
        std::cout << what << "..." << std::endl;                                                                                                               \
        timing::counter __tmp_counter(#what);                                                                                                                  \
        __tmp_counter.start();                                                                                                                                 \
        expr \
        __tmp_counter.stop();                                                                                                                             \
    }

#else

#define START_TIMER(NAME)
#define STOP_TIMER(NAME)
#define TIMING(what, expr) { expr }

#endif

namespace timing {

#ifdef TIMING_ON

class counter {
  private:
    using clock = std::chrono::high_resolution_clock;
    using duration_type = std::chrono::duration<double>;

    std::string name;
    std::string type{};

    duration_type total{0};
    decltype(clock::now()) interval_begin;

  public:
    void start();
    void stop();
    void report();
    duration_type get_duration() const;

    counter() = delete;
    counter(std::string&& n) : name(n) {}
    counter(std::string&& n, std::string&& t) : name(n), type(t) {}
    ~counter();
};

extern counter _rational_functions_timing;
extern counter _transcendental_functions_timing;
extern counter _summations_timing;
#endif

} // namespace timing
