#include "Spinor.h"

namespace FivePointAmplitudes {

template class momentum<double>;
template class spinor<complex<double>>;
template class la<complex<double>>;
template class lat<complex<double>>;
template class smatrix<complex<double>>;

template complex<double> spaa<double>(const momentum<double>& p1,const momentum<double>& p2);
template complex<double> spbb<double>(const momentum<double>& p1,const momentum<double>& p2);
template complex<double> spab<double>(const momentum<double>& p1,const momentum<double>& p2,const momentum<double>& p3);
template complex<double> spba<double>(const momentum<double>& p1,const momentum<double>& p2,const momentum<double>& p3);

template momentum<double> lorentz_boost(const momentum<double>&, std::array<double,3>);

#ifdef FivePointAmplitudes_QD_ENABLED
template class momentum<dd_real>;
template class spinor<complex<dd_real>>;
template class la<complex<dd_real>>;
template class lat<complex<dd_real>>;
template class smatrix<complex<dd_real>>;

template complex<dd_real> spaa<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2);
template complex<dd_real> spbb<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2);
template complex<dd_real> spab<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2,const momentum<dd_real>& p3);
template complex<dd_real> spba<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2,const momentum<dd_real>& p3);

template class momentum<qd_real>;
template class spinor<complex<qd_real>>;
template class la<complex<qd_real>>;
template class lat<complex<qd_real>>;
template class smatrix<complex<qd_real>>;

template complex<qd_real> spaa<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2);
template complex<qd_real> spbb<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2);
template complex<qd_real> spab<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2,const momentum<qd_real>& p3);
template complex<qd_real> spba<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2,const momentum<qd_real>& p3);

template momentum<dd_real> lorentz_boost(const momentum<dd_real>&, std::array<dd_real,3>);
template momentum<qd_real> lorentz_boost(const momentum<qd_real>&, std::array<qd_real,3>);
#endif

double large_cancellations_threshold = 10e-7;

} // namespace FivePointAmplitudes

