/**
 * This file is a modified version of a part of Caravel (https://gitlab.com/caravel-public/caravel) distributed under GPLv3.
 */
#pragma once
#include <cmath>
#include <string>
#include <vector>
#include <sstream>
#include <exception>

namespace FivePointAmplitudes {

struct MathList {
    std::string head{"MathList"};
    std::vector<std::string> tail{};
    MathList() = default;
    MathList(const std::string& parent_string, const std::string& forced_head = "");
    MathList(const char* parent_string) : MathList(std::string{parent_string}) {}
    /**
     * Direct accessor of tail, 0-based.
     */
    std::string operator[](size_t position) const; 
    /**
     * Get size of tail.
     */
    size_t size() const;
    // iterators 
    auto begin() const { return tail.begin(); }
    auto end() const { return tail.end(); }
    auto begin() { return tail.begin(); }
    auto end() { return tail.end(); }
    auto cbegin() const { return tail.cbegin(); }
    auto cend() const { return tail.cend(); }
};

std::string read_file(std::string filename);

template <typename T> T get_rational_number(const std::string& s_in) {

    T num;
    T den = 1;

    {
        auto pos = s_in.find("/");
        if ( pos == std::string::npos) {
            std::stringstream in(s_in);
            in >> num;
            if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
        }
        else {
            {
                std::stringstream in(s_in.substr(0,pos));
                in >> num;
                if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
            }
            {
                std::stringstream in(s_in.substr(pos+1,s_in.size()));
                in >> den;
                if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
            }
        }
    }

    using std::isfinite;

    if (!isfinite(num) or !isfinite(den)) { throw std::invalid_argument("Cannot iterpret " + s_in + "!\""); }

    return num/den;

}
std::ostream& operator<<(std::ostream&, const MathList&);

} // namespace FivePointAmplitudes
