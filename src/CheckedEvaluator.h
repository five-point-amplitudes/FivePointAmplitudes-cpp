#pragma once

#include <PentagonFunctions_config.h>
#include <FivePointAmplitudes_config.h>

//FIXME:  should not need to include these evaluators, 
// for now they are included just for the relevant letters evaluation
#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
#include "RemainderEvaluator.h"
#include "RemainderEvaluatorSpinor.h"
#endif // PENTAGON_FUNCTIONS_M0_ENABLED
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
#include "remainders/Vjj/functions.hpp"
#endif // PENTAGON_FUNCTIONS_M1_ENABLED

#include "PrecisionCast.h"

#include <algorithm>
#include <cmath>
#include <array>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>
#include <complex>
#include <functional>

#include <Kin.h>


namespace FivePointAmplitudes {

using ::PentagonFunctions::KinType;

using H_EvaluatorFunctionType_Interface = std::function<std::vector<std::vector<double>>(mom_conf<double> mc, double mu)>;

namespace detail {

template <typename T> constexpr auto has_large_cancellations_flag_impl(T* obj) -> decltype(obj->large_cancellations_flag, std::true_type{});
template <typename T> std::false_type has_large_cancellations_flag_impl(...);
template <typename T> constexpr bool has_large_cancellations_flag_v = decltype(has_large_cancellations_flag_impl<T>(std::declval<T*>()))::value;


template <typename T, size_t N> std::array<T,N> get_perturbed_point(std::array<T, N> sij) {
    using std::nextafter;
    for (auto& it : sij) it = nextafter(it,0);
    return sij;
}

// for momenta configurations we perform a "random" lorentz_boost
template <typename T>  mom_conf<T> get_perturbed_point(const mom_conf<T>& psp) {
    static const std::array<T,3> boost_beta{T{0.11},T{0.15},T{-0.14}};

    return lorentz_boost(psp, boost_beta);
}

template <typename T> T maximal_relative_diff(const std::vector<std::vector<T>>& v1, const std::vector<std::vector<T>>& v2) {
    using std::abs;
    T toreturn = 0;

    if (v1.size() != v2.size() or v1.empty()) throw std::runtime_error("Incompatible vector dimension encountered!");

    // NOTE: we ignore the first entry, because it's a tree which is not dimensionless
    for (size_t i = 1; i < v1.size(); ++i) {

        if (v1[i].size() != v2[i].size()) throw std::runtime_error("Incompatible vector dimension encountered!");

        for (size_t j = 0; j < v1[i].size(); ++j) {
            T rel_diff = abs(1 -  v2[i][j]/v1[i][j]);
            if (rel_diff > toreturn) std::swap(rel_diff, toreturn);
        }
    }

    return toreturn;
}


// This is a convoluted way to ensure that the functions that do not define `kin_type` (the 3j planar ones) 
// still keep working and have the default KinType::m0
// TODO: remove this once there are no more functions like that left (presumable when the full color for 3j gets implemented)
template <typename T> auto defines_kin_type(int) -> decltype(T::kin_type, std::true_type{});
template <typename T> std::false_type defines_kin_type(...);
template <typename T> constexpr KinType deduce_kin_type() {
    if constexpr (decltype(defines_kin_type<T>(0))::value) {
        return T::kin_type;
    }
    else {
        return KinType::m0;
    }
}


template <KinType KT, typename K, typename T> auto get_letters_to_check(K kin, T mu, const std::vector<Permutation>& permutations) {

    if constexpr (false) {}
#ifdef PENTAGON_FUNCTIONS_M0_ENABLED 
    if constexpr (KT == KinType::m0 ) {
        // NOTE: we do NOT pass the scale here
        // In the massless case all ratios are dimensionless, so we do not need to rescale.
        auto inv_letters = detail::get_inverse_pentagon_letters(kin, T(1));
        // the last letter is ImSqrtDelta, we don't check it
        inv_letters.at(25) = T(1);
        {
            using std::abs;
            for (auto& it : inv_letters) it = 1 / abs(it);
        }

        return inv_letters;
    }
#endif // PENTAGON_FUNCTIONS_M0_ENABLED
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
    else if constexpr (KT == KinType::m1) {
        kin = kin.rescale(mu);

        std::set<T> to_check;

        auto do_permutation = [&](Permutation perm) {
            using std::abs;
            auto kin_perm = kin.permute(perm);
            auto sijs = Vjj::get_sij_variables(kin_perm);
            auto inv_letters = Vjj::get_inverse_denominators(sijs);

            for (auto& it : inv_letters) to_check.insert(1 / abs(it));
        };

        for(auto& perm : permutations) do_permutation(perm);

        return std::vector(to_check.begin(),to_check.end());
    }
#endif // PENTAGON_FUNCTIONS_M1_ENABLED
    else {
        throw std::runtime_error("Rescue system not implemented for this kinematics!");
    }
}


} // namespace detail

template <template <typename> class hh> struct CheckedEvaluator {
    double letters_threshold = 1e-3; /**< The threshold on the ratio of minimal to maximal letter values below which the point is a candidate to be unstable. */
#ifdef FivePointAmplitudes_QD_ENABLED
    double rescue_threshold = 1e-2; /**< The threshold on the estimated accuracy (from the perturbation test) which determines when the point is deemed unstable */
#else
    double rescue_threshold = 1e-1;
#endif
    double large_result_threshold = 2e3; /**< The threshold on the result, above which a second double-precision evaluation will be triggered */ 

    hh<double> h_double;

#ifdef FivePointAmplitudes_QD_ENABLED
    hh<dd_real> h_hp;
#endif // FivePointAmplitudes_QD_ENABLED

    using ReturnType = typename hh<double>::ReturnType;
    static constexpr KinType kin_type = detail::deduce_kin_type<hh<double>>();

    CheckedEvaluator() = default;
    CheckedEvaluator(CheckedEvaluator&) = default;
    CheckedEvaluator(const CheckedEvaluator&) = default;
    CheckedEvaluator(CheckedEvaluator&&) = default;
    CheckedEvaluator& operator=(const CheckedEvaluator&) = default;
    CheckedEvaluator& operator=(CheckedEvaluator&&) = default;

    template <typename... Ts> CheckedEvaluator(Ts&&... args) : h_double(std::forward<Ts>(args)...)
        #ifdef FivePointAmplitudes_QD_ENABLED 
        , h_hp(std::forward<Ts>(args)...)
        #endif // FivePointAmplitudes_QD_ENABLED
    {
#ifdef FivePointAmplitudes_QD_ENABLED 
        ::PentagonFunctions::IntegrationTolerance<dd_real> = 1e-10;
        ::PentagonFunctions::SwitchToHPThreshold<dd_real> = 90*1e-10;
#endif // FivePointAmplitudes_QD_ENABLED
    }

    template <template <typename> class F, typename... Ts> void add_finite_renormalization(Ts... args) {
        h_double.finite_renormalization = F<double>{args...};
#ifdef FivePointAmplitudes_QD_ENABLED
        h_hp.finite_renormalization = F<dd_real>{args...};
#endif
    }
        

    size_t n_evaluated_points = 0;         /**< Number of all points evaluated. */
    size_t n_failed_letters_threshold = 0; /**< Number of evaluation that failed the initial check of letters. */
    size_t n_large_cancellations_detected = 0; 
    size_t n_large_result_detected = 0; 
    size_t n_failed_rescue_threshold = 0;  /**< Number of evaluation that failed the scaling check of letters. */

    double last_accuracy_estimate = -1; /**< Stores the accuracy estimate of the last point which failed the letters check */

    /**
     * Evaluate a squared remainder with a check and rescue with higher precision if the support for it is configured.
     * Throws an exception if the remainder was deemed unstable and no support of high precision is configured.
     *
     * @param kinematics
     * @param mu is the regularization scale, 
     *      CHECK THE UNDERLYING EVALUATOR FOR ENERGY DIMENSION. 
     *      It is of dimension 1 for everything else, 
     *      but for 3j planar hard functions it was defined to be of dimension 2!
     */
    template <typename KinematicsType> ReturnType operator()(const KinematicsType& kin, double mu) {

        auto result = h_double(kin, mu);

        bool large_cancellations_flag = false;

        // check if the result it large
        bool large_result_flag = false;
        {
            int l_order = -1;

            for(auto& ii : result) {
                l_order++;
                // don't check the tree
                if(l_order == 0) continue;

                for(auto& jj : ii ) {
                    using std::fabs;
                    if (fabs(jj) > large_result_threshold) {
                        large_result_flag = true;
                        break;
                    }
                }

                if(large_result_flag) {
                    n_large_result_detected++;
                    break;
                }
            }
        }

        //dbg(detail::has_large_cancellations_flag_v<hh<double>>);

        // if the implementation provides detection of large cancellations through a flag, we use it to initiate stability check 
        if constexpr (detail::has_large_cancellations_flag_v<hh<double>>)  {
            large_cancellations_flag = h_double.large_cancellations_flag;
            if (large_cancellations_flag) { n_large_cancellations_detected++; }
        }

        // reset the accuracy estimate
        last_accuracy_estimate = -1;

        n_evaluated_points++;

        // this function call should return all the letters (absolute values thereof) that shall be checked
        auto letters = detail::get_letters_to_check<kin_type>(kin, mu, h_double.get_permutations());

        // find the ratio Wmin/Wmax
        auto Wminmax_iterators = std::minmax_element(letters.begin(), letters.end() - 1);

        auto Wminmax = (*(Wminmax_iterators.first)) / (*(Wminmax_iterators.second));

        bool letters_threshold_failed = (Wminmax < letters_threshold); 

        if(letters_threshold_failed) n_failed_letters_threshold++;

        // potential unstable point
        if (letters_threshold_failed or large_cancellations_flag or large_result_flag) {

            n_failed_letters_threshold++;

            auto perturbed_result = h_double(detail::get_perturbed_point(kin), mu);

            last_accuracy_estimate = detail::maximal_relative_diff(result, perturbed_result);

            if (last_accuracy_estimate > rescue_threshold) {

                n_failed_rescue_threshold++;

#ifdef FivePointAmplitudes_QD_ENABLED
                auto kin_hp = increase_precision<typename hh<dd_real>::ReturnType::value_type::value_type>(kin);
                auto new_result = h_hp(kin_hp, mu);

                return to_double(new_result);
#else
                throw std::runtime_error("An unstable phase space point encountered with\n\testimated accuracy (scaling test) " +
                                         std::to_string(last_accuracy_estimate) +
                                         ",\nand cannot be rescued because the support of high-precision evaluations was not enabled.");
#endif // FivePointAmplitudes_QD_ENABLED
            }
        }

        return result;
    }
};

} // namespace FivePointAmplitudes
