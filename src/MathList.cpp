/**
 * This file is a modified version of a part of Caravel (https://gitlab.com/caravel-public/caravel) distributed under GPLv3.
 */
#include "MathList.h"

#include <fstream>
#include <iostream>

namespace FivePointAmplitudes {

MathList::MathList(const std::string& parent_string, const std::string& forced_head){
    size_t bracket_count = 0;
    std::stringstream buffer(parent_string);

    // Get the head (and check it's the correct one)
    getline(buffer, head, '[');
    bracket_count = 1;
    if ((forced_head != "" ) && (head != forced_head)){
        throw std::invalid_argument("Error while reading MathList:\n\tExpected head \"" + forced_head + "\"\n\tReceived head \"" + head + "\"");
    }

    std::stringstream current_stream;

    char c;
    while (buffer.get(c)){
        // Reckless disreguarding of whitespace (This would break
        // text arguments, if this ever got more advanced)
        if (c == '\n' || c == ' ') continue;
        if (c == '[') bracket_count++;
        if (c == ']') bracket_count--;

        if (bracket_count == 0){
            // We have finished all that is relevant of buffer, so
            // save and leave.
            auto final_string = current_stream.str();
            if (final_string.size() > 0){
                tail.push_back(std::move(final_string));
            }
            break;
        }

        if (((bracket_count == 1) && (c ==','))){
            // Push back, and clear the stream
            tail.push_back(current_stream.str());
            current_stream.clear(); // Clears state bits
            current_stream.str(""); // Empties
        }
        else current_stream << c;
    }
    if(bracket_count != 0){
        std::cerr << "SYNTAX ERROR: Missing closing ] while reading head \"" << head << "\" from \n\t\"" << parent_string << "\"";
        throw std::invalid_argument("Error while reading MathList.");
    }
    std::string test_empty;
    buffer >> test_empty;
    if(!test_empty.empty()){
        std::cerr << "SYNTAX ERROR: Unused characters after obtaining \n\t\"" << *this <<  "\"\n\tfrom\n\t\"" << parent_string << "\"";
        throw std::invalid_argument("Error while reading MathList.");
    }
}

std::string MathList::operator[](size_t position) const{
    return tail.at(position);
}

size_t MathList::size() const  {
  return tail.size();
}

std::ostream& operator<<(std::ostream& s, const MathList& l){
    s<<l.head << "[";
    if(!l.tail.empty()){
        for (size_t i = 0; i < l.size() - 1; i++) { s << l[i] << ","; }
        s << l[l.size() - 1];
    }
    s<<"]";

    return s;
}
    
std::string read_file(std::string filename){
    std::ifstream f(filename);

    if (f.is_open()){
        std::stringstream string_buffer;
        string_buffer << f.rdbuf();
        f.close();
        return string_buffer.str();
    }

    throw std::runtime_error("ERROR: could not open file " + filename);
}

} // FivePointAmplitudes

