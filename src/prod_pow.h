#pragma once

template <typename T> inline T prod_pow(T x, int n) {
    T result(1);
    for (int i = 0; i < n; i++) { result *= x; }
    return result;
}
