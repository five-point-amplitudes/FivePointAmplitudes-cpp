#pragma once

#include <array>
#include <iostream>
#include <map>
#include <stdexcept>

#include "Spinor.h"

namespace FivePointAmplitudes {

template <typename T> struct sij5 {
    static constexpr size_t N = 5;
    std::map<std::pair<int, int>, T> _s;

    sij5(const std::array<T, N>&);
    sij5(const mom_conf<T>&);

    /**
     * Get s[i,j], 1-based
     */
    T get(int i, int j) const {
        if (j < i) std::swap(i, j);
        return _s.at({i, j});
    }
    T operator()(int i, int j) const { return get(i, j); }

    /**
     * Return a copy of *this with permuted momenta according to
     * @param permutation
     */
    sij5<T> permute(const std::array<int, N>&);
    sij5<T> permute(const std::vector<int>&);
    sij5<T> permute(std::initializer_list<int>&& ll) {return  permute(std::vector<int>(std::move(ll)));};

    operator std::array<T,N>() const;
};

template <typename T> sij5<T>::sij5(const std::array<T, N>& v) {
    for (size_t i = 1; i <= N; ++i) { _s[{i, i}] = T(0); }
    _s[{1, 2}] = v[0];
    _s[{2, 3}] = v[1];
    _s[{3, 4}] = v[2];
    _s[{4, 5}] = v[3];
    _s[{1, 5}] = v[4];

    _s[{1, 3}] = -_s[{1, 2}] - _s[{2, 3}] + _s[{4, 5}];
    _s[{1, 4}] = -_s[{1, 5}] + _s[{2, 3}] - _s[{4, 5}];
    _s[{2, 4}] = _s[{1, 5}] - _s[{2, 3}] - _s[{3, 4}];
    _s[{2, 5}] = -_s[{1, 2}] - _s[{1, 5}] + _s[{3, 4}];
    _s[{3, 5}] = _s[{1, 2}] - _s[{3, 4}] - _s[{4, 5}];
}

template <typename T> sij5<T>::sij5(const mom_conf<T>& mc) {
    if(mc.size() != N) throw std::runtime_error("sij5 must be constructed from 5 massless momenta");
    _s[{1, 2}] = mc.s(1,2);
    _s[{2, 3}] = mc.s(2,3);
    _s[{3, 4}] = mc.s(3,4);
    _s[{4, 5}] = mc.s(4,5);
    _s[{1, 5}] = mc.s(1,5);

    _s[{1, 3}] = -_s[{1, 2}] - _s[{2, 3}] + _s[{4, 5}];
    _s[{1, 4}] = -_s[{1, 5}] + _s[{2, 3}] - _s[{4, 5}];
    _s[{2, 4}] = _s[{1, 5}] - _s[{2, 3}] - _s[{3, 4}];
    _s[{2, 5}] = -_s[{1, 2}] - _s[{1, 5}] + _s[{3, 4}];
    _s[{3, 5}] = _s[{1, 2}] - _s[{3, 4}] - _s[{4, 5}];
}

template <typename T> sij5<T> sij5<T>::permute(const std::array<int, N>& perm) {

    std::array<T, N> new_v;

    for (size_t i = 0; i < N; ++i) {
        auto j = (i + 1) % N;
        new_v[i] = get(perm[i], perm[j]);
    }

    return sij5<T>(new_v);
}

template <typename T> sij5<T> sij5<T>::permute(const std::vector<int>& perm) {
    if (perm.size() != N) throw std::runtime_error("permutation size for sij5 is not 5");

    std::array<int, N> new_perm;
    for (size_t i = 0; i < N; ++i) { new_perm[i] = perm.at(i); }

    return permute(new_perm);
}

template <typename T> sij5<T>::operator std::array<T, N>() const {
    std::array<T, N> toret;
    for (size_t i = 0; i < N; ++i) {
        auto j = (i + 1) % N;
        toret[i] = get(i + 1, j + 1);
    }
    return toret;
}

template <typename T> std::ostream& operator<<(std::ostream& s, const sij5<T>& sij) {
    s << "sijs : \n";
    for (auto& it : sij._s) { s << "s[" << it.first.first << "," << it.first.second << "] = " << it.second << "\n"; }
    return s;
}

} // namespace FivePointAmplitudes
