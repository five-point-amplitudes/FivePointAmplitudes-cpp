/**
 * 
 *****************************************************************************************
 *
 * This code is adapted from Caravel 
 * https://gitlab.com/caravel-public/caravel
 * which is published under GPLv3 licence.
 * A copy of the license can be obtained therein, as well as in this distribution.
 *
 *****************************************************************************************
 *
 *
 * Contains implementations of 4d momenta, la and lat spinors, slashed matrices and all products.
 * 
 * Only 2-dimensional spinors are used and dotted and undotted representations are tracked consistently.
 * The notation closely follows the one of S@M paper arXiv:0710.5559v2
 * Identifications between spinor notations:
 *
 *              |*>   <-->  la      <-->    \lambda_a
 *              <*|   <-->  Cla     <-->    \lambda^a
 *              |*]   <-->  lat     <-->    \dot\lambda^a
 *              [*|   <-->  Clat    <-->    \dot\lambda_a
 *
 *
 * Action of \gamma^5:
 *
 *              \gamma^5 |*> = |*>
 *              \gamma^5 |*] = -\gamma^5 |*]
 *
 * We implement and store only la and lat. This means that after any operation stored values for spinors will be
 * converted to lower indices. However all operations are aware of this and unless a user tries to mess with elements
 * of spinors explicitly, all operations behave as one would expect in the bracket notation like < * | * | * ] and so on.
 *
 * Most likely you only need to use spinor products
 *  spaa(1,2,3,...,n) := <1|23...|n>
 *  spab(1,2,3,...,n) := <1|23...|n]
 *  spba(1,2,3,...,n) := [1|23...|n>
 *  spbb(1,2,3,...,n) := [1|23...|n]
 * which can be called with (any number of) momenta directly and without bothering of constructing spinors explicitly.
 * They are implemented in a way that strong type safety will allow you to compile only sensible things.
 *
 * Or vectors from spinors with
 *  LvBA = [l1| \gamma^mu |l2>/2
 *  LvAB = <l1| \gamma^mu |l2]/2
 *
 * 
*/
#pragma once

#include "FivePointAmplitudes_config.h"
#include "Utilities.h"

#include <algorithm>
#include <iostream>
#include <array>
#include <stdexcept>
#include <vector>
#include <set>
#include <type_traits>

#ifdef FivePointAmplitudes_QD_ENABLED 
#include <qd/qd_real.h>
#endif // FivePointAmplitudes_QD_ENABLED


namespace FivePointAmplitudes {

template <typename T> class momentum;
template <typename T> class spinor;
template <typename T> class smatrix;
template <typename T> class la;
template <typename T> class lat;

using Permutation = std::vector<int>;
using EmbeddingMap = std::vector<std::vector<int>>;

/**
 * Simple class for 4D momentum.
 */
template <typename T> class momentum {
  public:
    static constexpr size_t N = 4;
    typedef T value_type;

    momentum(const std::array<T, 4>& set_components) : components{set_components} {}
    momentum(const T& e0, const T& e1, const T& e2, const T& e3) : components{e0, e1, e2, e3} {}

    momentum() = default;
    momentum(const momentum& other) = default;
    momentum(momentum&& other) = default;
    momentum& operator=(const momentum&) = default;
    momentum& operator=(momentum&&) = default;

    momentum<T>& operator*=(const T& x);
    momentum<T>& operator/=(const T& x);
    momentum<T>& operator+=(const momentum<T>& m2);
    momentum<T>& operator-=(const momentum<T>& m2);

    // access elements, all 0-based!
    auto& operator[](size_t i) { return components[i]; }
    auto& operator[](size_t i) const { return components[i]; }
    auto& operator()(size_t i) { return components.at(i); }
    auto& operator()(size_t i) const { return components.at(i); }

    auto begin() { return components.begin(); };
    auto end() { return components.end(); };
    auto begin() const { return components.begin(); };
    auto end() const { return components.end(); };
    auto cbegin() { return components.cbegin(); };
    auto cend() { return components.cend(); };

    constexpr size_t size() const {return components.size();}
    /**
     * Retuen a parity-conjugated momentum
     */
    momentum parity_conjugate() const { return {components[0], -components[1], -components[2], -components[3]};}

  private:
    std::array<T, N> components{};
};

template <typename T> momentum<T>& momentum<T>::operator+=(const momentum<T>& m2) {
    for (size_t i = 0; i < components.size(); ++i) { components[i] += m2[i]; }
    return *this;
}
template <typename T> momentum<T> operator-(momentum<T> m1, const momentum<T>& m2) {return m1-=m2;}

template <typename T> momentum<T>& momentum<T>::operator-=(const momentum<T>& m2) {
    for (size_t i = 0; i < components.size(); ++i) { components[i] -= m2[i]; }
    return *this;
}
template <typename T> momentum<T> operator+(momentum<T> m1, const momentum<T>& m2) {return m1+=m2;}

template <typename T> momentum<T>& momentum<T>::operator*=(const T& x) {
    for (size_t i = 0; i < components.size(); ++i) { components[i] *= x; }
    return *this;
}
template <typename T> momentum<T> operator*(momentum<T> m1, const T& x) {return m1*=x;}
template <typename T> momentum<T> operator*(const T& x, momentum<T> m1) {return m1*=x;}

/**
 * Minkowski scalar product between 2 momenta;
 */
template <typename T> T sp(const momentum<T>& p1, const momentum<T>& p2) {
    T result = p1[0] * p2[0];

    for (size_t i = 1; i < p1.size(); ++i) { result -= p1[i] * p2[i]; }
    return result;
}

/** 
 * Construct a vector [l1| \gamma^mu |l2>/2
 * \param l1 spinor [l1| or momentum
 * \param l2 spinor |l2> or momentum
 * \return vector [l1| \gamma^mu |l2>/2
 */
template <class T> momentum<T> LvBA(const momentum<T>& l1,const momentum<T>& l2);
/** 
 * Construct a vector <l1| \gamma^mu |l2]/2
 * \param l1 spinor <l1| or momentum
 * \param l2 spinor |l2] or momentum
 * \return vector <l1| \gamma^mu |l2]/2
 */
template <class T> momentum<T> LvAB(const la<T>& l1,const lat<T>& l2);
template <class T> momentum<T> LvAB(const momentum<T>& l1,const momentum<T>& l2);

template <class T> lat<T> operator*(const smatrix<T>&,const la<T>&);
template <class T> la<T> operator*(const smatrix<T>&,const lat<T>&);
template <class T> lat<T> operator*(const la<T>&,const smatrix<T>&);
template <class T> la<T> operator*(const lat<T>&,const smatrix<T>&);
// Spinor inner products
template <class T> T operator*(const la<T>&,const la<T>&);
template <class T> T operator*(const lat<T>&,const lat<T>&);
// Print functions
template <class T> std::ostream& operator<<(std::ostream&, const smatrix<T>& );
template <class T> std::ostream& operator<<(std::ostream&, const la<T>& );
template <class T> std::ostream& operator<<(std::ostream&, const lat<T>& );


//! Template class for spinorial objects
template <class T> class spinor {
  protected:
    std::array<T, 2> a{};

  public:
    //! safe access
    const T& at(unsigned i) const { return a.at(i); }
    //! unsafe access
    const T& operator()(unsigned i) const { return a[i]; }
    const T& operator[](unsigned i) const { return a[i]; }

    //! Constructor (usually not needed, use the constructors for la and lat instead)
    /** \sa la  lat smatrix */
    spinor() = default;
    spinor(const T& A1, const T& A2) : a({A1, A2}) {}
    spinor(const std::array<T, 2>& seta) : a(seta) {}

    std::vector<T> get_vector() const { return std::vector<T>{a[0], a[1]}; }

    // const std::array<T,2>& get_elements() const { return a; }

    static_assert(is_complex_v<T>, "Spinors are only allowed to have complex components!");
    using TR = remove_complex_t<T>;
};

//! Template for spinorial objects of the type "la"
template <class T> class la : public spinor<T> {
    using spinor<T>::a;

  public:
    template <typename TT = T> la(const momentum<TT>& p);
    la(const spinor<T>& s) : spinor<T>(s){};
    la(const T& A1, const T& A2) : spinor<T>(A1, A2){};

    la() = default;
    la(const la&) = default;
    la& operator=(const la<T>&) = default;
    la(la&&) = default;
    la& operator=(la&&) = default;

    la<T>& operator+=(const la<T>& p) {
        a[0] += p[0];
        a[1] += p[1];
        return *this;
    }
    la<T>& operator-=(const la<T>& p) {
        a[0] -= p[0];
        a[1] -= p[1];
        return *this;
    }
    la<T>& operator/=(const T& x) {
        a[0] /= x;
        a[1] /= x;
        return *this;
    }
    la<T>& operator*=(const T& x) {
        a[0] *= x;
        a[1] *= x;
        return *this;
    }
};

template <typename T> la(const momentum<T>&) -> la<add_complex_t<T>>;

//! Template for spinorial objects of the type "la tilde"
/** \sa smatrix  */
template <class T> class lat : public spinor<T> {
    using spinor<T>::a;

  public:
    template <typename TT = T> lat(const momentum<TT>& p);
    lat(const spinor<T>& s) : spinor<T>(s){};
    lat(const T& A1, const T& A2) : spinor<T>(A1, A2){};

    lat() = default;
    lat(const lat&) = default;
    lat& operator=(const lat<T>&) = default;
    lat(lat&&) = default;
    lat& operator=(lat&&) = default;

    lat<T>& operator+=(const lat<T>& p) {
        a[0] += p[0];
        a[1] += p[1];
        return *this;
    }
    lat<T>& operator-=(const lat<T>& p) {
        a[0] -= p[0];
        a[1] -= p[1];
        return *this;
    }
    lat<T>& operator/=(const T& x) {
        a[0] /= x;
        a[1] /= x;
        return *this;
    }
    lat<T>& operator*=(const T& x) {
        a[0] *= x;
        a[1] *= x;
        return *this;
    }
};

template <typename T> lat(const momentum<T>&) -> lat<add_complex_t<T>>;


// Basic algebra
template <typename T> la<T> operator+(la<T> b1,const la<T>& b2){ return b1+=b2; }
template <typename T> la<T> operator-(la<T> b1,const la<T>& b2){ return b1-=b2; }
template <typename T> la<T> operator-(const la<T>& b){ return la<T>(-b[0],-b[1]); }
template <typename T> la<T> operator*(const T& c, la<T> b){ return b*=c; }
template <typename T> la<T> operator*(la<T> b,const T& c){ return b*=c; }
template <typename T> la<T> operator/(la<T> b,const T& c){ return b/=c; }

template <typename T> lat<T> operator+(lat<T> b1,const lat<T>& b2){ return b1+=b2; }
template <typename T> lat<T> operator-(lat<T> b1,const lat<T>& b2){ return b1-=b2; }
template <typename T> lat<T> operator-(const lat<T>& b){ return lat<T>(-b[0],-b[1]); }
template <typename T> lat<T> operator*(const T& c, lat<T> b){ return b*=c; }
template <typename T> lat<T> operator*(lat<T> b,const T& c){ return b*=c; }
template <typename T> lat<T> operator/(lat<T> b,const T& c){ return b/=c; }

//! Template class for slashed matrices
/**
 * The slashed matrices are 2x2 matrices they correspond to the CLat[p].CLa[p] objects in S\@M.
 * Smatrix objects can be multiplied with la and lat objects. 
 * They can't be multiplied together since a smatrix object is supposed to have a dotted and an undotted index,
 * whereas the product of two slashed matrices has two dotted or two undotted indices.
 *
 * Slashed matrices are stored as p^{\dot{a}a} and all operations are aware of this.
 */
template <class T> class smatrix {
  protected:
    std::array<T, 4> a; // {a11,a12,a21,a22}

  public:
    // No bound checking, 1-based
    const T& operator()(unsigned i, unsigned j) const { return a[2 * (i - 1) + (j - 1)]; }
    // With bound checking
    const T& at(unsigned i, unsigned j) const { return a.at(2 * (i - 1) + (j - 1)); }

    //! explicit constructor
    smatrix(const T& A11, const T& A12, const T& A21, const T& A22) : a({A11, A12, A21, A22}) {}

    template <typename TT = T>
    smatrix(const momentum<TT>& p)
        : a({p[0] - p[3], -(p[1] - T(0, 1) * p[2]), -(p[1] + T(0, 1) * p[2]), p[0] + p[3]}) {}

    static_assert(is_complex_v<T>);
};

// Prohibit inter-representation mixing

template <class T> T operator*(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator*(const lat<T>& b1,const la<T>& b2) = delete;

template <class T> T operator+(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator+(const lat<T>& b1,const la<T>& b2) = delete;

template <class T> T operator-(const la<T>& b1,const lat<T>& b2) = delete;
template <class T> T operator-(const lat<T>& b1,const la<T>& b2) = delete;

template <typename Thigh, typename Tlow> inline la<Thigh>
to_precision(const la<Tlow>& a){
    return la<Thigh>(Thigh(a[0]),Thigh(a[1]));
}
template <typename Thigh, typename Tlow> inline lat<Thigh>
to_precision(const lat<Tlow>& a){
    return lat<Thigh>(Thigh(a[0]),Thigh(a[1]));
}





namespace detail{
    template <class T> T plus(const momentum<T>& l){
        return l[0]+l[3];
    }
    template <class T> T minus(const momentum<T>& l){
        return l[0]-l[3];
    }

    //inline double conj(double x) { return x; }
//#ifdef FivePointAmplitudes_QD_ENABLED
    //inline dd_real conj(dd_real x) { return x; }
    //inline qd_real conj(qd_real x) { return x; }
//#endif // FivePointAmplitudes_QD_ENABLED
}

// Spinor inner products
template <class T> T operator*(const la<T>& b1,const la<T>& b2){ return b2[0]*b1[1] - b1[0]*b2[1]; }
template <class T> T operator*(const lat<T>& b1,const lat<T>& b2){ return b1[0]*b2[1] - b2[0]*b1[1]; }


//**************************************************
// These products are implemented to respect the "store only lower index" for spinors rule
// Do not touch this!
//**************************************************
template <class T> lat<T> operator*(const smatrix<T>& m,const la<T>& b){
    return lat<T>(-m(2,1)*b[0] - m(2,2)*b[1], m(1,1)*b[0] + m(1,2)*b[1]);
}
template <class T> lat<T> operator*(const la<T>& b,const smatrix<T>& m){
    return lat<T>(m(2,1)*b[0] + m(2,2)*b[1], -m(1,1)*b[0] - m(1,2)*b[1]);
}

template <class T> la<T> operator*(const smatrix<T>& m,const lat<T>& b){
    return la<T>(b[0]*m(1,2) + b[1]*m(2,2), -b[0]*m(1,1) - b[1]*m(2,1));
}
template <class T> la<T> operator*(const lat<T>& b,const smatrix<T>& m){
    return la<T>(-b[0]*m(1,2) - b[1]*m(2,2), b[0]*m(1,1) + b[1]*m(2,1));
}
//**************************************************

//! spinor product < * | * > 
template <typename T> add_complex_t<T> spaa(const momentum<T>& p1,const momentum<T>& p2){ return la{p1}*la{p2}; }
template <typename T> add_complex_t<T> spaa(const la<T>& l1,const la<T>& l2){ return l1*l2; }
template <typename T1, typename T> add_complex_t<T> spaa(const momentum<T1>& l1,const la<T>& l2){ return la{l1}*l2; }
template <typename T1, typename T> add_complex_t<T> spaa(const la<T>& l1,const momentum<T1>& l2){ return l1*la{l2}; }

template <class T, class... Ts> add_complex_t<T> spaa(const momentum<T>& first, const Ts&... rest){
    return spaa(la{first},rest...);
}
template <class T, typename T1, class... Ts> add_complex_t<T> spaa(const la<T>& first, const momentum<T1>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 0, "spaa with odd number of arguments called");
    return spba(first*smatrix<T>(second),rest...);
}

//! spinor bracket [ * | * > 
template <class T, typename T1, class... Ts> add_complex_t<T> spba(const lat<T>& first, const momentum<T1>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 1, "spba with even number of arguments called");
    return spaa(first*smatrix<T>(second),rest...);
}
template <class T,class... Ts> add_complex_t<T> spba(const momentum<T>& first, const Ts&... rest){
    return spba(lat{first},rest...);
}

//! spinor product [ * | * ]
template <class T> add_complex_t<T> spbb(const momentum<T>& p1,const momentum<T>& p2){ return lat{p1}*lat{p2}; }
template <class T> add_complex_t<T> spbb(const lat<T>& l1,const lat<T>& l2){ return l1*l2; }
template <typename T1, typename T> add_complex_t<T> spbb(const momentum<T1>& l1,const lat<T>& l2){ return lat{l1}*l2; }
template <typename T1, typename T> add_complex_t<T> spbb(const lat<T>& l1,const momentum<T1>& l2){ return l1*lat{l2}; }


//! spinor bracket < * | * ] 
template <class T,class... Ts> add_complex_t<T> spab(const momentum<T>& first, const Ts&... rest){
    return spab(la{first},rest...);
}
template <class T, typename T1, class... Ts> add_complex_t<T> spab(const la<T>& first, const momentum<T1>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 1, "spab with even number of arguments called");
    return spbb(first*smatrix<T>(second),rest...);
}

template <class T, class... Ts> add_complex_t<T> spbb(const momentum<T>& first, const Ts&... rest){
    return spbb(lat{first},rest...);
}
template <class T, typename T1, class... Ts> add_complex_t<T> spbb(const lat<T>& first, const momentum<T1>& second, const Ts&... rest){
    static_assert((sizeof...(rest))%2 == 0, "spba with odd number of arguments called");
    return spab(first*smatrix<T>(second),rest...);
}


template <class T> momentum<T> LvBA(const lat<T>& l1,const la<T>& l2){
    T inv2=T(1)/T(2);
    T A11=inv2*l1[0]*l2[0];
    T A12=inv2*l1[0]*l2[1];
    T A21=inv2*l1[1]*l2[0];
    T A22=inv2*l1[1]*l2[1];
    return momentum<T>(A22+A11,A21+A12,T(0,-1)*(A12-A21),A11-A22);
}

template <class T> momentum<T> LvAB(const la<T>& l1,const lat<T>& l2){
    return LvBA(l2,l1);
}

template <class T> momentum<T> LvBA(const momentum<T>& l1,const momentum<T>& l2){ return LvBA<T>(lat<T>(l1), la<T>(l2));}
template <class T> momentum<T> LvAB(const momentum<T>& l1,const momentum<T>& l2){ return LvAB<T>(la<T>(l1), lat<T>(l2));}

template <class T> template <typename TT> la<T>::la(const momentum<TT>& p) {
    using std::sqrt;
    using std::abs;
    using std::conj;

    using detail::minus;
    using detail::plus;
    using TR = remove_complex_t<T>;

    if (abs(plus(p)) < root_epsilon<TR>)
        if (abs(minus(p)) < root_epsilon<TR>) {
            a[0] = (p[1] - T(0, 1) * p[2]) / sqrt(TR(2) * p[1]);
            a[1] = (p[1] + T(0, 1) * p[2]) / sqrt(TR(2) * p[1]);
        }
        else {
            T sqm = sqrt(minus(p));
            a[0] = TR(1) / sqm * (p[1] - T(0, 1) * p[2]);
            a[1] = sqm;
        }
    else {
        T sqk1p = sqrt(abs(plus(p)));
        a[0] = sqk1p;
        a[1] = sqk1p * (p[1] + T(0, 1) * p[2]) / plus(p);
    }
}

template <class T> template <typename TT> lat<T>::lat(const momentum<TT>& p) {
    using std::sqrt;
    using std::abs;
    using std::conj;

    using detail::minus;
    using detail::plus;
    using TR = remove_complex_t<T>;

    if (abs(plus(p)) < root_epsilon<TR>) {
        if (abs(minus(p)) < root_epsilon<TR>) {
            a[0] = (p[1] + T(0, 1) * p[2]) / sqrt(TR(2) * p[1]);
            a[1] = (p[1] - T(0, 1) * p[2]) / sqrt(TR(2) * p[1]);
        }
        else {
            T sqm = sqrt(minus(p));
            a[0] = TR(1) / sqm * (p[1] + T(0, 1) * p[2]);
            a[1] = sqm;
        }
    }
    else {
        T sqk1p = sqrt(abs(plus(p)));
        T inv = TR(1) / sqk1p;
        a[0] = plus(p) * inv;
        a[1] = inv * (p[1] - T(0, 1) * p[2]);
    }
}

/**
 * Momenta configuration.
 *
 * Momenta inidice 1-based.
 */
template <typename T> struct mom_conf {
    std::vector<momentum<T>> momenta;

    auto& operator[](size_t i) { return momenta[i-1]; } //! 1-based!
    auto& operator[](size_t i) const { return momenta[i-1]; } //! 1-based!
    auto& at(size_t i) { return momenta.at(i-1); } //! 1-based!
    auto& at(size_t i) const { return momenta.at(i-1); } //! 1-based!

    auto size() const { return momenta.size(); }

    auto begin() { return momenta.begin(); }
    auto end() { return momenta.end(); }
    auto begin() const { return momenta.begin(); }
    auto end() const { return momenta.end(); }
    auto cbegin() const { return momenta.cbegin(); }
    auto cend() const { return momenta.cend(); }

    /**
     * Return a Mandlestam invariant s(i,j,k...).
     * Indices are 1-based.
     */
    template <class... Args> T s(Args&&... args) const {
        static_assert((std::is_integral_v<std::remove_reference_t<Args>> && ...));
        auto sum = (... + momenta.at(args-1));
        return sp(sum,sum);
    }
    /**
     * Return a permuted mom_conf by permutation p.
     * The permutation should be given as a list.
     */
    mom_conf permute(const Permutation& p) const {
        if (p.size() != momenta.size()) throw std::runtime_error("Permutation must have the same length as the momenta configuration.");

        decltype(momenta) mc_new;

        mc_new.reserve(momenta.size());

        for (auto& pi : p) {
            mc_new.push_back(momenta.at(pi-1)); 
        }

        auto check_indices = p;
        std::sort(check_indices.begin(),check_indices.end());

        for (size_t i = 1; i <= momenta.size(); ++i) {
            if (check_indices.at(i-1) != static_cast<int>(i)) throw std::runtime_error("Incorrect permutation!");
        }

        return {mc_new};
    }
    /**
     * Return a new mom_conf obtained by embedding this one.
     */
    mom_conf embed_into(const EmbeddingMap& p) const {
        size_t n_orig = 0;

        decltype(momenta) mc_new;

        mc_new.reserve(p.size());

        for (auto& new_pi : p) {
            n_orig += new_pi.size();
            momentum<T> mom;
            for(auto& pi: new_pi) {
                mom += momenta.at(pi-1);
            }
            mc_new.push_back(mom);
        }

        if (n_orig != momenta.size()) throw std::runtime_error("Embedding map must map all momenta.");

        return {mc_new};
    }
    /**
     * Return a parity conjugated mom_conf     
     **/
    mom_conf parity_conjugate() const {
        auto new_momenta = momenta;
        for (auto& mi : new_momenta) mi = mi.parity_conjugate();
        return {new_momenta};
    }
    /**
     * Return a rescaled momentum configuration by a scale mu.
     * NOTE: mu has energy dimension 1, not 2.
     */
    mom_conf rescale(const T& mu) const {
        mom_conf new_mc = *this;
        for (auto& mi : new_mc) {
            for (auto& pi : mi) { pi /= mu; }
        }
        return new_mc;
    }
    /**
     * Return tr5 in the "standard"/spinor definition.
     *
     * Notice that this differs from Caravel's internal definition (as of v0.2.3)
     */
    auto get_tr5() const {
        if (momenta.size() < 4) throw std::runtime_error("tr5 with less than 4 momenta requested");
        const auto& mc = momenta;
        return -spab(mc[0], mc[1], mc[2], mc[3], mc[0]) + spab(mc[0], mc[3], mc[2], mc[1], mc[0]);
    }
    /**
     * Check if on-shell conditions and momentum conservation are satisfied,
     * up to a given threshold.
     *
     * Momenta with indices listed in to_skip_indices (1-based) are considered offshell.
     */
    bool is_onshell_q(std::vector<int> offshell_indices = {}, const T& threshold = root_epsilon<T>) {
        using std::abs;
        momentum<T> sum; 

        std::set<int> to_check(offshell_indices.begin(), offshell_indices.end());

        for (size_t i = 1; i <= momenta.size(); ++i) {
            const auto& pi = momenta.at(i-1);
            if(to_check.count(i) == 0 ) {
                if (abs(sp(pi,pi)) > threshold) return false;
            }
            sum += pi;
        }

        for(auto& xi : sum) {
            if (abs(xi) > threshold) return false;
        }

        return true;
    }
    /**
     * Check if on-shell conditions and momentum conservation are satisfied,
     * up to a given threshold.
     *
     * squares can be 0 and the number of them must match the number of momenta.
     */
    bool is_onshell_q(const std::vector<T>& squares, const T& threshold = root_epsilon<T>) {
        using std::abs;
        momentum<T> sum; 

        if (squares.size() != momenta.size()) throw std::runtime_error("squares must be the same size as the number of momenta");

        for (size_t i = 1; i <= momenta.size(); ++i) {
            const auto& pi = momenta.at(i-1);
            if (abs(sp(pi,pi) - squares.at(i-1)) > threshold) return false;
            sum += pi;
        }

        for(auto& xi : sum) {
            if (abs(xi) > threshold) return false;
        }

        return true;
    }
};

template <typename T> momentum<T> lorentz_boost(const momentum<T>& rv, std::array<T,3> beta) {
    using std::sqrt;

    const T& t = rv[0];

    T bn = sqrt(beta[0]*beta[0]+beta[1]*beta[1]+beta[2]*beta[2]);

    if (bn * bn >= T(1)) throw std::runtime_error("beta should have norm less than 1 for lorentz boost!");

    T gamma = T(1)/sqrt(T(1)-bn*bn);

    T t2 = gamma * t;

    for (size_t i = 1; i <= 3; ++i) { t2 -= gamma * rv(i) * beta.at(i - 1); }

    for (auto& bi : beta) bi/=bn;

    T rdotbeta{0};
    for (size_t i = 1; i <= 3; ++i) { rdotbeta += rv(i) * beta.at(i - 1); }
    rdotbeta *= (gamma - T(1));

    std::array<T,3> r2;

    for (size_t i = 0; i < 3; ++i) {
        r2[i] = rv[i+1] + rdotbeta*beta.at(i) - gamma * t * bn * beta.at(i);
    }

    return {t2, r2[0], r2[1], r2[2]};
}


template <typename T> mom_conf<T> lorentz_boost(mom_conf<T> mc, const std::array<T,3>& beta) {
    for(auto& mi : mc.momenta) mi = lorentz_boost(mi, beta);
    return mc;
}


/********************** instantiations *******************************************/


extern template class momentum<double>;
extern template class spinor<complex<double>>;
extern template class la<complex<double>>;
extern template class lat<complex<double>>;
extern template class smatrix<complex<double>>;

extern template complex<double> spaa<double>(const momentum<double>& p1,const momentum<double>& p2);
extern template complex<double> spbb<double>(const momentum<double>& p1,const momentum<double>& p2);
extern template complex<double> spab<double>(const momentum<double>& p1,const momentum<double>& p2,const momentum<double>& p3);
extern template complex<double> spba<double>(const momentum<double>& p1,const momentum<double>& p2,const momentum<double>& p3);

extern template momentum<double> lorentz_boost(const momentum<double>&, std::array<double,3>);

#ifdef FivePointAmplitudes_QD_ENABLED
extern template class momentum<dd_real>;
extern template class spinor<complex<dd_real>>;
extern template class la<complex<dd_real>>;
extern template class lat<complex<dd_real>>;
extern template class smatrix<complex<dd_real>>;

extern template complex<dd_real> spaa<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2);
extern template complex<dd_real> spbb<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2);
extern template complex<dd_real> spab<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2,const momentum<dd_real>& p3);
extern template complex<dd_real> spba<dd_real>(const momentum<dd_real>& p1,const momentum<dd_real>& p2,const momentum<dd_real>& p3);

extern template class momentum<qd_real>;
extern template class spinor<complex<qd_real>>;
extern template class la<complex<qd_real>>;
extern template class lat<complex<qd_real>>;
extern template class smatrix<complex<qd_real>>;

extern template complex<qd_real> spaa<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2);
extern template complex<qd_real> spbb<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2);
extern template complex<qd_real> spab<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2,const momentum<qd_real>& p3);
extern template complex<qd_real> spba<qd_real>(const momentum<qd_real>& p1,const momentum<qd_real>& p2,const momentum<qd_real>& p3);

extern template momentum<dd_real> lorentz_boost(const momentum<dd_real>&, std::array<dd_real,3>);
extern template momentum<qd_real> lorentz_boost(const momentum<qd_real>&, std::array<qd_real,3>);
#endif

} // namespace FivePointAmplitudes

