#pragma once

#include "FivePointAmplitudes_config.h"

#include "Spinor.h"


namespace FivePointAmplitudes {

// initialized in Spinor.cpp
extern double large_cancellations_threshold; 

template <typename T> momentum<T> to_real_momentum (const momentum<complex<T>>& m_c, const T thr = std::numeric_limits<T>::epsilon()*100) {

    std::array<T, m_c.N> real_components;

    for (size_t i = 0; i < m_c.size(); ++i) {
        const auto& pi = m_c[i];
        if (abs(pi.imag()) > thr) throw std::runtime_error("Large imaginary part in to_real_momentum!");
        real_components[i] = pi.real();
    }

    return {real_components};
}

template <typename T_high, typename T_low> momentum<T_high> to_precision (const momentum<T_low>& m_low) {

    std::array<T_high, m_low.N> real_components;

    for (size_t i = 0; i < m_low.size(); ++i) {
        real_components[i] = T_high(m_low[i]);
    }

    return {real_components};
}

template <typename T_high, typename T_low> mom_conf<T_high> to_precision (const mom_conf<T_low>& m_low) {
    mom_conf<T_high> mc_high;
    mc_high.momenta.reserve(m_low.size());

    for(auto& mi : m_low) mc_high.momenta.push_back(to_precision<T_high>(mi));

    return mc_high;
}


/**
 * Increase precision of a phase space point,
 * ensuring that onshellness and momentum conservation are preserved to the higher precision.
 */
template <class T_high, class T_low> mom_conf<T_high> increase_precision(const mom_conf<T_low>& mc) {
    
    if (mc.size() < 4) { throw std::runtime_error("increase_precision requires at least 4 momenta"); }

    mom_conf<T_high> new_mc = to_precision<T_high>(mc);

    auto get_p3_norm = [](const auto& m) {
        T_high norm{};

        for (size_t i = 1; i <=3; ++i) {
            norm += m[i]*m[i];
        }

        using std::sqrt;

        return sqrt(norm);
    };

    momentum<T_high> sum;

    const size_t N = mc.size();

    for (size_t i = 1; i <= N-1; ++i) {
        auto& mi = new_mc[i];
        auto n = get_p3_norm(mi);
        if( mi[0] >= T_high(0) ) {
            mi[0] = n;
        }
        else {
            mi[0] = -n;
        }

        if(i <= N-2) sum -= mi;
    }

    new_mc[N-1]  *= sp(sum,sum)/(T_high{2}*sp(sum, new_mc[N-1]));

    new_mc[N] = sum - new_mc[N-1];

    return new_mc;
}
/**
 * Increase precision for sij point trivially.
 */
template <class T_high, class T_low, size_t N> std::array<T_high, N> increase_precision(const std::array<T_low,N>& arr) {
    std::array<T_high, N> new_arr;

    for (size_t i = 0; i < N; ++i) {
        new_arr[i] = static_cast<T_high>(arr[i]);
    }

    return new_arr;
}

inline double to_double(double x) { return x; }

template <typename T> std::complex<double> to_double(const std::complex<T>& x) { return {to_double(x.real()), to_double(x.imag())}; }

template <typename T> std::vector<double> to_double(const std::vector<T>& x) {
    std::vector<double> result;
    result.reserve(x.size());
    for (const auto& it : x) result.emplace_back(to_double(it));
    return result;
}

template <typename T> std::vector<std::vector<double>> to_double(const std::vector<std::vector<T>>& x) {
    std::vector<std::vector<double>> result;
    result.reserve(x.size());
    for (const auto& it : x) result.emplace_back(to_double(it));
    return result;
}

} // namespace FivePointAmplitudes
