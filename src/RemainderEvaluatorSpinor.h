#pragma once

#include "FivePointAmplitudes_config.h"

#include "FunctionsManager.h"
#include "Kinematics.h"
#include "Timing.h"

#include "Spinor.h"

#include "RemainderEvaluator.h"
#include <type_traits>

namespace FivePointAmplitudes {

namespace detail {


/**
 * Get a global list of spinor brackets that are used in spinor expressions
 */
template <typename T> auto get_spinor_brackets(const mom_conf<T>& mc) {
    return std::array{spaa(mc[1], mc[2]),        spaa(mc[1], mc[3]),        spaa(mc[1], mc[4]),        spaa(mc[1], mc[5]),        spaa(mc[2], mc[3]),
                      spaa(mc[2], mc[4]),        spaa(mc[2], mc[5]),        spaa(mc[3], mc[4]),        spaa(mc[3], mc[5]),        spaa(mc[4], mc[5]),
                      spab(mc[1], mc[2], mc[1]), spab(mc[1], mc[3], mc[1]), spab(mc[1], mc[4], mc[1]), spab(mc[1], mc[5], mc[1]), spab(mc[2], mc[1], mc[2]),
                      spab(mc[2], mc[3], mc[2]), spab(mc[2], mc[4], mc[2]), spab(mc[2], mc[5], mc[2]), spab(mc[3], mc[1], mc[3]), spab(mc[3], mc[2], mc[3]),
                      spab(mc[3], mc[4], mc[3]), spab(mc[3], mc[5], mc[3]), spab(mc[4], mc[1], mc[4]), spab(mc[4], mc[2], mc[4]), spab(mc[4], mc[3], mc[4]),
                      spab(mc[4], mc[5], mc[4]), spab(mc[5], mc[1], mc[5]), spab(mc[5], mc[2], mc[5]), spab(mc[5], mc[3], mc[5]), spab(mc[5], mc[4], mc[5]),
                      spbb(mc[2], mc[1]),        spbb(mc[3], mc[1]),        spbb(mc[3], mc[2]),        spbb(mc[4], mc[1]),        spbb(mc[4], mc[2]),
                      spbb(mc[4], mc[3]),        spbb(mc[5], mc[1]),        spbb(mc[5], mc[2]),        spbb(mc[5], mc[3]),        spbb(mc[5], mc[4])};
}
/**
 * Get a global list of inverse denominators that can be encountered in spinor expressions
 */
template <typename T> auto get_inverse_pentagon_spinor_letters(const std::array<T, 40>& abb) {
    std::array qs = {abb[0],
                     abb[1],
                     abb[2],
                     abb[3],
                     abb[4],
                     abb[5],
                     abb[6],
                     abb[7],
                     abb[8],
                     abb[9],
                     abb[10] + abb[11],
                     abb[10] + abb[12],
                     abb[10] + abb[13],
                     abb[14] + abb[15],
                     abb[14] + abb[16],
                     abb[14] + abb[17],
                     abb[18] + abb[20],
                     abb[18] + abb[21],
                     abb[22] + abb[23],
                     abb[22] + abb[24],
                     abb[22] + abb[25],
                     abb[26] + abb[27],
                     abb[26] + abb[28],
                     abb[26] + abb[29],
                     abb[30],
                     abb[31],
                     abb[32],
                     abb[33],
                     abb[34],
                     abb[35],
                     abb[36],
                     abb[37],
                     abb[38],
                     abb[39]};

    for (auto& qi : qs) qi = T(1) / qi;

    return qs;
}

template <typename T> std::array<T, 26> get_inverse_pentagon_letters(const mom_conf<T>& psp, T) {
    if (psp.size() != 5) throw std::runtime_error("get_inverse_pentagon_letters requires a 5-point phase-space point!");

    return get_inverse_pentagon_letters({psp.s(1, 2), psp.s(2, 3), psp.s(3, 4), psp.s(4, 5), psp.s(1, 5)}, psp.get_tr5().imag());
}

} // namespace detail



/**
 * Evaluator for spinor remainders
 *
 * @param mc -- momenta
 * @param str5 -- +1 or -1 depending on what convention for tr5 was used for analytic formulas. +1 for same, -1 for opposite.
 * @param coeff_basis -- a function that evaluate rational basis
 * @param functions_basis -- list of functions that evaluate pentagon functions
 * @param coefficients -- matrix elements to combine together rational and transcendental
 * @param f_norm -- a function that evaluates normalization (tree or spinor weight) to make rational coefficients dimensionless
 * @param f_manager -- pentagon functions manager (for caching)
 * @param permutation -- permute momenta for rational coefficients
 */
template <typename T, typename Tm, typename Tc_basis, typename Tf_basis, typename Fnorm>
std::complex<T> eval_remainder_spinor(mom_conf<T> mc, int str5, T musq, const Tc_basis& coeff_basis, const std::vector<Tf_basis>& functions_basis,
                               const Tm& coefficients, Fnorm f_norm, std::shared_ptr<FunctionsManager> f_manager, const std::vector<int>& permutation) {
    using TC = std::complex<T>;
    using std::array;

    // The coefficients of all functions in all remainders are by construction dimensionless,
    // so for better numerical stability we rescale them.
    // We could choose the scale ourselves here, but we can also assume that the renormalization
    // scale has been chosen naturally, so we use it instead.
    if (musq != T(1)) {
        using std::sqrt;
        T mu = sqrt(musq);
        mc = mc.rescale(mu);
    }

    Kin<T> kin{{mc.s(1,2), mc.s(2,3), mc.s(3,4), mc.s(4,5), mc.s(1,5)}};

#ifdef TIMING_ON
    ::timing::_rational_functions_timing.start();
#endif // TIMING_ON
    mom_conf<T> permuted_mc = mc.permute(permutation);

    auto spp = detail::get_spinor_brackets(permuted_mc);
    auto qs = detail::get_inverse_pentagon_spinor_letters(spp);

    auto norm = f_norm(permuted_mc);
    norm = T(1)/norm;

    auto coeffs = coeff_basis(spp, qs);
    for (auto& ri : coeffs) ri *= norm;
#ifdef TIMING_ON
    ::timing::_rational_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON
    ::timing::_transcendental_functions_timing.start();
#endif // TIMING_ON
    const auto& function_values = f_manager->eval(kin);
#ifdef TIMING_ON
    ::timing::_transcendental_functions_timing.stop();
#endif // TIMING_ON

#ifdef TIMING_ON
    ::timing::_summations_timing.start();
#endif // TIMING_ON
    std::vector<TC> functions_basis_values;
    functions_basis_values.reserve(function_values.size());

    // If the give phase-space point has negative tr5, flip str5.
    // Not that we do not *set* it to -1, but flip. The original convention is still respected.
    {
        T tr5i = mc.get_tr5().imag();
        if (tr5i < 0) str5 *= -1;
    }
    // if we are evauating on a permuted, we adjust the sign of tr5
    str5 *= detail::permutation_signature(permutation);

    for (const auto& mono : functions_basis) {
        TC c{1};
        for (const auto& el : mono) {
            switch (el.first) {
                case f_type::f: c *= function_values.at(el.second); break;
                case f_type::bc: c *= bc<T>.at(el.second); break;
                case f_type::bci: c *= TC(0, bc<T>.at(el.second)); break;
                case f_type::str5: c *= str5; break;
                case f_type::isqrt: // we don't use this here
                case f_type::one: break;
            }
        }
        functions_basis_values.push_back(std::move(c));
    }


    // Neumaier summation (for improved numerical stability) [doi:10.1002/zamm.19740540106]
    auto sum_coeff = [&coeffs](const auto& values) -> TC {
        using std::get;
        
        // trivial cases 
        switch (values.size()) {
            case 0: return T(0);
            case 1: return coeffs[get<int>(values[0])] * get<T>(values[0]);
            case 2: return coeffs[get<int>(values[0])] * get<T>(values[0]) + coeffs[get<int>(values[1])] * get<T>(values[1]);
            default: break;
        }

        TC sum{0};
        TC c{0};

        for (const auto& it : values) {
            using std::abs;
            using std::get;

            auto si = (coeffs[get<int>(it)] * get<T>(it));

            auto t = sum + si;

            if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
            else {
                c += ((si - t) + sum);
            }
            sum = t;
        }
        sum += c;
        return sum;
    };

    TC sum{0};
    TC c{0};

    for (size_t i = 0; i < functions_basis.size(); ++i){
        using std::abs;

        TC si = sum_coeff(coefficients[i]);

        si *= functions_basis_values[i];

        auto t = sum + si;
        if (abs(sum) >= abs(si)) { c += ((sum - t) + si); }
        else {
            c += ((si - t) + sum);
        }
        sum = t;
    }
    sum += c;

#ifdef TIMING_ON
    ::timing::_summations_timing.stop();
#endif // TIMING_ON

    return sum;
}

} // namespace FivePointAmplitudes


