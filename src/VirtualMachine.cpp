#include "VirtualMachine.h"

namespace vm {

std::mutex global_instructions_storage_mutex;
std::unordered_map<std::string, std::vector<instruction>>  global_instructions_storage;

} // vm
