#pragma once

#include "FivePointAmplitudes_config.h"


#include "MathList.h"
#include "Spinor.h"
#include "Utilities.h"
#include "VirtualMachine.h"

#include <Constants.h>

#include <matchit/matchit.h>
#include <memory>

namespace FivePointAmplitudes {
namespace Vjj {

template <typename T> struct FiniteRenormalizationOperator {

    using C = add_complex_t<T>;

    std::vector<MathList> vars;
    std::vector<vm::VirtualMachine<C>> vms;

    // TODO: generalize 
    T mb;

    FiniteRenormalizationOperator() = default;

    FiniteRenormalizationOperator(std::string filename) {
        auto file = read_file(filename);

        std::istringstream str_stream(file);

        std::string vars_input;
        std::getline(str_stream, vars_input);

        for(auto&& it : MathList(vars_input, "variables")) {
            vars.emplace_back(std::move(it));
        }

        // Here we assume input of exactly 6 coefficients:
        // {as^0 nf^0 , as^1 nf^0, as^1 nf^1, as^2 nf^0, as^2 nf^1, as^2 Nf^2}
        // TODO: generalize
        for (size_t i = 1; i <= 6 ; ++i) {
            //skip any whitespace or new lines
            str_stream >> std::ws;
            vms.emplace_back(str_stream);
        }
    }


    using ResultType = std::vector<std::vector<C>>;

    ResultType operator()(const mom_conf<T>& mc, T mu, const Permutation& perm) {

        // In the trivial empty case we return 1
        // TODO: generalize format, siszes of vectors
        if(vms.empty()) return ResultType{{C{1}},{C{0}, C{0}},{C{0}, C{0}, C{0}}};

        auto mc_perm = mc.permute(perm);

        using namespace matchit;

       std::vector<C> vars_eval;
       vars_eval.reserve(vars.size());

       using std::log;

       auto logIsij = [&](int i, int j) -> C {
           auto sij = mc_perm.s(i,j)/(mu*mu);

           if(sij < 0) return log(-sij);
           else return {log(sij), -constants::pi<T>};
       };

       for(auto v : vars) {
           C val = match(v.head) (
                   pattern | "pi" = constants::pi<T>,
                   pattern | "zeta3" = constants::zeta3<T>,
                   pattern | "logmb" = log(mb/mu),
                   pattern | "logIsij" = [&](){return logIsij(std::stoi(v[0]), std::stoi(v[1]));},
                   pattern | _ = C{}
                   );

           if (val == C{}) {
                throw std::runtime_error("Unsupported variable " + v.head + " encountered in FiniteRenormalizationOperator");
           }
            vars_eval.push_back(val);
       }

       ResultType result;

       //TODO: generalize
       result.push_back(std::vector<C>{vms.at(0)(vars_eval)});
       result.push_back(std::vector<C>{vms.at(1)(vars_eval), vms.at(2)(vars_eval)});
       result.push_back(std::vector<C>{vms.at(3)(vars_eval), vms.at(4)(vars_eval), vms.at(5)(vars_eval)});

       return result;
    }
};

} // namespace Vjj


template <typename T> struct FiniteRenormalizationOperator_Add {

    using C = add_complex_t<T>;

    std::vector<MathList> extra_vars;
    std::shared_ptr<vm::VirtualMachine<C, std::vector<C>>> assembly;

    FiniteRenormalizationOperator_Add() = default;

    FiniteRenormalizationOperator_Add(std::string filename) {
        auto file = read_file(std::string(FivePointAmplitudes_DATADIR) + "/" + filename);

        std::istringstream str_stream(file);

        std::string vars_input;
        std::getline(str_stream, vars_input);

        for(auto&& it : MathList(vars_input, "variables")) {
            extra_vars.emplace_back(std::move(it));
        }

        str_stream >> std::ws;

        assembly = std::make_shared<vm::VirtualMachine<C, std::vector<C>>>(filename, str_stream);
    }

    std::vector<C> operator()(const mom_conf<T>& mc, T mu, std::vector<C> args) {
       using namespace matchit;

       std::vector<C> vars_eval;
       vars_eval.reserve(extra_vars.size());

       using std::log;

       auto logIsij = [&](int i, int j) -> C {
           auto sij = mc.s(i,j)/(mu*mu);

           if(sij < 0) return log(-sij);
           else return {log(sij), -constants::pi<T>};
       };

       for(auto v : extra_vars) {
           C val = match(v.head) (
                   pattern | "pi" = constants::pi<T>,
                   pattern | "zeta3" = constants::zeta3<T>,
                   //pattern | "logmb" = log(mb/mu),
                   pattern | "logIsij" = [&](){return logIsij(std::stoi(v[0]), std::stoi(v[1]));},
                   pattern | _ = C{}
                   );

           if (val == C{}) {
                throw std::runtime_error("Unsupported variable " + v.head + " encountered in FiniteRenormalizationOperator");
           }
            vars_eval.push_back(val);
       }

       args.insert(args.end(), vars_eval.begin(), vars_eval.end());

       return assembly -> operator()(args);
    }
};




} // namespace FivePointAmplitudes




