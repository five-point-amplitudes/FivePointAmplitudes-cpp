#pragma once

#include <cmath>
#include <complex>
#include <cstdint>
#include <ios>
#include <istream>
#include <fstream>
#include <limits>
#include <mutex>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace vm{

enum class op_type : std::int16_t {
    add = 1,
    add_many = 10,

    mul = 2,
    mul_many = 20,

    neg = 3,

    sq = 51,
    inv = 52,
    power_int = 53, //! integer powers only

    complex_conjugate = 55, //! integer powers only

    fetch_constant = 9, //! take from constants, 1-based indexing!

    assign_c = 1001, //! assign valueto a register from constants
    assign_a = 1002, //! assign value to a register from arguments
    assign_i = 1003, //! assign value to a register from integer constant

    ret = 0, //! return
};


struct instruction {
    uint32_t* reg_input = nullptr; /// input registers
    uint32_t reg_out{0}; /// output register
    uint16_t n_reg_input{0}; /// number of input registers
    op_type op_code{0}; /// operation code

    instruction(uint32_t* _reg_input, uint32_t _reg_out, uint16_t _n_reg_input, op_type _op_code)
        : reg_input{_reg_input}, reg_out{_reg_out}, n_reg_input{_n_reg_input}, op_code{_op_code} {};

    instruction& operator=(const instruction& other) {
        reg_input = new uint32_t[other.n_reg_input];
        std::copy(other.reg_input, other.reg_input+other.n_reg_input, reg_input);

        reg_out = other.reg_out;
        n_reg_input = other.n_reg_input;
        op_code = other.op_code;

        return *this;
    }

    instruction(const instruction& other) {
        reg_input = new uint32_t[other.n_reg_input];
        std::copy(other.reg_input, other.reg_input+other.n_reg_input, reg_input);

        reg_out = other.reg_out;
        n_reg_input = other.n_reg_input;
        op_code = other.op_code;
    }

    instruction(instruction&& other) {
        reg_input = other.reg_input;
        other.reg_input = nullptr;

        reg_out = other.reg_out;
        n_reg_input = other.n_reg_input;
        op_code = other.op_code;
    }

    instruction& operator=(instruction&& other) {
        reg_input = other.reg_input;
        other.reg_input = nullptr;

        reg_out = other.reg_out;
        n_reg_input = other.n_reg_input;
        op_code = other.op_code;

        return *this;
    }

    ~instruction(){
        delete[] reg_input;
    }
};


extern std::mutex global_instructions_storage_mutex;
extern std::unordered_map<std::string, std::vector<instruction>> global_instructions_storage;

template <typename T> struct is_complex : std::false_type {};
template <typename T> struct is_complex<std::complex<T>> : std::true_type {};
template <typename T> static constexpr bool is_complex_v = is_complex<T>::value;

template <typename T, typename TR = T> class VirtualMachine {
  static_assert(std::is_same_v<TR,T> or std::is_same_v<TR,std::vector<T>>);
  public:
    size_t n_args{0};         //! number of arguments
    size_t n_registers{0};
    size_t return_register_index{0}; //! the register which contains the return value after the last instruction

    std::vector<int> integer_constants{};
    std::vector<T> constants;


    TR operator()(const std::vector<T>& args);

    std::string vm_id_string;
    std::vector<instruction> instructions{};


    VirtualMachine() = default;
    VirtualMachine(const VirtualMachine&) = default;
    VirtualMachine(VirtualMachine&&) = default;
    VirtualMachine& operator=(VirtualMachine&&) = default;
    VirtualMachine& operator=(const VirtualMachine&) = default;
    /**
     * Construct from a string.
     *
     * Structure:
     *
     * 1: [n_args] [n_registers] [n_integer_constants] [n_constants] [n_instructions (including return)] [return_register_index] 
     * 2: [integer constants, space separated]
     * 3: [T constants, space separated, formatted to be readable by T::operator>>]
     * 4: ...
     * .  ...
     * .  [instruction code] [out register] [in registers, space separated]
     * .  ...
     * .  ...
     * n: [last instruction is return]
     */
    VirtualMachine(std::istream& input);
    /**
     * Same constructor as above, 
     * but an id string can be provided. 
     *
     * This id string will be used to access the global instruction storage,
     * instead of storing instructions locally.
     * This is useful to reduce memory footprint when using multiple precision types,
     * because instructions are not precision dependent.
     *
     * The construction will be also skipped the second time.
     *
     * WARNING: it is on the user to ensure that there are no two different VMs that have the same id GLOBALLY
     */
    VirtualMachine(std::string set_vm_id_string, std::istream& input);

  private:
    size_t read_preamble(std::istream& input);
    void read_instructions(size_t n_instructions, std::istream& input);
};


/**
 * Read a rational number of type T from input stream. 
 * Requires operator>> for type T to be implemented.
 *
 * The number should be in the form of either 
 * (1) [num] 
 * (2) [num]/[den],
 * where both [num] and [den] are readable as type T.
 *
 * In case (2) num and den are read as T first, then their ratio is computed.
 * It is then required that T::operator/= exists.
 */
template <typename T> T read_rational_number(const std::string& s_in) {

    T num;

    if (auto pos = s_in.find("/"); pos == std::string::npos) {
        std::stringstream in(s_in);
        in >> num;
        if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
    }
    else {
        T den;
        {
            std::stringstream in(s_in.substr(0,pos));
            in >> num;
            if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
        }
        {
            std::stringstream in(s_in.substr(pos+1,s_in.size()));
            in >> den;
            if (in.fail()) { throw std::invalid_argument("Cannot iterpret " +s_in +"!\""); }
        }

        num /= den;
    }

    if (num != num) { throw std::invalid_argument("Cannot iterpret " + s_in + "!\""); }

    return num;
}

template <typename T, typename TR> size_t VirtualMachine<T, TR>::read_preamble(std::istream& input){
    if(!input) throw std::invalid_argument("Receied broken input stream!");

    size_t n_integer_constants, n_constants, n_instructions;

    std::string line;


    // line 1
    {
        std::getline(input, line);
        std::istringstream ss(line);
        if(!(ss  >> n_args>> n_registers>> n_integer_constants>> n_constants>> n_instructions>> return_register_index) ) {
            throw std::invalid_argument("Bad formatting encountered by VirtualMachine");
        }

        //dbg(n_args, n_registers, n_integer_constants, n_constants, n_instructions, return_register_index);

        if (return_register_index >= n_registers) throw std::out_of_range("Register index out of range");
    }


    // line 2
    {
        integer_constants.reserve(n_integer_constants);

        std::getline(input, line);
        if (!line.empty()) {
            std::istringstream ss(line);
            int num;
            while (ss >> num) {
                if (ss.fail()) throw std::invalid_argument("Bad formatting of integer constants or integer overflow encountered.");
                integer_constants.push_back(num);
            }
        }
        if (integer_constants.size() != n_integer_constants) throw std::invalid_argument("Invalid number of integer constants.");
        integer_constants.shrink_to_fit();
    }

    // line 3
    {
        constants.reserve(n_constants);

        std::getline(input, line);
        if (!line.empty()) {
            std::istringstream ss(line);
            std::string number;
            while (std::getline(ss, number, ' ')) {
                constants.push_back(read_rational_number<T>(number));
            }
        }
        if(constants.size() != n_constants) throw std::invalid_argument("Invalid number of constants.");
        constants.shrink_to_fit();
    }

    return n_instructions;
}

template <typename T, typename TR> void VirtualMachine<T, TR>::read_instructions(size_t n_instructions, std::istream& input){
    std::string line;
    instructions.reserve(n_instructions);

    while (std::getline(input, line)) {
        std::istringstream ss(line);

        int i_code;
        uint32_t r_out;

        ss >> i_code >> r_out;

        // TODO: more specialized checks depending on the code

        if (ss.fail()) throw std::invalid_argument("Bad formatting encountered by VirtualMachine");
        if (r_out >= n_registers) throw std::out_of_range("Register index out of range");

        std::vector<uint32_t> reg_list;

        size_t r_in;
        while (ss >> r_in) {
            //if (r_in >= n_registers) throw std::out_of_range("Register index out of range");
            if (ss.fail()) throw std::invalid_argument("Bad formatting encountered by VirtualMachine");
            reg_list.push_back(r_in);
        }

        auto op = static_cast<op_type>(i_code);

        if (!is_complex_v<T> and op == op_type::complex_conjugate) {
            throw std::invalid_argument("Encountered complex conjugation instruction for VM with non-complex type");
        }

        uint32_t* reg_list_raw = new uint32_t[reg_list.size()];
        std::copy(reg_list.begin(),reg_list.end(), reg_list_raw);

        size_t n_reg_list = reg_list.size();

        if(n_reg_list > std::numeric_limits<uint16_t>::max()) {
            throw std::invalid_argument("Number of input registers in a command is too large.");
        }

        instructions.emplace_back(reg_list_raw, r_out, static_cast<uint16_t>(n_reg_list), op);

        // Stop reading from istream after return is encountered
        if (op == op_type::ret) break;
    }

    if(instructions.back().op_code != op_type::ret) {
        throw std::invalid_argument("Bad formatting encountered by VirtualMachine. No return instruction encountered.");
    }
    if(instructions.size() != n_instructions) {
        throw std::invalid_argument("Bad formatting encountered by VirtualMachine. Received number of instructions that does not match the declared one.");
    }


    // for backwards compatibility the first "input" register actually encodes type of the output register
    if (bool return_is_vector = instructions.back().n_reg_input > 1;
        (return_is_vector and !std::is_same_v<TR, std::vector<T>>) or (!return_is_vector and !std::is_same_v<TR, T>)) {
        throw std::invalid_argument("VirtualMachine instantiated with a return type that is not compatible with the received string");
    }

    //instructions.pop_back();

    instructions.shrink_to_fit();
}

template <typename T, typename TR> VirtualMachine<T, TR>::VirtualMachine(std::istream& input){

    size_t n_instructions = read_preamble(input);

    read_instructions(n_instructions, input);

    //dbg(integer_constants);
    //dbg(constants);
    //for(const auto& [code, r_out, rs_in] : instructions) {
        //dbg(code, r_out, rs_in);
    //}
}

template <typename T, typename TR> VirtualMachine<T, TR>::VirtualMachine(std::string set_vm_id_string, std::istream& input) : vm_id_string(set_vm_id_string) {
    size_t n_instructions = read_preamble(input);

    bool stored_q = false;
    {
        std::lock_guard lock(global_instructions_storage_mutex);
        stored_q = (global_instructions_storage.count(vm_id_string) > 0);
    }

    // We only read instructions if they were not already read for this id  
    if (!stored_q) {
        read_instructions(n_instructions, input);

        std::lock_guard lock(global_instructions_storage_mutex);
        global_instructions_storage.try_emplace(vm_id_string);
        global_instructions_storage[vm_id_string].swap(instructions);
    }
    // we still need to skip through the input stream as if we were reading in case the stream has other stuff after
    else { 
        std::string line;
        while (std::getline(input, line)) {
            std::istringstream ss(line);

            int i_code;
            uint32_t r_out;

            ss >> i_code >> r_out;

            auto op = static_cast<op_type>(i_code);

            // Stop reading from istream after return is encountered
            if (op == op_type::ret) break;
        }
    }
}

template <typename T> inline T prod_pow(T x, int n) {
    if (n < 0) {
        return T{1}/::vm::prod_pow(x, -n);
    }
    switch (n) {
        case 0: return T{1};
        case 1: return x;
        case 2: return x * x;
        case 3: return x * x * x;
        case 4: x *= x; return x * x;
        default: break;
    }

    T result(1);

    while (n > 0) {
        if(n%2  == 1) {
            result *= x;
        }
        x *= x;
        n /= 2;
    }
    return result;
}


/**
 * Pairwise summation for improved numerical stability.
 * `start` is 0-based, `end` is inclusive.
 */
template <typename Tcontainer, size_t baseCaseThreshold = 10>
auto pairwise_sum_impl(const Tcontainer& data, size_t start, size_t end) {

    using T = std::remove_cv_t<std::remove_reference_t<decltype(data[0])>>;

    if (end - start + 1 <= baseCaseThreshold) {
        // Base case: If the range is small enough, perform naive summation
        T sum{};
        for (size_t i = start; i <= end; ++i) {
            sum += data[i];
        }
        return sum;
    }

    size_t mid = (start + end) / 2;

    T sum_left = pairwise_sum_impl<Tcontainer, baseCaseThreshold>(data, start, mid);
    T sum_right = pairwise_sum_impl<Tcontainer, baseCaseThreshold>(data, mid + 1, end);

    return sum_left + sum_right;
}

template <typename Tcontainer, size_t baseCaseThreshold = 10> auto pairwise_sum(const Tcontainer& data) { return pairwise_sum_impl(data, 0, data.size()-1); }

template <typename T, size_t baseCaseThreshold = 10> auto pairwise_sum(const std::vector<T>& registers, uint32_t* reg_in, uint16_t n_reg_input) {
    std::vector<T> data;
    data.reserve(n_reg_input);
    for (size_t i = 0; i < n_reg_input; ++i) {
        data.push_back(registers[reg_in[i]]);
    }
    return pairwise_sum_impl(data, 0, n_reg_input-1); 
}

template <typename T, typename TR> TR VirtualMachine<T, TR>::operator()(const std::vector<T>& args) {

    if(args.size() != n_args) throw std::invalid_argument("Invalid number of arguments received in VM!");

    /**
     * We have one register initialized to zero by default.
     */
    std::vector<T> registers(std::max(n_registers, size_t{1}));
    registers[0] = T{};
    
    const std::vector<instruction>& instructions_ref = instructions.empty() ? global_instructions_storage.at(vm_id_string) : instructions;

    for(const auto& [rs_in, r_out, n_reg_in, code] : instructions_ref) {
        switch(code) {
            case op_type::assign_c: {
                registers[r_out] = constants[rs_in[0]];
                break;
            }

            case op_type::assign_a: {
                registers[r_out] = args[rs_in[0]];
                break;
            }

            case op_type::assign_i: {
                registers[r_out] = static_cast<T>(integer_constants[rs_in[0]]);
                break;
            }

            case op_type::add: {
                registers[r_out] = registers[rs_in[0]] + registers[rs_in[1]];
                break;
            }

            case op_type::add_many: {
                if (n_reg_in <= 10) {
                    T tt{0};
                    for (uint16_t i = 0; i < n_reg_in; ++i) { tt += registers[rs_in[i]]; }
                    registers[r_out] = tt;
                }
                else {
                    registers[r_out] = pairwise_sum(registers, rs_in, n_reg_in);
                }
                break;
            }

            case op_type::mul: {
                registers[r_out] = registers[rs_in[0]] * registers[rs_in[1]];
                break;
            }

            case op_type::mul_many: {
                T tt{1};
                for (uint16_t i = 0; i < n_reg_in; ++i) { tt *= registers[rs_in[i]]; }
                registers[r_out] = tt;
                break;
            }

            case op_type::neg: {
                registers[r_out] = -registers[rs_in[0]];
                break;
            }

            case op_type::sq: {
                registers[r_out] = registers[rs_in[0]] * registers[rs_in[0]];
                break;
            }

            case op_type::inv: {
                registers[r_out] = T{1}/registers[rs_in[0]];
                break;
            }

            case op_type::power_int: {
                registers[r_out] = ::vm::prod_pow(registers[rs_in[0]], integer_constants[rs_in[1]]);
                break;
            }

            case op_type::fetch_constant: {
                // NOTE: -1 here is because Mathematica uses 1-based element index
                registers[r_out] = constants[integer_constants[rs_in[0]]-1];
                break;
            }

            case op_type::complex_conjugate: {
                if constexpr (is_complex_v<T>) {
                    using std::conj;
                    registers[r_out] = conj(registers[rs_in[0]]);
                }
                else {
                    // this is not allowed by the constructor for non-complex types
                    break;
                }
            }
            
            // This should have been validate during initialization to be the last instruction, so we don't do anything
            case op_type::ret: break;
        }
    }
    

    /*
     * Here we check if the return is actually from an integer register of is a vector. 
     * This can happen in the trivial corner case when the machine must just return an integer. 
     * Here this is done a awkwardly for backwards compatibility: we check if return statement has the third argument;
     *  if it is 2, the return is from integer registers
     *  if it is 3 or not provided, it is normal register
     *
     *  If the length of input registers is greater than 1, the return is a vector, 
     *  and we construct it's entries
     */
    if constexpr (std::is_same_v<TR,T>) {
        if((instructions_ref.back().n_reg_input>0) and instructions_ref.back().reg_input[0]==2) {
            return static_cast<T>(integer_constants[return_register_index]);
        }
        else 
            return registers[return_register_index];
    }
    else {
        const auto& return_instruction = instructions_ref.back();
        std::vector<T> to_return(return_instruction.n_reg_input -1);
        for (size_t i = 1; i < return_instruction.n_reg_input; ++i) {
            to_return[i-1] = registers[return_instruction.reg_input[i]];
        }
        return to_return;
    }
}

} // namespace vm
