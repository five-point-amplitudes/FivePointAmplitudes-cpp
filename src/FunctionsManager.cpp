#include "FunctionsManager.h"
#include <stdexcept>

namespace FivePointAmplitudes {

namespace detail {
template <typename T> std::complex<double> to_double(const std::complex<T>& c) { return {to_double(c.real()), to_double(c.imag())}; }
} // namespace detail

template <KinType KT> size_t FunctionsManagerG<KT>::add(const FunID<KT>& fid) {
    std::lock_guard<std::mutex> _lock(_mtx);

    if (locked) { throw std::logic_error("FunctionsManagerG does not accept new functions after the first evaluation!"); }

    auto find = _function_indices.find(fid);

    if (find != _function_indices.end()) return find->second;

    _evaluators.push_back(fid.template get_evaluator<R>());
    last_values.push_back({});

#ifdef FivePointAmplitudes_QD_ENABLED
    _evaluators_HP.push_back(fid.template get_evaluator<dd_real>());
    last_values_HP.push_back({});
#endif // FivePointAmplitudes_QD_ENABLED


    size_t new_id = _evaluators.size() - 1;

    _function_indices[fid] = new_id;

    return new_id;
}

template <KinType KT> const std::vector<typename FunctionsManagerG<KT>::C>& FunctionsManagerG<KT>::eval(const KinR& kin) {
    if(manual_cache_flush && value_stored) return last_values;

    std::lock_guard<std::mutex> _lock(_mtx);

    const size_t N = _evaluators.size();

    assert(last_values.size() == N);

    if (kin.v == last_kin.v) return last_values;

    for (size_t i = 0; i < N; ++i) { last_values[i] = _evaluators[i](kin); }
    last_kin = kin;

    locked = true;

    if (manual_cache_flush) { value_stored = true; }

    return last_values;
}

#ifdef FivePointAmplitudes_QD_ENABLED 
template <KinType KT> const std::vector<typename FunctionsManagerG<KT>::CHP>& FunctionsManagerG<KT>::eval(const KinHP& kin) {return eval_HP(kin);}

template <KinType KT> const std::vector<typename FunctionsManagerG<KT>::CHP>& FunctionsManagerG<KT>::eval_HP(const KinHP& kin) {
    if(manual_cache_flush && value_stored_HP) return last_values_HP;

    std::lock_guard<std::mutex> _lock(_mtx);

    const size_t N = _evaluators.size();

    assert(last_values_HP.size() == N);

    if (kin.v == last_kin_HP.v) return last_values_HP;

    for (size_t i = 0; i < N; ++i) {
        last_values_HP[i] = _evaluators_HP[i](kin); 
        last_values[i] = detail::to_double(last_values_HP[i]); 
    }
    last_kin_HP = kin;

    for (size_t i = 0; i < kin.v.size(); ++i) {
        last_kin.v[i] = to_double(kin.v[i]);
    }
    last_kin = Kin<R, KT>{last_kin.v};

    locked = true;

    if (manual_cache_flush) { 
        value_stored = true;
        value_stored_HP = true; 
    }

    return last_values_HP;
}
#endif // FivePointAmplitudes_QD_ENABLED


template <KinType KT> void FunctionsManagerG<KT>::flush() {
    if (!manual_cache_flush) {
        throw std::runtime_error("Manual cache flush in FunctionsManagerG called, but was not enabled!");
    }

    value_stored = false;

#ifdef FivePointAmplitudes_QD_ENABLED 
    value_stored_HP = false;
#endif // FivePointAmplitudes_QD_ENABLED
}


f_type from_string(std::string s) {
    if ( s == "F" ) return f_type::f;
    if ( s == "bc" ) return f_type::bc;
    if ( s == "bci" ) return f_type::bci;
    if ( s == "isqrt" ) return f_type::isqrt;
    if ( s == "str5" ) return f_type::str5;
    if ( s == "one" ) return f_type::one;

    throw std::runtime_error("Unidentified f_type "+s);
}

#ifdef PENTAGON_FUNCTIONS_M0_ENABLED
template class FunctionsManagerG<KinType::m0>;
#endif
#ifdef PENTAGON_FUNCTIONS_M1_ENABLED 
template class FunctionsManagerG<KinType::m1>;
#endif // PENTAGON_FUNCTIONS_M1_ENABLED


    
} // FivePointAmplitudes
