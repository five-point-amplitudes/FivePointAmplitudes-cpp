#include "Timing.h"

namespace timing {

void counter::start() { interval_begin = clock::now(); }
void counter::stop() {
    auto end = clock::now();
    total += std::chrono::duration_cast<duration_type>(end - interval_begin);
}

void counter::report() {
    using namespace std::chrono;
    auto ns = total;
    auto h = duration_cast<hours>(ns);
    ns -= h;
    auto m = duration_cast<minutes>(ns);
    ns -= m;
    auto s = duration_cast<seconds>(ns);
    ns -= s;
    auto ms = duration_cast<milliseconds>(ns);

    std::cout << name;
    if (type != "") { std::cout << "<" << type << ">"; }
    std::cout << " took\n\t";
    if (h.count() > 0) { std::cout << h.count() << "h "; }
    if (h.count() > 0 or m.count() > 0) { std::cout << m.count() << "m "; }
    if (h.count() > 0 or m.count() > 0 or s.count() > 0) { std::cout << s.count() << "s "; }
    std::cout << ms.count() << "ms" << std::endl;
}

counter::duration_type counter::get_duration() const { return total; }

counter::~counter() { report(); }

counter _rational_functions_timing("rational functions");
counter _transcendental_functions_timing("transcendental functions");
counter _summations_timing("summations");

} // namespace timing
