#pragma once
#include "Spinor.h"

namespace FivePointAmplitudes {

template <typename T> class SpinorRemainderBase {
  public:
    using TC = std::complex<T>;
    virtual TC eval(const mom_conf<T>&, T) = 0;
    virtual ~SpinorRemainderBase(){};
};

} // namespace FivePointAmplitudes
