#pragma once

#include <FunctionID.h>
#include <Constants.h>

#include <unordered_map>
#include <complex>
#include <mutex>
#include <cassert>
#include <vector>
#include <memory>

#include "FivePointAmplitudes_config.h"

#ifdef FivePointAmplitudes_QD_ENABLED 
#include <qd/dd_real.h>
#endif // FivePointAmplitudes_QD_ENABLED

namespace FivePointAmplitudes {
using namespace PentagonFunctions;


namespace detail {

template <typename T, KinType KT> std::array<T, Kin<T, KT>::Nvis> base_point() {
    if constexpr (KT == KinType::m0) { return {T(3), T(-1), T(1), T(1), T(-1)}; }
    else if constexpr (KT == KinType::m1) {
        return {T(1), T(3), T(2), T(-2), T(7), T(-2)};
    }
}

} // detail

template <KinType KT> class FunctionsManagerG {
  public:
    using R = double;
    using C = std::complex<double>;

#ifdef FivePointAmplitudes_QD_ENABLED 
    using RHP = dd_real;
    using CHP = std::complex<dd_real>;
#endif // FivePointAmplitudes_QD_ENABLED

    using KinR = Kin<R, KT>;
#ifdef FivePointAmplitudes_QD_ENABLED 
    using KinHP = Kin<RHP, KT>;
#endif // FivePointAmplitudes_QD_ENABLED

  private:
    std::unordered_map<FunID<KT>, size_t> _function_indices;

    std::vector<FunctionObjectType<double, KT>> _evaluators;

    std::vector<C> last_values;
    KinR last_kin{detail::base_point<R, KT>()};

#ifdef FivePointAmplitudes_QD_ENABLED 
    std::vector<FunctionObjectType<RHP, KT>> _evaluators_HP;
    std::vector<CHP> last_values_HP;
    KinHP last_kin_HP{detail::base_point<RHP, KT>()};
#endif // FivePointAmplitudes_QD_ENABLED


  public:
    /**
     * Add a function to the manager
     */
    size_t add(const FunID<KT>&);
    /**
     * Evaluate the functions and return the result.
     * Consecutive calls with the same kinematics evaluate only once.
     */
    const std::vector<C>& eval(const KinR&);
#ifdef FivePointAmplitudes_QD_ENABLED
    const std::vector<CHP>& eval(const KinHP&);
    const std::vector<CHP>& eval_HP(const KinHP&);
#endif // FivePointAmplitudes_QD_ENABLED

    size_t size() const { return _evaluators.size(); }

    FunctionsManagerG() = default;
    FunctionsManagerG(const FunctionsManagerG&) = default;
    FunctionsManagerG(FunctionsManagerG&&) = default;
    FunctionsManagerG& operator=(const FunctionsManagerG&) = default;
    FunctionsManagerG& operator=(FunctionsManagerG&&) = default;

    FunctionsManagerG(bool _set_manual_cache_flush) : manual_cache_flush{_set_manual_cache_flush} {};

    /**
     * Forget currently stored values.
     * Will reevaluate all functions when the next eval() is called.
     */
    void flush();

  private:
    bool locked{false}; /// will not allow adding more functions once locked after the first evaluation
    bool manual_cache_flush{false}; /// if this is set to true, always return the last value until flush() is called, very dangerous!

    bool value_stored{false};
#ifdef FivePointAmplitudes_QD_ENABLED 
    bool value_stored_HP{false};
#endif // FivePointAmplitudes_QD_ENABLED
    mutable std::mutex _mtx;
};


/**
 * For backwards compatibility
 */
using FunctionsManager = FunctionsManagerG<KinType::m0>;

/**
 * Represents (abstractly) the type of entry in the basis of functions
 * f    - function
 * bc   - real boundary constant
 * bci  - imaginary boundary constant
 * str5 - sign(Im(tr5))
 * isqrt - inverse square root (index should correspond to the letter of alphabet)
 * one  - literally one
 */
enum class f_type { f, bc, bci, str5, isqrt, one };
f_type from_string(std::string);

} // namespace FivePointAmplitudes
