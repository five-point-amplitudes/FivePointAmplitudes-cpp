#pragma once

#include "Kinematics.h"
#include <cmath>

namespace  FivePointAmplitudes {

template <typename T> T m_m_p_p_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(pow(s(1, 2), 3) / (s(1, 5) * s(2, 3) * s(3, 4) * s(4, 5)));
}

template <typename T> T m_p_m_p_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(pow(-s(4, 5) + s(1, 2) + s(2, 3), 4) / (s(1, 2) * s(1, 5) * s(2, 3) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_m_p_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-(pow(s(2, 3), 2) * (s(1, 2) + s(2, 3) + s(4, 5) * T(-1))) / (s(1, 2) * s(1, 5) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_p_m_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-((s(1, 5) + s(4, 5) + s(2, 3) * T(-1)) * pow(s(1, 5) + s(2, 3) * T(-1) + s(3, 4) * T(-1), 3)) /
               (s(1, 2) * s(1, 5) * s(2, 3) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_p_p_m__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-pow(s(1, 2) + s(1, 5) + s(3, 4) * T(-1), 3) / (s(1, 2) * s(2, 3) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_Qp_Qm_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-(pow(s(2, 3) + s(3, 4) + s(1, 5) * T(-1), 2) * (s(1, 5) + s(4, 5) + s(2, 3) * T(-1))) / (s(1, 2) * s(1, 5) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_Qm_Qp_p__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-(pow(s(2, 3), 2) * (s(1, 5) + s(4, 5) + s(2, 3) * T(-1))) / (s(1, 2) * s(1, 5) * s(3, 4) * s(4, 5)));
}

template <typename T> T qp_qm_Qm_Qp_m__tree_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs(-pow(s(1, 5) + s(4, 5) + s(2, 3) * T(-1), 3) / (s(1, 2) * s(1, 5) * s(3, 4) * s(4, 5)));
}

template <typename T> T p_p_p_p_p__norm_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs((pow(s(1, 2), 5) * s(1, 3)) / (pow(s(1, 4), 2) * pow(s(1, 5), 2) * pow(s(2, 3), 3)));
}

template <typename T> T m_p_p_p_p__norm_squared(const sij5<T>& s) {
    using std::abs;
    using std::pow;
    return abs((pow(s(1, 2), 5) * pow(s(1, 3), 3)) / (pow(s(1, 4), 2) * pow(s(1, 5), 2) * pow(s(2, 3), 5)));
}

template <typename T> T qp_qm_p_p_p__norm_squared(const sij5<T>& s) {

    // this is the square of the spinor weight which was used for reconstruction:
    T result = s(1, 3) / (s(1, 2) * s(1, 4) * s(1, 5) * s(2, 3));
    result *= result;
    result /= s(1, 2);

    // and here is the (square of) the factor which makes the helicity remainders with vanishing trees dimensionless:
    using std::abs;
    using std::pow;
    result *= pow(s(1, 2), 6);

    // so overall this factor has units  = -2, as the tree amplitude squared

    return abs(result);
}

    
} //  FivePointAmplitudes


