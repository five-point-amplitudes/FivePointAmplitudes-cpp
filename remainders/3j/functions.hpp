#pragma once

#include "Kinematics.h"
#include "Spinor.h"

#include "Utilities.h"
#include "matchit/matchit.h"
#include "prod_pow.h"

namespace FivePointAmplitudes{
namespace jjj {

template <typename T> std::vector<T> get_sij_variables(const mom_conf<T>& mc) {
    return {mc.s(1, 2), mc.s(2, 3), mc.s(3, 4), mc.s(4, 5), mc.s(1, 5)};
}

template <typename T> std::vector<add_complex_t<T>> get_spinor_variables(const mom_conf<T>& mc) {
    return {spaa(mc[1], mc[2]),        spaa(mc[1], mc[3]),        spaa(mc[1], mc[4]),        spaa(mc[1], mc[5]),        spaa(mc[2], mc[3]),
            spaa(mc[2], mc[4]),        spaa(mc[2], mc[5]),        spaa(mc[3], mc[4]),        spaa(mc[3], mc[5]),        spaa(mc[4], mc[5]),
            spab(mc[1], mc[2], mc[1]), spab(mc[1], mc[3], mc[1]), spab(mc[1], mc[4], mc[1]), spab(mc[1], mc[5], mc[1]), spab(mc[2], mc[1], mc[2]),
            spab(mc[2], mc[3], mc[2]), spab(mc[2], mc[4], mc[2]), spab(mc[2], mc[5], mc[2]), spab(mc[3], mc[1], mc[3]), spab(mc[3], mc[2], mc[3]),
            spab(mc[3], mc[4], mc[3]), spab(mc[3], mc[5], mc[3]), spab(mc[4], mc[1], mc[4]), spab(mc[4], mc[2], mc[4]), spab(mc[4], mc[3], mc[4]),
            spab(mc[4], mc[5], mc[4]), spab(mc[5], mc[1], mc[5]), spab(mc[5], mc[2], mc[5]), spab(mc[5], mc[3], mc[5]), spab(mc[5], mc[4], mc[5]),
            spbb(mc[1], mc[2]),        spbb(mc[1], mc[3]),        spbb(mc[1], mc[4]),        spbb(mc[1], mc[5]),        spbb(mc[2], mc[3]),
            spbb(mc[2], mc[4]),        spbb(mc[2], mc[5]),        spbb(mc[3], mc[4]),        spbb(mc[3], mc[5]),        spbb(mc[4], mc[5])};
}

template <typename T> std::vector<add_complex_t<T>> get_inverse_denominators(const std::vector<add_complex_t<T>>& abb) {
    std::vector<add_complex_t<T>> qs{abb[0],
                      abb[1],
                      abb[2],
                      abb[3],
                      abb[4],
                      abb[5],
                      abb[6],
                      abb[7],
                      abb[8],
                      abb[9],
                      abb[30],
                      abb[31],
                      abb[32],
                      abb[33],
                      abb[34],
                      abb[35],
                      abb[36],
                      abb[37],
                      abb[38],
                      abb[39],
                      abb[10] + abb[11],
                      abb[10] + abb[12],
                      abb[11] + abb[12],
                      abb[10] + abb[13],
                      abb[11] + abb[13],
                      abb[12] + abb[13],
                      abb[14] + abb[15],
                      abb[14] + abb[16],
                      abb[15] + abb[16],
                      abb[14] + abb[17],
                      abb[15] + abb[17],
                      abb[16] + abb[17],
                      abb[18] + abb[19],
                      abb[18] + abb[20],
                      abb[19] + abb[20],
                      abb[18] + abb[21],
                      abb[19] + abb[21],
                      abb[20] + abb[21],
                      abb[22] + abb[23],
                      abb[22] + abb[24],
                      abb[23] + abb[24],
                      abb[22] + abb[25],
                      abb[23] + abb[25],
                      abb[24] + abb[25],
                      abb[26] + abb[27],
                      abb[26] + abb[28],
                      abb[27] + abb[28],
                      abb[26] + abb[29],
                      abb[27] + abb[29],
                      abb[28] + abb[29]};

    for (auto& q : qs) q = 1 / q;

    return qs;
}

template <typename T> std::complex<T> tree_5g_pppmm(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){
   return prod_pow(spaa(mc[perm[3]],mc[perm[4]]), 3) / (spaa(mc[perm[0]],mc[perm[1]]) * spaa(mc[perm[1]],mc[perm[2]]) * spaa(mc[perm[2]],mc[perm[3]]) * spaa(mc[perm[4]],mc[perm[0]]));
}

template <typename T> std::complex<T> tree_5g_ppmpm(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){
    return prod_pow(spaa(mc[perm[2]],mc[perm[4]]), 4) / (spaa(mc[perm[0]],mc[perm[1]]) * spaa(mc[perm[1]],mc[perm[2]]) * spaa(mc[perm[2]],mc[perm[3]]) * spaa(mc[perm[3]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[0]]));
}


template <typename T> std::complex<T> tree_2q3g_pmppm(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){

    auto den = spaa(mc[perm[4]],mc[perm[0]]);

    for (size_t i = 0; i <= 3; ++i) {
        den *= spaa(mc[perm[i]],mc[perm[i+1]]);
    }

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[4]]),3) * spaa(mc[perm[0]],mc[perm[4]]);

    return num/den;
}

template <typename T> std::complex<T> tree_2q3g_pmpmp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){

    auto den = spaa(mc[perm[4]],mc[perm[0]]);

    for (size_t i = 0; i <= 3; ++i) {
        den *= spaa(mc[perm[i]],mc[perm[i+1]]);
    }

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[3]]),3) * spaa(mc[perm[0]],mc[perm[3]]);

    return num/den;
}

template <typename T> std::complex<T> tree_2q3g_pmmpp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){

    auto den = spaa(mc[perm[4]],mc[perm[0]]);

    for (size_t i = 0; i <= 3; ++i) {
        den *= spaa(mc[perm[i]],mc[perm[i+1]]);
    }

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[2]]),3) * spaa(mc[perm[0]],mc[perm[2]]);

    return num/den;
}




template <typename T> std::complex<T> tree_4q1g_pmpmp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){

    //qbpqmqbpqmp_QQbarQQbarG_OL_Nc0_Nf0
    //tt[{},m,p]*tt[{p},m,p]

    //(-⟨4|1⟩⟨2|3⟩⟨2|4⟩²)/(⟨4|1⟩⟨1|2⟩⟨2|5⟩⟨5|3⟩⟨3|4⟩)

    auto den = spaa(mc[perm[2]],mc[perm[3]]) * spaa(mc[perm[0]],mc[perm[1]]) * spaa(mc[perm[1]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[2]]);

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[3]]),2) * spaa(mc[perm[2]],mc[perm[1]]);

    return num/den;
}

template <typename T> std::complex<T> tree_4q1g_pmmpp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){

    //qbpqmqmqbpp_QQbarQQbarG_OL_Nc0_Nf0
    //tt[{},p,p]*tt[{p},m,m]

    //(⟨4|1⟩⟨2|3⟩⟨2|3⟩²)/(⟨4|1⟩⟨1|2⟩⟨2|5⟩⟨5|3⟩⟨3|4⟩)

    auto den = spaa(mc[perm[2]],mc[perm[3]]) *  spaa(mc[perm[0]],mc[perm[1]]) * spaa(mc[perm[1]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[2]]);

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[2]]),3);

    return num/den;
}

template <typename T> std::complex<T> tree_4q1g_mppmp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){
    //qmqbpqbpqmp_QQbarQQbarG_OL_Nc0_Nf0
    //tt[{},m,m]*tt[{p},p,p]

    //(⟨4|1⟩⟨2|3⟩⟨1|4⟩²)/(⟨4|1⟩⟨1|2⟩⟨2|5⟩⟨5|3⟩⟨3|4⟩)

    auto den = spaa(mc[perm[2]],mc[perm[3]]) *  spaa(mc[perm[0]],mc[perm[1]]) * spaa(mc[perm[1]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[2]]);

    auto num = prod_pow(spaa(mc[perm[0]],mc[perm[3]]),2) * spaa(mc[perm[1]],mc[perm[2]]);

    return num/den;
}


template <typename T> std::complex<T> tree_4q1g_m1_pmpmp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){
    //qbpqmqbpqmp_QQbarQQbarG_OL_Ncm1_Nf0
    //tt[{},m,p]*tt[{p},m,p]

    //(⟨1|4⟩⟨3|2⟩⟨2|4⟩²)/(⟨2|1⟩⟨1|4⟩⟨4|5⟩⟨5|3⟩⟨3|2⟩)

    auto den = spaa(mc[perm[1]],mc[perm[0]]) * spaa(mc[perm[3]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[2]]);

    auto num = prod_pow(spaa(mc[perm[1]],mc[perm[3]]),2);

    return num/den;
}

    
template <typename T> std::complex<T> tree_4q1g_m1_pmmpp(const mom_conf<T>& mc, const Permutation& perm = {1,2,3,4,5}){
    //qbpqmqmqbpp_QQbarQQbarG_OL_Ncm1_Nf0
    //tt[{},m,p]*tt[{p},p,m]

    //(-⟨1|4⟩⟨3|2⟩⟨2|3⟩²)/(⟨2|1⟩⟨1|4⟩⟨4|5⟩⟨5|3⟩⟨3|2⟩)


    auto den = spaa(mc[perm[1]],mc[perm[0]]) * spaa(mc[perm[3]],mc[perm[4]]) * spaa(mc[perm[4]],mc[perm[2]]);

    auto num = -prod_pow(spaa(mc[perm[1]],mc[perm[2]]),2);

    return num/den;
}

} // jjj
} // FivePointAmplitudes
