#pragma once

#include "FivePointAmplitudes_config.h"
#include "Utilities.h"

#include "RemainderEvaluatorVM.h"
#include "Spinor.h"

#include "matchit/matchit.h"


#include <array>
#include <memory>
#include <stdexcept>


namespace FivePointAmplitudes {

namespace Vjj {

template <typename T> std::vector<T> get_sij_variables(const mom_conf<T>& mc) {
    return {mc.s(5, 6), mc.s(5, 6,1), mc.s(1, 2), mc.s(2, 3), mc.s(3, 4), mc.s(4, 5, 6)};
}


template <typename T> std::complex<T> get_V_over_photon_propagtor (const T& M_V, const T& G_V, const T& s) {
    using TC = add_complex_t<T>;

    TC num = s;

    TC den = s - M_V*M_V + std::complex<T>{T{}, G_V*M_V};

    num /= den;

    return num;
}

template <typename T> T get_v_coupling (std::string type, T Q_q, const T& cw) {
    using namespace matchit;
    using std::sqrt;

    //T cw = M_W/M_Z;
    T sw_sq = T{1}-cw*cw;

    T s2w = T{2}*cw*sqrt(sw_sq);

    // NOTE: we assume here that positively charged (2/3) quarks are up, and negative (-1/3) are down

    int sign;

    if constexpr (is_complex_v<T>)
        sign = Q_q.real() > 0 ? 1 : -1;
    else
        sign = Q_q > 0 ? 1 : -1;

    auto f = matchit::match(type) (
            pattern | "e_L" = (T{-1} + 2*sw_sq),
            pattern | "e_R" = (2*sw_sq),

            pattern | "q_L" = (static_cast<T>(sign) - 2*Q_q*sw_sq),
            pattern | "q_R" = (-2*Q_q*sw_sq),

            pattern | _ = T{0}
    );

    if (f==T{0}) {
        throw std::runtime_error("unsupported coupling type in Vjj::get_v_coupling");
    }

    f /= s2w;


    return f;
}

inline auto swap_v_L(std::string v_L) -> std::string {
    return matchit::match(v_L) (
            matchit::pattern | "e_L" = "e_R",
            matchit::pattern | "e_R" = "e_L",
            matchit::pattern | matchit::_ = ""
    );
}

inline auto swap_v_q (std::string v_L) -> std::string {
    return matchit::match(v_L) (
            matchit::pattern | "q_L" = "q_R",
            matchit::pattern | "q_R" = "q_L",
            matchit::pattern | matchit::_ = ""
    );
}



template <typename T> std::vector<T> get_inverse_denominators(const std::vector<T>& v) {

    T z[40];
    z[0] = v[2] + v[3];
    z[1] = v[3] + v[4];
    z[2] = v[1] + v[2];
    z[3] = v[0] + -v[2];
    z[4] = v[0] + -v[5];
    z[5] = v[2] + -v[5];
    z[6] = v[3] + -v[5];
    z[7] = -v[0] + v[1];
    z[8] = -v[1] + v[3];
    z[9] = -v[1] + v[4];
    z[10] = v[3] + z[5];
    z[11] = v[4] + z[8];
    z[12] = v[2] + -z[9];
    z[13] = -v[4] + z[5];
    z[14] = -v[2] + -z[7];
    z[15] = v[5] + z[7];
    z[16] = v[0] * v[3];
    z[17] = v[1] * v[5];
    z[18] = z[16] + -z[17];
    z[19] = v[0] * v[2];
    z[20] = prod_pow(v[5], 2);
    z[21] = z[19] + -z[20];
    z[22] = z[6] + -z[7];
    z[23] = z[5] + z[8];
    z[24] = -z[7] + z[10];
    z[25] = z[18] + z[19];
    z[26] = v[4] * v[5];
    z[27] = z[18] + z[26];
    z[28] = v[1] * v[2];
    z[29] = z[18] + z[28];
    z[30] = -v[3] * z[7];
    z[30] = -z[28] + z[30];
    z[31] = v[2] * z[4];
    z[32] = z[18] + z[31];
    z[33] = 2 * v[0];
    z[34] = -v[1] + z[33];
    z[34] = v[1] * z[34];
    z[35] = prod_pow(v[0], 2);
    z[34] = z[34] + -z[35];
    z[19] = -z[19] + -z[34];
    z[26] = z[26] + z[31];
    z[36] = v[0] * v[5];
    z[36] = -z[20] + z[36];
    z[37] = z[26] + -z[36];
    z[26] = z[18] + z[26];
    z[38] = v[4] * z[7];
    z[28] = -z[28] + z[38];
    z[38] = z[18] + -z[28];
    z[39] = v[1] * z[7];
    z[28] = -z[28] + z[39];
    z[39] = -v[1] + -2 * v[5];
    z[39] = v[1] * z[39];
    z[16] = 4 * z[16] + -z[20] + z[39];
    z[20] = v[2] + -z[33];
    z[20] = v[2] * z[20];
    z[33] = -v[0] + -v[2];
    z[33] = v[4] + 2 * z[33];
    z[33] = v[4] * z[33];
    z[20] = z[20] + z[33] + z[35];
    z[33] = v[3] * v[5];
    z[17] = -z[17] + z[33] + z[36];
    z[31] = -z[17] + z[31];
    z[33] = -v[2] * z[15];
    z[17] = -z[17] + z[33];
    z[33] = v[3] * z[22];
    z[35] = -v[1] + 2 * v[3] + z[5];
    z[35] = v[2] * z[35];
    z[33] = z[33] + z[35];
    z[35] = v[1] + z[5];
    z[35] = v[2] * z[35];
    z[36] = -2 * v[2] + v[5] + z[9];
    z[36] = v[4] * z[36];
    z[35] = z[18] + z[35] + z[36];
    z[36] = v[3] + -2 * z[7];
    z[36] = v[3] * z[36];
    z[39] = -v[0] + z[8];
    z[39] = v[2] + 2 * z[39];
    z[39] = v[2] * z[39];
    z[34] = -z[34] + z[36] + z[39];

    std::vector<T> qs{v[0], v[2], v[3], v[4], v[5], v[1], z[0], z[1], z[2], z[3], z[4], z[5], z[6], -z[7], z[8], z[9], z[10], z[11], z[12], z[13], z[14], -z[15], z[18], z[21], z[22], z[23], z[24], z[25], z[27], z[29], z[30], z[32], z[19], z[37], z[26], z[38], z[28], z[16], z[20], z[31], z[17], z[33], z[35], z[34]};

    for(auto& q : qs) q=1/q;

    return qs;
}



template <typename T> std::complex<T> gluonspp_tree(const mom_conf<T>& mc){
auto result = (prod_pow(spaa(mc[4],mc[6]), 2) * T(2)) / (spaa(mc[1],mc[2]) * spaa(mc[2],mc[3]) * spaa(mc[3],mc[4]) * spaa(mc[6],mc[5]));
return std::complex{-result.imag(), result.real()};
}


template <typename T> T gluonspp_remainder_from_formfactors(const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {
std::array<T, 20> abb;
abb[0] = mc.s(5,6);
abb[1] = spaa(mc[3],mc[4]);
abb[2] = spaa(mc[4],mc[3]);
abb[3] = spaa(mc[4],mc[6]);
abb[4] = spaa(mc[6],mc[5]);
abb[5] = spab(mc[4],mc[5],mc[1]);
abb[6] = spab(mc[4],mc[5],mc[2]);
abb[7] = spab(mc[4],mc[5],mc[3]);
abb[8] = spab(mc[4],mc[6],mc[1]);
abb[9] = spab(mc[4],mc[6],mc[2]);
abb[10] = spab(mc[4],mc[6],mc[3]);
abb[11] = spab(mc[6],mc[5],mc[1]);
abb[12] = spab(mc[6],mc[5],mc[2]);
abb[13] = spab(mc[6],mc[5],mc[3]);
abb[14] = spbb(mc[1],mc[2]);
abb[15] = spbb(mc[1],mc[3]);
abb[16] = spbb(mc[1],mc[5]);
abb[17] = spbb(mc[2],mc[3]);
abb[18] = spbb(mc[2],mc[5]);
abb[19] = spbb(mc[3],mc[5]);

{
T z[12];
z[0] = 1 / (abb[2] * prod_pow(abb[0] * abb[3] * abb[14] * abb[15] * abb[17], 2));
z[1] = abb[7] + abb[10] * (T(1) / T(2));
z[1] = abb[10] * z[1];
z[2] = prod_pow(abb[7], 2);
z[1] = z[1] + (T(1) / T(2)) * z[2];
z[2] = prod_pow(abb[14], 2);
z[3] = abb[1] * z[0];
z[1] = formFactor[2] * z[1] * z[2] * z[3];
z[4] = (T(1) / T(2)) * formFactor[0];
z[5] = prod_pow(abb[17], 2);
z[6] = z[4] * z[5];
z[7] = abb[8] * z[6];
z[8] = abb[5] * formFactor[0];
z[9] = z[5] * z[8];
z[7] = z[7] + z[9];
z[7] = abb[8] * z[7];
z[9] = prod_pow(abb[5], 2);
z[6] = z[6] * z[9];
z[6] = z[6] + z[7];
z[7] = abb[9] + abb[6] * (T(1) / T(2));
z[7] = abb[6] * z[7];
z[10] = prod_pow(abb[9], 2);
z[7] = z[7] + (T(1) / T(2)) * z[10];
z[10] = prod_pow(abb[15], 2);
z[7] = formFactor[1] * z[7] * z[10];
z[11] = z[6] + -z[7];
z[11] = z[3] * z[11];
z[11] = z[1] + z[11];
z[10] = abb[12] * abb[18] * z[10] * z[11];
z[6] = z[6] + z[7];
z[6] = z[3] * z[6];
z[6] = -z[1] + z[6];
z[2] = abb[13] * abb[19] * z[2] * z[6];
z[6] = -z[4] * z[9];
z[4] = abb[8] * z[4];
z[4] = -z[4] + -z[8];
z[4] = abb[8] * z[4];
z[4] = z[4] + z[6];
z[4] = prod_pow(abb[17], 4) * z[4];
z[6] = z[5] * z[7];
z[4] = z[4] + z[6];
z[3] = z[3] * z[4];
z[1] = z[1] * z[5];
z[1] = z[1] + z[3];
z[1] = abb[11] * abb[16] * z[1];
z[1] = z[1] + z[2] + z[10];
return abb[4] * z[1];
}

}


template <typename T> std::complex<T> gluonsmp_tree(const mom_conf<T>& mc){
auto result = ((-(mc.s(1,2,3) * spaa(mc[2],mc[4]) * (spaa(mc[2],mc[4]) * (spab(mc[6],mc[2],mc[3]) + spab(mc[6],mc[4],mc[3])) * spbb(mc[1],mc[2]) + mc.s(2,3,4) * spaa(mc[4],mc[6]) * spbb(mc[1],mc[3])) * spbb(mc[1],mc[5])) + mc.s(2,3,4) * spaa(mc[3],mc[4]) * spaa(mc[4],mc[6]) * (spab(mc[2],mc[1],mc[5]) + spab(mc[2],mc[3],mc[5])) * prod_pow(spbb(mc[1],mc[3]), 2)) * T(2)) / (mc.s(2,3) * mc.s(6,5) * mc.s(1,2,3) * mc.s(2,3,4) * spaa(mc[3],mc[4]) * spbb(mc[1],mc[2]));
return std::complex{-result.imag(), result.real()};
}


template <typename T> T gluonsmp_remainder_from_formfactors(const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {
std::array<T, 36> abb;
abb[0] = mc.s(5,6);
abb[1] = mc.s(1,2,3);
abb[2] = mc.s(2,3,4);
abb[3] = spaa(mc[1],mc[2]);
abb[4] = spaa(mc[2],mc[3]);
abb[5] = spaa(mc[2],mc[4]);
abb[6] = spaa(mc[3],mc[4]);
abb[7] = spaa(mc[4],mc[2]);
abb[8] = spaa(mc[4],mc[3]);
abb[9] = spaa(mc[4],mc[6]);
abb[10] = spab(mc[2],mc[1],mc[5]);
abb[11] = spab(mc[2],mc[3],mc[5]);
abb[12] = spab(mc[2],mc[5],mc[2]);
abb[13] = spab(mc[2],mc[5],mc[3]);
abb[14] = spab(mc[2],mc[6],mc[2]);
abb[15] = spab(mc[2],mc[6],mc[3]);
abb[16] = spab(mc[4],mc[5],mc[1]);
abb[17] = spab(mc[4],mc[5],mc[2]);
abb[18] = spab(mc[4],mc[5],mc[3]);
abb[19] = spab(mc[4],mc[6],mc[1]);
abb[20] = spab(mc[4],mc[6],mc[2]);
abb[21] = spab(mc[4],mc[6],mc[3]);
abb[22] = spab(mc[6],mc[2],mc[3]);
abb[23] = spab(mc[6],mc[4],mc[3]);
abb[24] = spab(mc[6],mc[5],mc[1]);
abb[25] = spab(mc[6],mc[5],mc[2]);
abb[26] = spab(mc[6],mc[5],mc[3]);
abb[27] = spbb(mc[1],mc[2]);
abb[28] = spbb(mc[1],mc[3]);
abb[29] = spbb(mc[1],mc[5]);
abb[30] = spbb(mc[2],mc[3]);
abb[31] = spbb(mc[2],mc[5]);
abb[32] = spbb(mc[3],mc[1]);
abb[33] = spbb(mc[3],mc[2]);
abb[34] = spbb(mc[3],mc[5]);
abb[35] = spbb(mc[4],mc[3]);

{
T z[16];
z[0] = (T(1) / T(2)) / (abb[0] * abb[8] * abb[28] * (-abb[2] * abb[9] * abb[28] * (abb[6] * (abb[10] + abb[11]) * abb[28] + -1 * abb[1] * abb[5] * abb[29]) + abb[1] * (abb[22] + abb[23]) * abb[27] * abb[29] * prod_pow(abb[5], 2)) * prod_pow(abb[27] * abb[30], 2));
z[1] = abb[26] * abb[34];
z[2] = formFactor[1] * z[1];
z[3] = prod_pow(abb[27], 3);
z[4] = z[2] * z[3];
z[5] = abb[27] * formFactor[1];
z[6] = prod_pow(abb[30], 2);
z[7] = abb[24] * abb[29] * z[6];
z[8] = z[5] * z[7];
z[4] = z[4] + z[8];
z[4] = abb[2] * z[4];
z[8] = prod_pow(abb[28], 2);
z[9] = abb[25] * abb[31] * z[8];
z[10] = abb[2] * z[9];
z[5] = z[5] * z[10];
z[4] = z[4] + -z[5];
z[5] = abb[3] * z[4];
z[11] = prod_pow(abb[27], 2);
z[12] = z[2] * z[11];
z[13] = formFactor[1] * z[7];
z[12] = z[12] + z[13];
z[12] = abb[2] * z[12];
z[13] = formFactor[1] * z[10];
z[12] = z[12] + -z[13];
z[12] = abb[4] * abb[33] * z[12];
z[5] = -z[5] + z[12];
z[12] = abb[17] + abb[20];
z[5] = -prod_pow(abb[28], 3) * z[5] * z[12];
z[13] = z[1] * z[11];
z[13] = -z[7] + z[13];
z[14] = abb[4] * abb[32] * formFactor[0];
z[13] = abb[2] * z[13] * z[14];
z[14] = z[10] * z[14];
z[13] = z[13] + z[14];
z[6] = abb[28] * z[6] * z[13];
z[13] = -abb[19] * z[6];
z[6] = -abb[16] * z[6];
z[5] = z[5] + z[6] + z[13];
z[5] = abb[8] * z[5];
z[6] = formFactor[2] * z[7];
z[13] = z[6] * z[11];
z[1] = formFactor[2] * z[1];
z[14] = prod_pow(abb[27], 4);
z[15] = z[1] * z[14];
z[13] = z[13] + -z[15];
z[13] = abb[2] * z[13];
z[10] = formFactor[2] * z[10] * z[11];
z[10] = z[10] + z[13];
z[13] = abb[28] * z[10];
z[15] = formFactor[2] * z[9];
z[6] = z[6] + z[15];
z[3] = z[3] * z[6];
z[1] = prod_pow(abb[27], 5) * z[1];
z[1] = -z[1] + z[3];
z[3] = abb[7] * abb[35] * z[1];
z[3] = z[3] + z[13];
z[6] = abb[1] * abb[7];
z[3] = z[3] * z[6];
z[10] = abb[3] * abb[8] * z[8] * z[10];
z[3] = z[3] + z[10];
z[10] = abb[18] + abb[21];
z[3] = z[3] * z[10];
z[4] = z[4] * z[8] * z[12];
z[7] = z[7] + -z[9];
z[7] = formFactor[1] * z[7] * z[11];
z[2] = z[2] * z[14];
z[2] = z[2] + z[7];
z[7] = abb[12] + abb[14];
z[7] = abb[30] * z[7];
z[8] = abb[35] * z[12];
z[7] = z[7] + z[8];
z[2] = abb[28] * z[2] * z[7];
z[7] = abb[13] + abb[15];
z[1] = abb[30] * z[1] * z[7];
z[1] = z[1] + z[2];
z[1] = abb[7] * z[1];
z[1] = z[1] + z[4];
z[1] = z[1] * z[6];
z[1] = z[1] + z[3] + z[5];
return abb[6] * z[0] * z[1];
}

}


template <typename T> std::complex<T> gluonspm_tree(const mom_conf<T>& mc){
auto result = ((-(mc.s(2,3,4) * spaa(mc[3],mc[1]) * spaa(mc[4],mc[6]) * (spab(mc[3],mc[1],mc[5]) + spab(mc[3],mc[2],mc[5])) * spbb(mc[1],mc[2]) * spbb(mc[3],mc[4])) + mc.s(1,2,3) * (spab(mc[6],mc[3],mc[2]) + spab(mc[6],mc[4],mc[2])) * (mc.s(2,3,4) * (spab(mc[3],mc[1],mc[5]) + spab(mc[3],mc[2],mc[5])) + spaa(mc[1],mc[2]) * spaa(mc[3],mc[4]) * spbb(mc[1],mc[5]) * spbb(mc[4],mc[2]))) * T(2)) / (mc.s(2,3) * mc.s(6,5) * mc.s(1,2,3) * mc.s(2,3,4) * spaa(mc[1],mc[2]) * spbb(mc[3],mc[4]));
return std::complex{-result.imag(), result.real()};
}


template <typename T> T gluonspm_remainder_from_formfactors(const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {
std::array<T, 39> abb;
abb[0] = mc.s(5,6);
abb[1] = mc.s(1,2,3);
abb[2] = mc.s(2,3,4);
abb[3] = spaa(mc[1],mc[2]);
abb[4] = spaa(mc[1],mc[3]);
abb[5] = spaa(mc[2],mc[3]);
abb[6] = spaa(mc[3],mc[1]);
abb[7] = spaa(mc[3],mc[4]);
abb[8] = spaa(mc[4],mc[3]);
abb[9] = spaa(mc[4],mc[6]);
abb[10] = spab(mc[3],mc[1],mc[5]);
abb[11] = spab(mc[3],mc[2],mc[5]);
abb[12] = spab(mc[3],mc[5],mc[1]);
abb[13] = spab(mc[3],mc[5],mc[2]);
abb[14] = spab(mc[3],mc[5],mc[3]);
abb[15] = spab(mc[3],mc[6],mc[1]);
abb[16] = spab(mc[3],mc[6],mc[2]);
abb[17] = spab(mc[3],mc[6],mc[3]);
abb[18] = spab(mc[4],mc[5],mc[1]);
abb[19] = spab(mc[4],mc[5],mc[2]);
abb[20] = spab(mc[4],mc[5],mc[3]);
abb[21] = spab(mc[4],mc[6],mc[1]);
abb[22] = spab(mc[4],mc[6],mc[2]);
abb[23] = spab(mc[4],mc[6],mc[3]);
abb[24] = spab(mc[6],mc[3],mc[2]);
abb[25] = spab(mc[6],mc[4],mc[2]);
abb[26] = spab(mc[6],mc[5],mc[1]);
abb[27] = spab(mc[6],mc[5],mc[2]);
abb[28] = spab(mc[6],mc[5],mc[3]);
abb[29] = spbb(mc[1],mc[2]);
abb[30] = spbb(mc[1],mc[3]);
abb[31] = spbb(mc[1],mc[5]);
abb[32] = spbb(mc[2],mc[1]);
abb[33] = spbb(mc[2],mc[3]);
abb[34] = spbb(mc[2],mc[5]);
abb[35] = spbb(mc[3],mc[4]);
abb[36] = spbb(mc[3],mc[5]);
abb[37] = spbb(mc[4],mc[2]);
abb[38] = spbb(mc[4],mc[3]);

{
T z[19];
z[0] = 1 / (abb[0] * (abb[2] * abb[6] * abb[9] * (abb[10] + abb[11]) * abb[29] * abb[35] + -abb[1] * (abb[24] + abb[25]) * (abb[2] * (abb[10] + abb[11]) + abb[3] * abb[7] * abb[31] * abb[37])) * abb[38] * prod_pow(abb[29] * abb[30] * abb[33], 2));
z[1] = prod_pow(abb[33], 2);
z[2] = abb[26] * abb[31] * z[1];
z[3] = prod_pow(abb[30], 2);
z[4] = formFactor[1] * z[3];
z[5] = z[2] * z[4];
z[6] = abb[27] * abb[34];
z[7] = prod_pow(abb[30], 4) * formFactor[1] * z[6];
z[5] = z[5] + -z[7];
z[7] = prod_pow(abb[29], 2);
z[4] = z[4] * z[7];
z[8] = abb[28] * abb[36];
z[9] = z[4] * z[8];
z[9] = -z[5] + -z[9];
z[10] = abb[19] + abb[22];
z[9] = z[9] * z[10];
z[10] = abb[29] * z[9];
z[11] = abb[20] + abb[23];
z[12] = abb[28] * z[7] * z[11];
z[13] = abb[36] * z[12];
z[14] = z[2] * z[11];
z[13] = -z[13] + z[14];
z[13] = abb[30] * z[13];
z[11] = z[6] * z[11];
z[15] = prod_pow(abb[30], 3);
z[16] = z[11] * z[15];
z[13] = z[13] + z[16];
z[16] = formFactor[2] * z[7];
z[17] = z[13] * z[16];
z[10] = z[10] + -z[17];
z[10] = abb[37] * z[10];
z[5] = abb[33] * z[5];
z[17] = abb[33] * abb[36];
z[4] = abb[28] * z[4] * z[17];
z[4] = z[4] + z[5];
z[5] = abb[13] + abb[16];
z[4] = abb[29] * z[4] * z[5];
z[5] = z[7] * z[8];
z[8] = abb[30] * z[5];
z[15] = z[6] * z[15];
z[18] = abb[30] * z[2];
z[8] = -z[8] + z[15] + z[18];
z[15] = abb[14] + abb[17];
z[8] = abb[33] * z[8] * z[15] * z[16];
z[4] = z[4] + z[8] + z[10];
z[4] = abb[1] * z[4];
z[7] = -z[7] * z[9];
z[8] = prod_pow(abb[29], 3) * formFactor[2];
z[9] = z[8] * z[13];
z[7] = z[7] + z[9];
z[9] = abb[4] * abb[38];
z[7] = z[7] * z[9];
z[10] = z[3] * z[11];
z[10] = z[10] + z[14];
z[10] = abb[33] * z[10];
z[11] = z[12] * z[17];
z[10] = z[10] + -z[11];
z[8] = abb[5] * abb[38] * z[8] * z[10];
z[7] = -z[4] + z[7] + z[8];
z[7] = abb[4] * z[7];
z[3] = z[3] * z[6];
z[6] = z[2] + z[3];
z[6] = z[1] * z[6];
z[8] = z[1] * z[5];
z[6] = z[6] + -z[8];
z[6] = -z[6] * z[15];
z[10] = abb[37] * z[10];
z[6] = z[6] + z[10];
z[6] = abb[1] * abb[5] * z[6] * z[16];
z[2] = -z[2] + z[3];
z[3] = z[2] + z[5];
z[5] = -abb[12] + -abb[15];
z[3] = prod_pow(abb[33], 3) * z[3] * z[5];
z[1] = z[1] * z[2];
z[1] = z[1] + z[8];
z[2] = abb[18] + abb[21];
z[1] = z[1] * z[2];
z[2] = abb[37] * z[1];
z[2] = z[2] + z[3];
z[2] = abb[1] * z[2];
z[1] = abb[29] * z[1] * z[9];
z[1] = z[1] + z[2];
z[1] = abb[5] * abb[32] * formFactor[0] * z[1];
z[1] = z[1] + z[6] + z[7];
z[1] = abb[2] * z[1];
z[2] = -abb[3] * abb[8] * abb[37] * z[4];
z[1] = z[1] + z[2];
return abb[35] * (T(1) / T(2)) * z[0] * z[1];
}

}


template <typename T> std::complex<T> quarksmp_tree(const mom_conf<T>& mc){
auto result = ((mc.s(2,3,4) * spaa(mc[6],mc[4]) * (spab(mc[2],mc[1],mc[5]) + spab(mc[2],mc[3],mc[5])) * spbb(mc[1],mc[3]) + mc.s(1,2,3) * spaa(mc[2],mc[4]) * (spab(mc[6],mc[2],mc[3]) + spab(mc[6],mc[4],mc[3])) * spbb(mc[5],mc[1])) * T(2)) / (mc.s(2,3) * mc.s(6,5) * mc.s(1,2,3) * mc.s(2,3,4));
return std::complex{-result.imag(), result.real()};
}


template <typename T> T quarksmp_remainder_from_formfactors(const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {
std::array<T, 35> abb;
abb[0] = mc.s(5,6);
abb[1] = mc.s(1,2,3);
abb[2] = mc.s(2,3,4);
abb[3] = spaa(mc[1],mc[2]);
abb[4] = spaa(mc[2],mc[3]);
abb[5] = spaa(mc[2],mc[4]);
abb[6] = spaa(mc[4],mc[2]);
abb[7] = spaa(mc[6],mc[4]);
abb[8] = spab(mc[2],mc[1],mc[5]);
abb[9] = spab(mc[2],mc[3],mc[5]);
abb[10] = spab(mc[2],mc[5],mc[2]);
abb[11] = spab(mc[2],mc[5],mc[3]);
abb[12] = spab(mc[2],mc[6],mc[2]);
abb[13] = spab(mc[2],mc[6],mc[3]);
abb[14] = spab(mc[4],mc[5],mc[1]);
abb[15] = spab(mc[4],mc[5],mc[2]);
abb[16] = spab(mc[4],mc[5],mc[3]);
abb[17] = spab(mc[4],mc[6],mc[1]);
abb[18] = spab(mc[4],mc[6],mc[2]);
abb[19] = spab(mc[4],mc[6],mc[3]);
abb[20] = spab(mc[6],mc[2],mc[3]);
abb[21] = spab(mc[6],mc[4],mc[3]);
abb[22] = spab(mc[6],mc[5],mc[1]);
abb[23] = spab(mc[6],mc[5],mc[2]);
abb[24] = spab(mc[6],mc[5],mc[3]);
abb[25] = spbb(mc[1],mc[2]);
abb[26] = spbb(mc[1],mc[3]);
abb[27] = spbb(mc[1],mc[5]);
abb[28] = spbb(mc[2],mc[3]);
abb[29] = spbb(mc[2],mc[5]);
abb[30] = spbb(mc[3],mc[1]);
abb[31] = spbb(mc[3],mc[2]);
abb[32] = spbb(mc[3],mc[5]);
abb[33] = spbb(mc[4],mc[3]);
abb[34] = spbb(mc[5],mc[1]);

{
T z[18];
z[0] = 1 / (abb[0] * abb[26] * (abb[2] * abb[7] * (abb[8] + abb[9]) * abb[26] + abb[1] * abb[5] * (abb[20] + abb[21]) * abb[34]) * prod_pow(abb[25] * abb[28], 2));
z[1] = abb[24] * abb[32];
z[2] = z[0] * z[1];
z[3] = prod_pow(abb[25], 2);
z[4] = z[2] * z[3];
z[5] = prod_pow(abb[26], 2);
z[6] = abb[23] * abb[29] * z[5];
z[7] = z[0] * z[6];
z[8] = z[4] + -z[7];
z[8] = z[5] * z[8];
z[9] = prod_pow(abb[28], 2);
z[10] = abb[22] * abb[27] * z[9];
z[11] = z[0] * z[10];
z[12] = z[5] * z[11];
z[8] = z[8] + z[12];
z[8] = abb[4] * abb[31] * z[8];
z[13] = abb[25] * z[6];
z[14] = abb[3] * z[0];
z[15] = z[13] * z[14];
z[2] = abb[3] * z[2];
z[16] = prod_pow(abb[25], 3);
z[17] = z[2] * z[16];
z[15] = z[15] + -z[17];
z[5] = z[5] * z[15];
z[12] = abb[3] * abb[25] * z[12];
z[5] = z[5] + z[8] + -z[12];
z[8] = abb[15] + abb[18];
z[5] = -formFactor[1] * z[5] * z[8];
z[4] = -z[4] + -z[7] + z[11];
z[7] = abb[14] + abb[17];
z[4] = abb[4] * abb[30] * formFactor[0] * z[4] * z[7] * z[9];
z[6] = z[6] + z[10];
z[7] = z[3] * z[6] * z[14];
z[9] = prod_pow(abb[25], 4);
z[2] = z[2] * z[9];
z[2] = -z[2] + z[7];
z[7] = abb[16] + abb[19];
z[2] = abb[26] * formFactor[2] * z[2] * z[7];
z[2] = z[2] + z[4] + z[5];
z[2] = abb[2] * z[2];
z[4] = abb[1] * z[0];
z[5] = abb[25] * z[4] * z[10];
z[10] = z[4] * z[13];
z[1] = z[1] * z[4];
z[11] = z[1] * z[16];
z[5] = -z[5] + z[10] + -z[11];
z[10] = abb[10] + abb[12];
z[10] = abb[28] * z[10];
z[8] = abb[33] * z[8];
z[8] = z[8] + z[10];
z[5] = abb[26] * formFactor[1] * z[5] * z[8];
z[3] = z[3] * z[4] * z[6];
z[1] = z[1] * z[9];
z[1] = -z[1] + z[3];
z[3] = -abb[11] + -abb[13];
z[3] = abb[28] * z[3];
z[4] = -abb[33] * z[7];
z[3] = z[3] + z[4];
z[1] = formFactor[2] * z[1] * z[3];
z[1] = z[1] + z[5];
z[1] = abb[6] * z[1];
z[1] = z[1] + z[2];
return (T(1) / T(2)) * z[1];
}

}


template <typename T> std::complex<T> quarkspm_tree(const mom_conf<T>& mc){
auto result = ((mc.s(2,3,4) * spaa(mc[6],mc[4]) * (spab(mc[3],mc[1],mc[5]) + spab(mc[3],mc[2],mc[5])) * spbb(mc[1],mc[2]) + mc.s(1,2,3) * spaa(mc[3],mc[4]) * (spab(mc[6],mc[3],mc[2]) + spab(mc[6],mc[4],mc[2])) * spbb(mc[5],mc[1])) * T(2)) / (mc.s(2,3) * mc.s(6,5) * mc.s(1,2,3) * mc.s(2,3,4));
return std::complex{-result.imag(), result.real()};
}


template <typename T> T quarkspm_remainder_from_formfactors(const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {
std::array<T, 34> abb;
abb[0] = mc.s(5,6);
abb[1] = mc.s(1,2,3);
abb[2] = mc.s(2,3,4);
abb[3] = spaa(mc[1],mc[3]);
abb[4] = spaa(mc[2],mc[3]);
abb[5] = spaa(mc[3],mc[4]);
abb[6] = spaa(mc[4],mc[3]);
abb[7] = spaa(mc[6],mc[4]);
abb[8] = spab(mc[3],mc[1],mc[5]);
abb[9] = spab(mc[3],mc[2],mc[5]);
abb[10] = spab(mc[3],mc[5],mc[2]);
abb[11] = spab(mc[3],mc[5],mc[3]);
abb[12] = spab(mc[3],mc[6],mc[2]);
abb[13] = spab(mc[3],mc[6],mc[3]);
abb[14] = spab(mc[4],mc[5],mc[1]);
abb[15] = spab(mc[4],mc[5],mc[2]);
abb[16] = spab(mc[4],mc[5],mc[3]);
abb[17] = spab(mc[4],mc[6],mc[1]);
abb[18] = spab(mc[4],mc[6],mc[2]);
abb[19] = spab(mc[4],mc[6],mc[3]);
abb[20] = spab(mc[6],mc[3],mc[2]);
abb[21] = spab(mc[6],mc[4],mc[2]);
abb[22] = spab(mc[6],mc[5],mc[1]);
abb[23] = spab(mc[6],mc[5],mc[2]);
abb[24] = spab(mc[6],mc[5],mc[3]);
abb[25] = spbb(mc[1],mc[2]);
abb[26] = spbb(mc[1],mc[3]);
abb[27] = spbb(mc[1],mc[5]);
abb[28] = spbb(mc[2],mc[1]);
abb[29] = spbb(mc[2],mc[3]);
abb[30] = spbb(mc[2],mc[5]);
abb[31] = spbb(mc[3],mc[5]);
abb[32] = spbb(mc[4],mc[2]);
abb[33] = spbb(mc[5],mc[1]);

{
T z[16];
z[0] = (T(1) / T(2)) / (abb[0] * abb[25] * (abb[2] * abb[7] * (abb[8] + abb[9]) * abb[25] + abb[1] * abb[5] * (abb[20] + abb[21]) * abb[33]) * prod_pow(abb[26] * abb[29], 2));
z[1] = abb[15] + abb[18];
z[2] = abb[3] * z[1];
z[3] = prod_pow(abb[26], 4) * formFactor[1];
z[4] = -z[2] * z[3];
z[5] = abb[4] * abb[29];
z[6] = abb[3] * abb[26];
z[5] = z[5] + z[6];
z[7] = abb[16] + abb[19];
z[7] = formFactor[2] * z[7];
z[8] = abb[25] * z[7];
z[5] = z[5] * z[8];
z[9] = prod_pow(abb[26], 2);
z[10] = z[5] * z[9];
z[4] = z[4] + z[10];
z[4] = abb[25] * z[4];
z[10] = abb[14] + abb[17];
z[10] = abb[4] * abb[28] * formFactor[0] * z[10];
z[11] = prod_pow(abb[29], 2);
z[12] = z[10] * z[11];
z[13] = z[9] * z[12];
z[4] = z[4] + z[13];
z[13] = abb[23] * abb[30];
z[4] = z[4] * z[13];
z[9] = formFactor[1] * z[9];
z[2] = z[2] * z[9];
z[14] = z[2] * z[11];
z[6] = z[6] * z[11];
z[15] = abb[4] * prod_pow(abb[29], 3);
z[6] = z[6] + z[15];
z[6] = z[6] * z[8];
z[6] = z[6] + z[14];
z[6] = abb[25] * z[6];
z[8] = -prod_pow(abb[29], 4) * z[10];
z[6] = z[6] + z[8];
z[8] = abb[22] * abb[27];
z[6] = z[6] * z[8];
z[2] = z[2] + -z[5];
z[2] = abb[25] * z[2];
z[2] = z[2] + z[12];
z[5] = abb[31] * prod_pow(abb[25], 2);
z[2] = abb[24] * z[2] * z[5];
z[2] = z[2] + z[4] + z[6];
z[2] = abb[2] * z[2];
z[4] = abb[10] + abb[12];
z[4] = abb[29] * z[4];
z[1] = abb[32] * z[1];
z[1] = -z[1] + z[4];
z[3] = -z[1] * z[3];
z[4] = abb[11] + abb[13];
z[4] = abb[29] * formFactor[2] * z[4];
z[6] = abb[32] * z[7];
z[4] = z[4] + -z[6];
z[4] = abb[25] * z[4];
z[6] = prod_pow(abb[26], 3) * z[4];
z[3] = z[3] + z[6];
z[3] = z[3] * z[13];
z[4] = abb[26] * z[4];
z[1] = z[1] * z[9];
z[6] = z[1] + z[4];
z[6] = z[6] * z[8] * z[11];
z[1] = z[1] + -z[4];
z[1] = abb[24] * z[1] * z[5];
z[1] = z[1] + z[3] + z[6];
z[1] = abb[1] * abb[6] * z[1];
z[1] = z[1] + z[2];
return z[0] * z[1];
}

}


template <typename T> auto remainder_from_formfactors(std::string name, const mom_conf<remove_complex_t<T>>& mc, const std::vector<T>& formFactor) {

    using namespace matchit;
    using s = std::string;

    // this removes the index since we have same for all Nf powers
    name = name.erase(name.size()-1);

    auto f = matchit::match(name) (
            pattern | or_(s("Vjj_1l_gluonspp"),s("Vjj_2l_gluonspp")) = gluonspp_remainder_from_formfactors<T>,
            pattern | or_(s("Vjj_1l_gluonspm"),s("Vjj_2l_gluonspm")) = gluonspm_remainder_from_formfactors<T>,
            pattern | or_(s("Vjj_1l_gluonsmp"),s("Vjj_2l_gluonsmp")) = gluonsmp_remainder_from_formfactors<T>,

            pattern | or_(s("Vjj_1l_quarkspm"),s("Vjj_2l_quarkspm")) = quarkspm_remainder_from_formfactors<T>,
            pattern | or_(s("Vjj_1l_quarksmp"),s("Vjj_2l_quarksmp")) = quarksmp_remainder_from_formfactors<T>,

            pattern | _ = nullptr
    );

    if(!f) {
        throw std::runtime_error("Remainder " + name + " not recognized");
    }

    return f(mc, formFactor);
}

template <typename TC> auto get_tree(std::string name, const mom_conf<TC>& mc) {

    using namespace matchit;
    using s = std::string;

    using T = remove_complex_t<TC>;

    // this removes the index since we have same for all Nf powers
    name = name.erase(name.size()-1);

    auto f = matchit::match(name) (
            pattern | or_(s("Vjj_1l_gluonspp"),s("Vjj_2l_gluonspp")) = gluonspp_tree<T>,
            pattern | or_(s("Vjj_1l_gluonspm"),s("Vjj_2l_gluonspm")) = gluonspm_tree<T>,
            pattern | or_(s("Vjj_1l_gluonsmp"),s("Vjj_2l_gluonsmp")) = gluonsmp_tree<T>,
                                                                        
            pattern | or_(s("Vjj_1l_quarkspm"),s("Vjj_2l_quarkspm")) = quarkspm_tree<T>,
            pattern | or_(s("Vjj_1l_quarksmp"),s("Vjj_2l_quarksmp")) = quarksmp_tree<T>,

            pattern | _ = nullptr
    );

    if(!f) {
        throw std::runtime_error("Remainder " + name + " not recognized");
    }

    return f(mc);
}

template <typename T, typename Tcoeffs> struct Vjj_remainder {

    std::string name;
    Permutation perm;
    std::shared_ptr<VM_Cache<Tcoeffs>> c_cache;
    std::shared_ptr<FunctionsManagerG<KinType::m1>> f_cache;

    std::vector<RemainderEvaluatorVM2_sij<KinType::m1, T, Tcoeffs>> remainders{};  

    bool large_cancellations_flag{false};

    void construct() {
        if (perm.size() != 6) throw std::runtime_error("Incorrect permutation!");

        // bring the lepton momenta into the unique order for form factors (they do not depend on this order)
        Permutation perm_ff;
        if (perm[4] == 5 and perm[5] == 6) { perm_ff = perm; }
        else if (perm[4] == 6 and perm[5] == 5) {
            perm_ff = perm;
            std::swap(perm_ff[4], perm_ff[5]);
        }
        else {
            throw std::runtime_error("Leptons are expected in positions 5,6 and must not be permuted!");
        }

        for (int i = 1; i <=3 ; ++i) {
            remainders.emplace_back(
                    name,
                    std::vector<int>{i},
                    perm_ff,
                    Vjj::get_sij_variables<Tcoeffs>,
                    Vjj::get_inverse_denominators<Tcoeffs>,
                    f_cache,
                    c_cache
            );
        }
    }

    auto operator()(const mom_conf<T>& mc, const T& musq) {
        std::vector<add_complex_t<T>> formFactor;

        large_cancellations_flag = false;

        for(auto& ri : remainders) {
            formFactor.push_back(ri(mc, musq));
            large_cancellations_flag = large_cancellations_flag || ri.large_cancellations_flag;
        }
        
        return Vjj::remainder_from_formfactors(name,mc.permute(perm), formFactor);
    }

};



template <typename Tres, typename Tcontrib> auto  add_squared_contribution (Tres& result, const Tcontrib& contribution, bool include_one_loop_squared = false) {
    using std::conj;
    auto sq = [](auto x) {
        return (conj(x) * x).real();
    };

    result[0][0] += sq(contribution[0][0]);

    result[1][0] += 2 * real( conj(contribution[0][0]) * contribution[1][0] );
    result[1][1] += 2 * real( conj(contribution[0][0]) * contribution[1][1] );

    result[2][0] += 2 * real( conj(contribution[0][0]) * contribution[2][0] );
    result[2][1] += 2 * real( conj(contribution[0][0]) * contribution[2][1] );
    result[2][2] += 2 * real( conj(contribution[0][0]) * contribution[2][2] );

    /* 1-loop squared */
    if(include_one_loop_squared) {
        result[2][0] += sq(contribution[1][0]);
        result[2][1] += 2 * real(conj(contribution[1][0]) * contribution[1][1] );
        result[2][2] += sq(contribution[1][1]);
    }
}



template <typename T> std::vector<T> operator+(std::vector<T> v1, const std::vector<T>& v2) {
    //if(v1.size() != v2.size()) throw std::runtime_error("Will not sum two vectors of different size.");

    std::vector<T> new_v(std::max(v1.size(),v2.size()));

    for (size_t i = 0; i < new_v.size(); ++i) {
        if(i < v1.size()) {
            new_v.at(i) = new_v[i] + v1.at(i);
        }
        if(i < v2.size()) {
            new_v.at(i) = new_v[i] + v2.at(i);
        }
    }

    return new_v;
}

template <typename T> T finite_renormalize(const T& remainder, const T& renormalization, size_t) { return remainder*renormalization;}

// This essential implements a product of two series, such that its coefficients can also be series
// What is assumed is that it's a dense series in a sense that it's leading order is 0, and all coefficients below `truncation` are provided.
template <typename T> std::vector<T> finite_renormalize(const std::vector<T>& remainder, const std::vector<T>& renormalization, size_t truncation) {

    std::vector<T> new_remainder(truncation+1);

    for (size_t i = 0; i < std::min(truncation+1, remainder.size()) ; i++) {
        for (size_t j = 0; j < std::min(truncation+1, renormalization.size()); j++) {
            if (i + j > truncation) continue;
            new_remainder[i + j] = new_remainder[i+j] +  finite_renormalize(remainder.at(i),renormalization.at(j), truncation);
        }
    }

    return new_remainder;
}



} // namespace Vjj

} // FivePointAmplitudes
