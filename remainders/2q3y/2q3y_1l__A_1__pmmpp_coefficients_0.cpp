#include "FivePointAmplitudes_config.h"
#include "prod_pow.h"
#include "Utilities.h"
#include <array>
#ifdef FivePointAmplitudes_QD_ENABLED
#include <qd/qd_real.h>
#endif

#include <Kin.h>

namespace FivePointAmplitudes {


template <typename T> std::array<T,12> eval_coef_2q3y_1l__A_1__pmmpp_0(const std::array<T,40>& abb, const std::array<T,34>& q)
{
T z[19];
z[0] = prod_pow(abb[4], 2) * q[9];
z[1] = -q[3] * q[5] * z[0];
z[0] = q[2] * q[6] * z[0];
z[2] = prod_pow(abb[0], 2);
z[3] = q[9] * z[2];
z[4] = q[5] * z[3];
z[5] = -prod_pow(abb[8], 2) * prod_pow(q[3], 3) * z[4];
z[3] = q[6] * z[3];
z[6] = prod_pow(abb[7], 2) * prod_pow(q[2], 3) * z[3];
z[7] = abb[7] * q[2];
z[8] = prod_pow(z[7], 2);
z[4] = q[3] * z[4] * z[8];
z[9] = abb[8] * q[3];
z[10] = prod_pow(z[9], 2);
z[3] = -q[2] * z[3] * z[10];
z[10] = abb[0] * z[10];
z[11] = prod_pow(q[3], 2);
z[12] = abb[1] * abb[8] * z[11];
z[13] = abb[6] * z[12];
z[13] = 3 * z[10] + -2 * z[13];
z[13] = prod_pow(q[9], 2) * z[13];
z[14] = prod_pow(q[2], 2) * q[3] * q[12];
z[15] = -abb[1] * abb[7] * abb[33] * q[6] * z[2] * z[14];
z[16] = q[2] * q[11];
z[2] = -abb[36] * q[5] * z[2] * z[12] * z[16];
z[12] = abb[0] * prod_pow(abb[1], 2);
z[11] = -abb[33] * z[11] * z[12] * z[16];
z[16] = abb[4] * (T(1) / T(2));
z[17] = q[22] * z[9];
z[18] = z[16] * z[17];
z[10] = -q[22] * z[10];
z[9] = abb[1] * abb[6] * abb[36] * prod_pow(q[22], 2) * z[9];
z[9] = (T(-1) / T(2)) * z[9] + z[10] + z[18];
z[10] = abb[39] * q[9];
z[9] = z[9] * z[10];
z[9] = z[9] + z[11];
z[11] = -abb[36] * z[12] * z[14];
z[12] = abb[1] * (T(1) / T(2));
z[14] = abb[5] * abb[33];
z[12] = -prod_pow(q[19], 2) * z[12] * z[14];
z[16] = q[19] * z[16];
z[12] = z[12] + z[16];
z[12] = z[7] * z[12];
z[8] = -abb[0] * q[19] * z[8];
z[8] = z[8] + z[12];
z[8] = z[8] * z[10];
z[8] = z[8] + z[11];
z[7] = q[19] * q[25] * z[7] * z[14];
z[10] = -abb[26] + -abb[29];
z[10] = q[26] * z[10] * z[17];
z[7] = z[7] + z[10];
z[7] = q[9] * z[7];
z[10] = -abb[33] * q[22];
z[10] = q[2] + z[10];
z[10] = abb[36] * q[25] * q[26] * z[10];
z[7] = z[7] + z[10];
z[7] = abb[39] * (T(1) / T(2)) * z[7];
return {z[1], z[0], z[5], z[6], z[4], z[3], z[13], z[15], z[2], z[9], z[8], z[7]};
}



template std::array<complex<double>,12> eval_coef_2q3y_1l__A_1__pmmpp_0(const std::array<complex<double>,40>& abb, const std::array<complex<double>,34>& q);
#ifdef FivePointAmplitudes_QD_ENABLED 
template std::array<complex<dd_real>,12> eval_coef_2q3y_1l__A_1__pmmpp_0(const std::array<complex<dd_real>,40>& abb, const std::array<complex<dd_real>,34>& q);
#endif

}
