#pragma once 

#include "Kinematics.h"
#include "Spinor.h"

namespace  FivePointAmplitudes {

/**
 * An auxiliary function which evaluates the square of helicity weight (normalization) of
 * an amplitude with 2 fermions and 3 massless vector particles.
 *
 * @param five-point kinematics
 */
template <typename T> T qp_qm_p_p_p_norm_squared(const sij5<T>& s) {

    // this is the square of the spinor weight which was used for reconstruction:
    T result = s(1, 3) / (s(1, 2) * s(1, 4) * s(1, 5) * s(2, 3));
    result *= result;
    result /= s(1, 2);

    // and here is the (square of) the factor which makes the helicity remainders with vanishing trees dimensionless:
    using std::pow;
    result *= pow(s(1,2),6);

    // so overall this factor has units  = -2, as the tree amplitude squared

    return result;
}

/**
 * The tree matrix element squared, not helicity summed.
 */
template <typename T> T qp_qm_m_p_p_tree_squared(const sij5<T>& s) {
    // TODO fix the factor, now it's adjusted to give the result of Caravel exactly
    T factor = 64;

    T me2 = s(1, 2) * s(2, 3) * s(2, 3) / (s(1, 4) * s(1, 5) * s(2, 4) * s(2, 5));

    me2 *= factor;

    return me2;
}

template <typename T> add_complex_t<T> qp_qm_p_p_p_norm(const mom_conf<T>& mc) {
    return (prod_pow(spaa(mc[1], mc[2]), 3) * spaa(mc[1], mc[3]) * spbb(mc[3], mc[1])) /
           (prod_pow(spaa(mc[1], mc[4]), 2) * prod_pow(spaa(mc[1], mc[5]), 2) * prod_pow(spaa(mc[2], mc[3]), 2));
}

template <typename T> add_complex_t<T> qp_qm_m_p_p_tree(const mom_conf<T>& mc) {
    return - (spaa(mc[1], mc[2]) * prod_pow(spaa(mc[2], mc[3]), 2)) / (spaa(mc[1], mc[4]) * spaa(mc[1], mc[5]) * spaa(mc[2], mc[4]) * spaa(mc[2], mc[5]));
}

    
} //  FivePointAmplitudes

