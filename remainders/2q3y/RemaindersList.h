#pragma once

#include "RemainderEvaluator.h"
#include "SpinorRemainderBase.h"
#include "MathList.h"

#include "remainders/2q3y/normalizations.hpp"

namespace FivePointAmplitudes {

#include "remainders/2q3y/2q3y_1l__A_1__pmmpp.hpp"
#include "remainders/2q3y/2q3y_1l__A_1__pmppp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_0__pmmpp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_0__pmppp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_1__pmmpp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_1__pmppp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_nf__pmmpp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_nf__pmppp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_nfqq__pmmpp.hpp"
#include "remainders/2q3y/2q3y_2l__A_2_nfqq__pmppp.hpp"

}