#include "FivePointAmplitudes_config.h"
#include "prod_pow.h"
#include "Utilities.h"
#include <array>
#ifdef FivePointAmplitudes_QD_ENABLED
#include <qd/qd_real.h>
#endif

#include <Kin.h>

namespace FivePointAmplitudes {


template <typename T> std::array<T,1> eval_coef_2q3y_1l__A_1__pmppp_0(const std::array<T,40>& abb, const std::array<T,34>& q)
{
T z[6];
z[0] = abb[4] * abb[31];
z[1] = q[2] * q[7];
z[2] = -q[6] * z[0] * z[1];
z[3] = -abb[22] + abb[23];
z[4] = -abb[18] + abb[19];
z[5] = -z[3] + -2 * z[4];
z[1] = z[1] * z[5];
z[0] = -q[5] * q[7] * z[0];
z[0] = z[0] + z[1];
z[0] = q[3] * z[0];
z[0] = z[0] + z[2];
z[0] = q[8] * z[0];
z[1] = 2 * z[3] + z[4];
z[1] = q[1] * z[1];
z[2] = abb[5] * abb[33];
z[3] = q[4] * z[2];
z[1] = z[1] + z[3];
z[1] = q[3] * z[1];
z[2] = q[1] * q[6] * z[2];
z[1] = z[1] + z[2];
z[1] = q[7] * z[1];
z[2] = -q[1] * q[5];
z[3] = -q[2] * q[4];
z[2] = z[2] + z[3];
z[2] = abb[6] * abb[36] * q[8] * z[2];
z[1] = z[1] + z[2];
z[1] = q[9] * z[1];
z[0] = z[0] + z[1];
z[0] = 4 * abb[0] * z[0];
return {z[0]};
}



template std::array<complex<double>,1> eval_coef_2q3y_1l__A_1__pmppp_0(const std::array<complex<double>,40>& abb, const std::array<complex<double>,34>& q);
#ifdef FivePointAmplitudes_QD_ENABLED 
template std::array<complex<dd_real>,1> eval_coef_2q3y_1l__A_1__pmppp_0(const std::array<complex<dd_real>,40>& abb, const std::array<complex<dd_real>,34>& q);
#endif

}
