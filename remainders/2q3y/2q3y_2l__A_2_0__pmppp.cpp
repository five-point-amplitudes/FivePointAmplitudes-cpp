#include "RemainderEvaluatorSpinor.h"
#include "SpinorRemainderBase.h"
#include "MathList.h"

#include "normalizations.hpp"

namespace FivePointAmplitudes {

#include "2q3y_2l__A_2_0__pmppp.hpp"

template <typename T> remainder_2q3y_2l__A_2_0__pmppp<T>::remainder_2q3y_2l__A_2_0__pmppp(const std::vector<int>& set_permutation, std::shared_ptr<FunctionsManager> mng) : permutation(set_permutation), f_manager(mng),
	coeff_basis{
eval_coef_2q3y_2l__A_2_0__pmppp_0<TC>
	}
{
    construct_functions_basis();
    construct_coefficient_list();
}

template <typename T> remainder_2q3y_2l__A_2_0__pmppp<T>::remainder_2q3y_2l__A_2_0__pmppp() : remainder_2q3y_2l__A_2_0__pmppp(std::make_shared<FunctionsManager>()) {}
template <typename T> remainder_2q3y_2l__A_2_0__pmppp<T>::remainder_2q3y_2l__A_2_0__pmppp(const std::vector<int>& set_permutation) : remainder_2q3y_2l__A_2_0__pmppp(set_permutation, std::make_shared<FunctionsManager>()) {}
template <typename T> remainder_2q3y_2l__A_2_0__pmppp<T>::remainder_2q3y_2l__A_2_0__pmppp(std::shared_ptr<FunctionsManager> mng) : remainder_2q3y_2l__A_2_0__pmppp({1,2,3,4,5}, mng) {}

template <typename T> void remainder_2q3y_2l__A_2_0__pmppp<T>::construct_functions_basis() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;

    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR)+"/2q3y_2l__A_2_0__pmppp_"+perm_string.str()+"_fmonomials.dat"));

    size_t n = 0;

    functions_basis.resize(input.size());

    for (MathList mm : input) {
        for (MathList pp : mm) {
            f_type t = from_string(pp.head);
            switch (t) {
                case f_type::one:
                case f_type::str5: functions_basis.at(n).emplace_back(t, 1); break;
                case f_type::bc:
                case f_type::bci: functions_basis.at(n).emplace_back(t, std::stoul(pp[0])); break;
                case f_type::f: {
                    int id = 0;
                    if (pp.size() >= 3) { id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1]), std::stoi(pp[2])}); }
                    else if (pp.size() == 2) {
                        id = f_manager->add({std::stoi(pp[0]), std::stoi(pp[1])});
                    }
                    functions_basis.at(n).emplace_back(t, id);
                }
                // in m0 functions set this is not used
                case f_type::isqrt: break;
            }
        }
        ++n;
    }
}

template <typename T> void remainder_2q3y_2l__A_2_0__pmppp<T>::construct_coefficient_list() {
    std::stringstream perm_string;
    for(auto& ii : permutation) perm_string << ii;

    auto input = MathList(read_file(std::string(FivePointAmplitudes_DATADIR) + "/2q3y_2l__A_2_0__pmppp_"+perm_string.str()+"_mmatrix.dat"));

    coefficients.reserve(input.size());

    for (MathList lc : input) {
        FLC coeff{};

        if (lc.size() != 0) {
            for (MathList prod : lc) { coeff.emplace_back(std::stoi(prod[0]), get_rational_number<T>(prod[1])); }
        }
        coefficients.push_back(coeff);
    }
}

template <typename T> std::complex<T> remainder_2q3y_2l__A_2_0__pmppp<T>::eval(const mom_conf<T>& mc, T musq) {
    return eval_remainder_spinor(mc, -1 , musq, coeff_basis, functions_basis, coefficients, qp_qm_p_p_p_norm<T>, f_manager, permutation);
}


template class remainder_2q3y_2l__A_2_0__pmppp<double>;
#ifdef FivePointAmplitudes_QD_ENABLED 
template class remainder_2q3y_2l__A_2_0__pmppp<FunctionsManager::RHP>;
#endif

}
