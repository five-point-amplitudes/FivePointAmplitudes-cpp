#pragma once
#include "2q3y_1l__A_1__pmppp_coefficients.hpp"

template <typename T> class remainder_2q3y_1l__A_1__pmppp : public SpinorRemainderBase<T> {
	static constexpr size_t NCOEFFS = 1;

	using TC = std::complex<T>;
	using RCoeffsType = std::array<TC,NCOEFFS>(*)( const std::array<TC,40>&, const std::array<TC,34>&);
        using FLC = std::vector<std::pair<int,T>>;
        using TCoeffList = std::vector<FLC>;

        std::vector<int> permutation{1, 2, 3, 4, 5};
	std::shared_ptr<FunctionsManager> f_manager;

	RCoeffsType coeff_basis;
	std::vector<std::vector<std::pair<f_type,int>>> functions_basis;
	// {index of coefficient, index of function, matrix element}
	TCoeffList coefficients;

      public:
	remainder_2q3y_1l__A_1__pmppp();
	remainder_2q3y_1l__A_1__pmppp(const std::vector<int>&);
	remainder_2q3y_1l__A_1__pmppp(std::shared_ptr<FunctionsManager>);
	remainder_2q3y_1l__A_1__pmppp(const std::vector<int>&, std::shared_ptr<FunctionsManager> f_manager);
	
	TC eval(const mom_conf<T>&, T);

  private:
        void construct_functions_basis();
        void construct_coefficient_list();
};

extern template class remainder_2q3y_1l__A_1__pmppp<double>;
#ifdef FivePointAmplitudes_QD_ENABLED 
extern template class remainder_2q3y_1l__A_1__pmppp<FunctionsManager::RHP>;
#endif
