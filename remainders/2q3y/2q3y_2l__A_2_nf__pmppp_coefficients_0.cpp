#include "FivePointAmplitudes_config.h"
#include "prod_pow.h"
#include "Utilities.h"
#include <array>
#ifdef FivePointAmplitudes_QD_ENABLED
#include <qd/qd_real.h>
#endif

#include <Kin.h>

namespace FivePointAmplitudes {


template <typename T> std::array<T,6> eval_coef_2q3y_2l__A_2_nf__pmppp_0(const std::array<T,40>& abb, const std::array<T,34>& q)
{
T z[12];
z[0] = abb[4] * q[8];
z[1] = abb[38] * q[2] * z[0];
z[2] = abb[6] * q[9];
z[3] = q[23] * z[1] * z[2];
z[4] = abb[35] * q[3];
z[5] = abb[5] * q[9];
z[6] = q[20] * z[5];
z[7] = -abb[4] * q[7] * z[4] * z[6];
z[3] = z[3] + z[7];
z[3] = abb[32] * (T(8) / T(3)) * z[3];
z[7] = abb[39] * q[1];
z[8] = abb[5] * q[7];
z[2] = q[19] * z[2] * z[7] * z[8];
z[9] = q[7] * q[16];
z[1] = -abb[6] * z[1] * z[9];
z[1] = z[1] + z[2];
z[1] = abb[37] * (T(8) / T(3)) * z[1];
z[2] = q[17] * z[0];
z[4] = -z[2] * z[4] * z[8];
z[8] = abb[6] * q[8];
z[10] = q[22] * z[8];
z[7] = z[5] * z[7] * z[10];
z[4] = z[4] + z[7];
z[4] = abb[34] * (T(8) / T(3)) * z[4];
z[7] = -q[9] * z[10];
z[5] = -q[7] * q[19] * z[5];
z[5] = z[5] + z[7];
z[5] = abb[39] * z[5];
z[7] = q[6] * q[7];
z[10] = abb[0] * q[5] * q[8];
z[11] = -prod_pow(abb[4], 2) * q[1] * z[7] * z[10];
z[5] = z[5] + z[11];
z[5] = abb[31] * (T(8) / T(3)) * z[5];
z[2] = z[2] + z[6];
z[2] = abb[35] * q[7] * z[2];
z[6] = q[4] * q[9];
z[10] = -prod_pow(abb[6], 2) * q[3] * z[6] * z[10];
z[2] = z[2] + z[10];
z[2] = abb[36] * (T(8) / T(3)) * z[2];
z[0] = z[0] * z[9];
z[8] = -q[9] * q[23] * z[8];
z[0] = z[0] + z[8];
z[0] = abb[38] * z[0];
z[6] = abb[0] * prod_pow(abb[5], 2) * q[2] * z[6] * z[7];
z[0] = z[0] + z[6];
z[0] = abb[33] * (T(8) / T(3)) * z[0];
return {z[3], z[1], z[4], z[5], z[2], z[0]};
}



template std::array<complex<double>,6> eval_coef_2q3y_2l__A_2_nf__pmppp_0(const std::array<complex<double>,40>& abb, const std::array<complex<double>,34>& q);
#ifdef FivePointAmplitudes_QD_ENABLED 
template std::array<complex<dd_real>,6> eval_coef_2q3y_2l__A_2_nf__pmppp_0(const std::array<complex<dd_real>,40>& abb, const std::array<complex<dd_real>,34>& q);
#endif

}
