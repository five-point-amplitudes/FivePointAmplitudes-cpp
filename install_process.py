#!/usr/bin/env python3

import sys
import zipfile
import urllib.request

from pathlib import Path
# from tqdm import tqdm
from io import BytesIO

urls = {
    '3j' : 'https://drive.switch.ch/index.php/s/zQEVjl67BRN8ijc/download', 
    'Vjj' : 'https://drive.switch.ch/index.php/s/pfiz6R82OAXycZW/download'
}


if len(sys.argv) != 3:
    print("Usage: python script_name.py <directory_path> <file_name>")
    sys.exit(1)

data_dir = Path(sys.argv[1])
process = sys.argv[2]
process_file = data_dir/('.'+process+'_installed')

archive_file = data_dir/'downloaded_file.zip'


data_dir.mkdir(parents=True, exist_ok=True)

try:
    if not process_file.is_file() :
        print("Downloading process", process)
        

        response = urllib.request.urlopen(urls[process])

        if response.getcode() == 200:
            total_size = int(response.headers['Content-Length'].strip())
            downloaded_size = 0

            # with tqdm(total=total_size, unit='B', unit_scale=True, unit_divisor=1024, desc="Downloading", dynamic_ncols=True) as pbar:
                # def reporthook(chunk, chunk_size, total_size):
                    # global downloaded_size
                    # downloaded_size += chunk_size
                    # pbar.update(chunk_size)

                # urllib.request.urlretrieve(urls[process], filename=archive_file, reporthook=reporthook)

            urllib.request.urlretrieve(urls[process], filename=archive_file)


            print("\textracting...")
            with zipfile.ZipFile(archive_file, 'r') as zip_ref:
                zip_ref.extractall(data_dir)

            archive_file.unlink()
            process_file.touch()
            print("\tdone")

        else:
            print("Could not download from ", urls[process], file=sys.stderr)
            sys.exit(1)
    else:
        print("Process", process, "already installed")

except Exception as e: 
    print("\tError: ", ' '.join(e.args), file=sys.stderr)
    sys.exit(1)

