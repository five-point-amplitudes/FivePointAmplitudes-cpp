#include "MathList.h"
#include "RemainderEvaluatorVM.h"
#include "Spinor.h"
#include "Timing.h"
#include "Utilities.h"
#include "VirtualMachine.h"
#include "catch.hpp"


#include <Kin.h>
#include "FunctionsManager.h"
#include "Kinematics.h"

#include "remainders/Vjj/functions.hpp"

#include "utilities.hpp"

#include "Parameters.h"

#include "ME2/h2__g_g__bb_b_eb_e.hpp"
#include "ME2/h2__q_qb__bb_b_eb_e.hpp"


//#include <dbg.h>
#include <cmath>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>



using ::PentagonFunctions::stof;
using namespace FivePointAmplitudes;
using namespace ::FivePointAmplitudes::Vjj;

using T = double;
using C = std::complex<T>;

static const mom_conf<T> mc = {{
        {stof<T>("-1.547843350404465913384627373230021512490359482045328048605385476723365048997890367083923854137625"),stof<T>("0.170606317783005262530418699838904611635997862746868979012283089933254302252118398150648840081459"),stof<T>("1.249321854631455859474719612993553455287228443736914570719868296361625992869843388032310403746115"),stof<T>("-0.897723468148218761157431977880753422379335561770435991599901955389876954398844045796133111485775")},
            {stof<T>("-2.028158032459967161814294145623065084956620513370652173432789863067039349916819449774110253529433"),stof<T>("0.201344686003876801802311056831318154847021350535546654238206892174347138093637659786263798471953"),stof<T>("1.474413842390593902961805376796266803210471307071325893240412674977502664780181288198624665975723"),stof<T>("1.378038150203720009587098750273796994845596593095760116427306341733551255317773128486319510877583")},
            {stof<T>("1.716617118772343700009342454812543201245371275809472205119779038593411035515251516039765865149305"),stof<T>("-0.670063380133598004470410077122799702031289207943541340882000398522401479786816674214142762161674"),stof<T>("-1.234655719696509112136241767856891986014172474533593197552309732987053630443997019728614062708599"),stof<T>("-0.98661778459892725460094332540199829286685673931218487290565754335608286681600437364552028096801")},
            {stof<T>("0.824997803458607854638766304762458020461777978378894871574938165211207528919202865493604292965783"),stof<T>("-0.233694126241285291267777319267363722469887810540635020709275451315743025098806274593033143745944"),stof<T>("-0.780622041979255851423601568594990952416678641955990316619732321839787108122239649784011726911358"),stof<T>("0.12898704837303194490943117378097845862560263944111762356933798372927823544948620589263330779443")},
            {stof<T>("0.940577624481963440307716244505708713516427034989858213884914897879472366398908080464332723099415"),stof<T>("0.582003840264901929789865335854797010708878591898458363027636733638571557173912241625926571725177"),stof<T>("-0.629743236352375659805373918319225575807061324169900003325902129006939668208490308858713314880448"),stof<T>("0.386498711332231841126887524893097518625338322991033272061422004281034505499700245790246854421018")},
            {stof<T>("0.093808836151518080243096514772376662223403706237754931458543238106313468081347354860331226448418"),stof<T>("-0.050197337676900698384407696134856352690720786696697634686850865908028492634045350755663304370715"),stof<T>("-0.078714698993909139071307735018711744259787310148756946462336787505348250875297697859595965221746"),stof<T>("-0.009182657161837779865042145665121256850345254445290147552506830997904175052111160727546280638917")}
    }};

TEST_CASE("gg -> Z(ee) bb"){

    EW_Input_Parameters params;

    params.enable_complex_mass_scheme = 0;

    params.M_W = 80.419;
    params.G_W = 2.06;

    params.M_Z = 91.188;
    params.G_Z = 2.49;

    using TC = add_complex_t<T>;

    //dbg::g_floating_point_format.precision(std::numeric_limits<T>::digits10);


    T cw = params.M_W/params.M_Z;

    using namespace Vjj;

    TC propf = get_V_over_photon_propagtor<T>(params.M_Z, params.G_Z, mc.s(5,6));


    T Q_b = T{-1}/T{3};

    struct Contribution {
        std::string v_L;
        std::string v_q;
        std::string hel;
        Permutation perm;
    };

    const std::vector contributions_e_L = {
        Contribution{"e_L", "q_L", "gluonspp", Permutation{3,2,1,4,5,6}}, //  ++ +- +-
        Contribution{"e_R", "q_R", "gluonspp", Permutation{4,1,2,3,6,5}}, //  ++ +- +-
        Contribution{"e_L", "q_L", "gluonsmp", Permutation{3,2,1,4,5,6}}, //  +- +- +-
        Contribution{"e_L", "q_L", "gluonspm", Permutation{3,2,1,4,5,6}}, //  -+ +- +-

        // 1<->2 gluons 
        Contribution{"e_L", "q_L", "gluonspp", Permutation{3,1,2,4,5,6}}, //  ++ +- +-
        Contribution{"e_R", "q_R", "gluonspp", Permutation{4,2,1,3,6,5}}, //  ++ +- +-
        Contribution{"e_L", "q_L", "gluonsmp", Permutation{3,1,2,4,5,6}}, //  +- +- +-
        Contribution{"e_L", "q_L", "gluonspm", Permutation{3,1,2,4,5,6}}, //  -+ +- +-
    };

    SECTION("born") {
        T result{};

        for (auto a : contributions_e_L) {
            auto sq = [](auto x) {
                using std::conj;
                return (conj(x) * x).real();
            };

            auto add_contributions = [&](const auto& mc, auto f_l, auto f_q) {
                auto mc_perm = mc.permute(a.perm);

                result += sq((-Q_b + get_v_coupling(f_l(a.v_L), Q_b, cw) * get_v_coupling(f_q(a.v_q), Q_b, cw) * propf)  *  get_tree("Vjj_1l_" + a.hel + "0", mc_perm) );
            };

            add_contributions(mc, id, id);
            add_contributions(mc.parity_conjugate(), swap_v_L, swap_v_q);
            add_contributions(mc.permute({1, 2, 3, 4, 6, 5}), swap_v_L, id);
            add_contributions(mc.permute({1, 2, 3, 4, 6, 5}).parity_conjugate(), id, swap_v_q);
        }

        using std::pow;

        // target produced from BlackHat_Massive
        T target = stof<T>("140782316.2312624");
        target /= (pow(4*constants::pi<T>,4)*27);

        CHECK(check(result, target, 14.));
    }

    SECTION("NNLO") {
        using Tcoeffs = dd_real;

        START_TIMER(constuct_gluons);
        auto h2 = h2__g_g__bb_b_eb_e<T, Tcoeffs>(params, Q_b);
        STOP_TIMER(constuct_gluons);

        START_TIMER(eval_gluons);
        auto result = h2(mc,1);
        STOP_TIMER(eval_gluons);

        // target produced from evaluating analytic remainders in Mathematica
        // after validating 1-loop ME2 (at the level of renormalized amplitudes squared) vs BlackHat_Massive
        std::vector<T> targets = {stof<T>("134.46855604748668884317"), stof<T>("-22.661200245272453154942"), stof<T>("-1.23196698476157851992195349755")};
        // this is without 1-loop squared
        //std::vector<T> targets = {stof<T>("128.21690662446932831665"), stof<T>("-26.156200311135750868399"), stof<T>("-2.37129261033837270587661272975")};

        for (size_t i = 0; i <= 2; ++i) {
            CHECK(check(result[2][i], targets[i], 12.)); 
        }

        targets = {stof<T>("1.4294702913450697779288787956"), stof<T>("0.3478082268608288921379487242198")};
        for (size_t i = 0; i <= 1; ++i) {
            CHECK(check(result[1][i], targets[i], 12.)); 
        }
    }

}


TEST_CASE("uu -> Z(ee) bb"){

    EW_Input_Parameters params;

    params.enable_complex_mass_scheme = 0;

    params.M_W = 80.419;
    params.G_W = 2.06;

    params.M_Z = 91.188;
    params.G_Z = 2.49;

    using TC = add_complex_t<T>;

    //dbg::g_floating_point_format.precision(std::numeric_limits<T>::digits10);


    T cw = params.M_W/params.M_Z;

    using namespace Vjj;

    TC propf = get_V_over_photon_propagtor<T>(params.M_Z, params.G_Z, mc.s(5,6));


    T Q_b = T{-1}/T{3};

    T Q_q = T{2}/T{3};

    struct Contribution {
        T Q;
        std::string v_L;
        std::string v_q;
        std::string hel;
        Permutation perm;
        bool P;

        Contribution swap_v_L() const {
            return {Q, ::FivePointAmplitudes::Vjj::swap_v_L(v_L), v_q, hel, perm, P};
        }
    };


    using std::pair;

    const std::vector contributions_e_L = {
        pair{
            Contribution{Q_b, "e_L", "q_L", "quarksmp", {3,2,1,4,5,6}, false}, 
            Contribution{Q_q, "e_L", "q_L", "quarksmp", {1,4,3,2,5,6}, false}
        },
        pair{
            Contribution{Q_b, "e_L", "q_L", "quarkspm", {3,2,1,4,5,6}, false}, 
            Contribution{Q_q, "e_L", "q_R", "quarkspm", {1,4,3,2,6,5}, true}, 
        },
        pair{
            Contribution{Q_b, "e_L", "q_R", "quarkspm", {3,2,1,4,6,5}, true}, 
            Contribution{Q_q, "e_L", "q_L", "quarkspm", {1,4,3,2,5,6}, false}, 
        },
        pair{
            Contribution{Q_b, "e_L", "q_R", "quarksmp", {3,2,1,4,6,5}, true}, 
            Contribution{Q_q, "e_L", "q_R", "quarksmp", {1,4,3,2,6,5}, true}
        },
    };

    SECTION("born") {
        T result{};

        for (auto aa : contributions_e_L) {
            auto sq = [](auto x) {
                using std::conj;
                return (conj(x) * x).real();
            };

            auto add_contributions = [&](const auto& mc, const Contribution& a) {
                auto mc_perm = mc.permute(a.perm);
                auto tree = get_tree("Vjj_1l_" + a.hel + "0", mc_perm);
                  /*
                   * This implements parity conjugation *correctly*.
                   * NOTE: it is NOT correct to evaluate the trees on a parity-conjugated point in this case,
                   *     because this generates a different little-group scaling. 
                   * The latter would still be correct to do in the case where only squared trees appear though.
                   */
                if (a.P) tree = -conj(tree);
                return (-a.Q + get_v_coupling(a.v_L, a.Q, cw) * get_v_coupling(a.v_q, a.Q, cw) * propf)  *  tree;
            };

            result += sq(add_contributions(mc, aa.first) + add_contributions(mc, aa.second));

            result += sq(add_contributions(mc.permute({1, 2, 3, 4, 6, 5}), aa.first.swap_v_L()) +
                         add_contributions(mc.permute({1, 2, 3, 4, 6, 5}), aa.second.swap_v_L()));
        }

        using std::pow;

        // target produced from BlackHat_Massive
        T target = stof<T>("56267393.67678506");
        target /= (pow(4*constants::pi<T>,4)*9);

        CHECK(check(result, target, 12.));
    }

    SECTION("NNLO") {
        using Tcoeffs = dd_real;

        START_TIMER(construct_quarks);
        auto h2 = h2__q_qb__bb_b_eb_e<T, Tcoeffs>(params, Q_b, Q_q);
        STOP_TIMER(construct_quarks);


        START_TIMER(eval_quarks);

        auto result = h2(mc,1);

        STOP_TIMER(eval_quarks);

        // target produced from evaluating analytic remainders in Mathematica, after validating 1-loop ME2 vs BlackHat_Massive
        std::vector<T> targets = {stof<T>("338.19570482637691854046"), stof<T>("-66.56139499311595521848844"), stof<T>("-0.70671749012276815736348500664")};
        // this is without 1-loop squared
        //std::vector<T> targets = {stof<T>("278.80535928737480413059"), stof<T>("-61.51322511548961762886518"), stof<T>("-6.20913380654948048286054072190")};

        for (size_t i = 0; i <= 2; ++i) {
            CHECK(check(result[2][i], targets[i], 12.)); 
        }

        targets = {stof<T>("14.0261890433339991299443787850"), stof<T>("-2.107766689378694131265888065411")};

        for (size_t i = 0; i <= 1; ++i) {
            CHECK(check(result[1][i], targets[i], 12.)); 
        }
    }
}
