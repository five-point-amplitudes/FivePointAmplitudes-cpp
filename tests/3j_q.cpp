#include "Utilities.h"
#include "catch.hpp"
/**
 * All targets for these tests were generated from this library itself, so what is checked is only the fact the the evaluation has not changed.
 */


#include <Kin.h>
#include "FunctionsManager.h"
#include "Kinematics.h"

#include "utilities.hpp"

#include "FiniteRenormalizationOperator.h"
#include "ME2/H2Evaluator.h"


TEST_CASE("2q3g H2","[2q3g H2]") {

    using namespace FivePointAmplitudes;
    using namespace FivePointAmplitudes::jjj;

    using T = double;
    //using C = add_complex_t<T>;

    auto check_results = [&](auto result, auto target) {

        for (size_t i = 0; i < result.size(); ++i) {
            for (size_t j = 0; j < result.at(i).size(); ++j) {
                CHECK(check(result.at(i).at(j), target.at(i).at(j), 10.));
            }
        }
    };

    mom_conf<T> mc = {
#include "3j_test_point.incl"
    };


    {
        std::string process = "u_g__ub_g_g";
        H2EvaluatorM0<T> h2(process);
        // we also check scheme change
        h2.finite_renormalization = FiniteRenormalizationOperator_Add<T>("h2__" + process + ".MS_to_Catani.dat");

        auto result = h2(mc,2);

        std::vector<std::vector<T>> targets = {{4.8930217842440070665765828691447e+06}, {6.4296095184014317996840182735401e+01, -3.2062644393353017493078036002791e+00}, {3.9158873151007561267731559198731e+03, -3.7846948456584413277149777685351e+02, 8.5637768841752459000617779230714e+00}};


        check_results(result, targets);

    }

}

TEST_CASE("4q1g H2","[4q1g H2]") {

    using namespace FivePointAmplitudes;
    using namespace FivePointAmplitudes::jjj;

    using T = double;
    //using C = add_complex_t<T>;

    auto check_results = [&](auto result, auto target) {

        for (size_t i = 0; i < result.size(); ++i) {
            for (size_t j = 0; j < result.at(i).size(); ++j) {
                CHECK(check(result.at(i).at(j), target.at(i).at(j), 10.));
            }
        }
    };

    mom_conf<T> mc = {
#include "3j_test_point.incl"
    };


    {
        std::string process = "u_d__db_ub_g";
        H2EvaluatorM0<T> h2(process);
        // we also check scheme change
        h2.finite_renormalization = FiniteRenormalizationOperator_Add<T>("h2__" + process + ".MS_to_Catani.dat");

        auto result = h2(mc,2);

        // we also check scheme change
        std::vector<std::vector<T>> targets = {{9.0984933769536808843645392208485e+02}, {7.6389728144736737695584740001378e+01, -3.9708574178137801298132179215219e+00}, {4.6447682187807130479270090698373e+03, -4.7431899888570114111330072498947e+02, 1.0947921895275705957115163719818e+01}};

        check_results(result, targets);

    }

    {
        std::string process = "u_g__ub_u_ub";
        H2EvaluatorM0<T> h2(process);
        // we also check scheme change
        h2.finite_renormalization = FiniteRenormalizationOperator_Add<T>("h2__" + process + ".MS_to_Catani.dat");

        auto result = h2(mc,2);

        // we also check scheme change
        std::vector<std::vector<T>> targets = {{2.2685448438768070968053566248973e+04}, {6.4219880514492008412630206273262e+01, -4.4258021914172650012401509996164e+00}, {3.8414596559909382464661064993728e+03, -4.5587991196865469470780552790801e+02, 1.3761916087228959045351278673197e+01}};

        check_results(result, targets);

    }


}

