#include "catch.hpp"

#include "Spinor.h"

#include "utilities.hpp"




TEST_CASE("spinor properties", "[spinor]") {

    using namespace FivePointAmplitudes;
    using R = double;
    using C = complex<R>;

    momentum<C> l1{{(sqrt(complex<R>(R(227) / R(225), 0)), complex<R>(R(7) / R(15), 0), complex<R>(R(-3) / R(15), 0), complex<R>(R(13) / R(15), 0))}};
    momentum<C> l2{{complex<R>(1,1),complex<R>(0,1),complex<R>(1,1),complex<R>(1,0)}};
    momentum<C> p1,p2,p3,p4,p5,p6,p7,p8,p9,lc1,lc2,lc3,lc4;

    // Light-like real
    p1=momentum<C>{{ C(-4.99999999999999889e-01,0.00000000000000000e+00),C(4.22627660143561745e-01,0.00000000000000000e+00),C(2.28978938890502130e-01,0.00000000000000000e+00),C(-1.37675366083252713e-01,0.00000000000000000e+00)}};
    p2=momentum<C>{{ C(-5.00000000000000011e-01,0.00000000000000000e+00),C(-4.22627660143561880e-01,0.00000000000000000e+00),C(-2.28978938890502157e-01,0.00000000000000000e+00),C(1.37675366083252697e-01,0.00000000000000000e+00)}};
    p3=momentum<C>{{ C(2.53439217188832259e-01,0.00000000000000000e+00),C(-2.26174796178327132e-01,0.00000000000000000e+00),C(1.13941843400268384e-01,0.00000000000000000e+00),C(-9.67753612904563091e-03,0.00000000000000000e+00)}};
    p4=momentum<C>{{ C(1.68321474617755927e-01,0.00000000000000000e+00),C(1.23420677126931766e-01,0.00000000000000000e+00),C(-8.52479294094133572e-02,0.00000000000000000e+00),C(7.63691417159653036e-02,0.00000000000000000e+00)}};
    p5=momentum<C>{{ C(1.19339393735042942e-01,0.00000000000000000e+00),C(1.15449136949164015e-01,0.00000000000000000e+00),C(-2.18270607609021701e-02,0.00000000000000000e+00),C(-2.09037578746182323e-02,0.00000000000000000e+00)}};
    p6=momentum<C>{{ C(2.21488645241389927e-01,0.00000000000000000e+00),C(6.49169098596420515e-02,0.00000000000000000e+00),C(2.11705604598264896e-01,0.00000000000000000e+00),C(4.87357844140326642e-03,0.00000000000000000e+00)}};
    p7=momentum<C>{{ C(2.37411269216978844e-01,0.00000000000000000e+00),C(-7.76119277574105651e-02,0.00000000000000000e+00),C(-2.18572457828217726e-01,0.00000000000000000e+00),C(-5.06614261537046917e-02,0.00000000000000000e+00)}};

    // Light-like complex
    lc1=momentum<C>{{ C(2.29062455360077721e-01,2.95993454295056229e-02),C(-3.16769253430750141e-01,1.23351678103839162e-02),C(2.60281861705790752e-02,-1.82950644858825914e-01),C(-9.72164783941438117e-02,-1.58917236449861488e-01)}};
    lc2=momentum<C>{{ C(2.70937544639922168e-01,-2.95993454295056229e-02),C(-1.05858406712811604e-01,-1.23351678103839162e-02),C(-2.55007125061081205e-01,1.82950644858825914e-01),C(2.34891844477396524e-01,1.58917236449861488e-01)}};
    lc3=momentum<C>{{ C(-5.17498327451089920e-01,2.95993454295056229e-02),C(-5.42944049609077408e-01,1.23351678103839162e-02),C(1.39970029570847432e-01,-1.82950644858825914e-01),C(-1.06894014523189458e-01,-1.58917236449861488e-01)}};
    lc4=momentum<C>{{ C(3.49176852833333993e-01,-2.95993454295056229e-02),C(4.19523372482145642e-01,-1.23351678103839162e-02),C(-5.47221001614340747e-02,1.82950644858825914e-01),C(3.05248728072241541e-02,1.58917236449861488e-01)}};

    // Random
    momentum<C> q{{C(-3,0.3),C(0,0.004),C(3,-4),C(6,0)}};
    momentum<C> q2{{C(5,-0.3),C(0,0),C(6.1,-0.123),C(8,3.5)}};
    momentum<C> q3{{C(0,1),C(0,0.004),C(-0.3,-4),C(0,-0.006)}};

    SECTION("Basic operations"){

        la<C> a1(complex<R>(0, 1.54), complex<R>(1.2, -0.85));
        la<C> a2(complex<R>(1, 1), complex<R>(3, 0));
        la<C> a3(complex<R>(-4.7, 0.2), complex<R>(0.1, -7.8));

        lat<C> b1(complex<R>(-3.1, 1.5), complex<R>(1.2, -0.8));
        lat<C> b2(complex<R>(1, 0), complex<R>(3, 0.4));
        lat<C> b3(complex<R>(-2.7, 0.1), complex<R>(0.1, -7.8));


        REQUIRE(check(a2*a1 ,-a1*a2));
        REQUIRE(check(b2*b1 , -b1*b2));
        REQUIRE(check(a1*a1,C{0}));
        REQUIRE(check(b2*b2,C{0}));

        REQUIRE(check(a1*a2 , complex<R>(2.04999999999999982e+00,-4.27000000000000046e+00)));
        REQUIRE(check(b1*b2 , complex<R>(-1.10999999999999996e+01,4.05999999999999961e+00)));

        REQUIRE(check((a1+a2)*a3 , complex<R>(-3.94819999999999993e+01,1.23810000000000002e+01)));
        REQUIRE(check((a1-a3)*a2 , complex<R>(-1.99500000000000028e+01,4.03000000000000025e+00)));
        REQUIRE(check((b1+b2)*b3 , complex<R>(2.27899999999999991e+01,1.50300000000000011e+01)));
        REQUIRE(check((b1-b3)*b2 , complex<R>(-2.85999999999999943e+00,-2.95999999999999996e+00)));
    }

    //SECTION("Self-consistency"){

        //SECTION("Dirac equation"){
            //REQUIRE(is_zero( smatrix<C>(l1)*la<C>(l1) ));
            //REQUIRE(is_zero( smatrix<C>(l1)*lat<C>(l1) ));
            //REQUIRE(is_zero( la<C>(l1) * smatrix<C>(l1)));
            //REQUIRE(is_zero( lat<C>(l1)* smatrix<C>(l1) ));

            //REQUIRE(is_zero( smatrix<C>(l2)*la<C>(l2) ));
            //REQUIRE(is_zero( smatrix<C>(l2)*lat<C>(l2) ));
            //REQUIRE(is_zero( la<C>(l2) * smatrix<C>(l2)));
            //REQUIRE(is_zero( lat<C>(l2)* smatrix<C>(l2) ));
        //}

        //SECTION("Momentum from spinors"){
            //REQUIRE_FL( LvBA(lat<C>(l1),la<C>(l1)) , l1 );
            ////REQUIRE_FL( LvBA<C,4>(lat<C>(l1r),la<C>(l1)) , l1 );
            //REQUIRE_FL( LvBA(lat<C>(l2),la<C>(l2)) , l2 );
        //}

        //// this is just our convention, can be changed in principle
        //SECTION("Spinors of -l (a convention)"){
            //REQUIRE_FL( la<C>(lc1) , la<C>(-lc1) );
            //REQUIRE_FL( lat<C>(lc1) , -lat<C>(-lc1) );
        //}

        //SECTION(" slashed(p)*slahsed(p) == p^2 * I "){
            //smatrix<C> q2s(q2);
            //la<C> la2(lc2);
            //lat<C> lat2(lc2);
            //REQUIRE_FL(q2s*(q2s*la2)/(q2*q2) , la2 );
            //REQUIRE_FL(q2s*(q2s*lat2)/(q2*q2) , lat2 );
            //REQUIRE_FL(la2*q2s*q2s/(q2*q2) , la2 );
            //REQUIRE_FL(lat2*q2s*q2s/(q2*q2) , lat2 );
        //}
    //}


    SECTION("Spinor products"){

        SECTION("associativity"){
            REQUIRE(check(la<C>(lc1)*(smatrix<C>(q)*lat<C>(lc2)),(la<C>(lc1)*smatrix<C>(q))*lat<C>(lc2)));
            REQUIRE(check(lat<C>(lc1)*(smatrix<C>(q2)*la<C>(lc2)),(lat<C>(lc1)*smatrix<C>(q2))*la<C>(lc2)));
            REQUIRE(check(lat<C>(lc1)*(smatrix<C>(q)*(smatrix<C>(lc4)*lat<C>(lc2))),(lat<C>(lc1)*smatrix<C>(q))*(smatrix<C>(lc4)*lat<C>(lc2))));
            REQUIRE(check(la<C>(lc1)*(smatrix<C>(q)*(smatrix<C>(q2)*la<C>(lc2))),(la<C>(lc1)*smatrix<C>(q))*(smatrix<C>(q2)*la<C>(lc2))));
        }

#define REQUIRE_FL(X,Y) REQUIRE(check((X),(Y)));

        SECTION("chain flip"){
            REQUIRE_FL(
                    la<C>(lc1)*smatrix<C>(q)*lat<C>(lc2),
                    (lat<C>(lc2)*smatrix<C>(q))*la<C>(lc1)
                    );

            REQUIRE_FL(
                    lat<C>(lc3)*smatrix<C>(q)*la<C>(lc2),
                    la<C>(lc2)*smatrix<C>(q)*lat<C>(lc3)
                    );

            REQUIRE_FL(
                    la<C>(lc1)*smatrix<C>(q)*smatrix<C>(lc3)*la<C>(lc2),
                    -la<C>(lc2)*smatrix<C>(lc3)*smatrix<C>(q)*la<C>(lc1)
                    );
            REQUIRE_FL(
                    lat<C>(lc4)*smatrix<C>(q)*smatrix<C>(lc3)*lat<C>(lc2),
                    -lat<C>(lc2)*smatrix<C>(lc3)*smatrix<C>(q)*lat<C>(lc4)
                    );
        }


        SECTION("smaple values"){
            REQUIRE_FL(spaa(p1,p3), C(1.04513985873959825e-01,-3.25861743096249545e-01));
            REQUIRE_FL(spbb(p1,p3), C(1.04513985873959769e-01,3.25861743096249490e-01));
            REQUIRE_FL(spaa(lc1,p2), C(-4.73171487164170568e-01,-2.56363497380385941e-01));
            REQUIRE_FL(spaa(lc3,lc1), C(4.00523218045437168e-01,3.66948875457201451e-03));
            REQUIRE_FL(spbb(p3,lc4), C(6.97181287721299725e-01,2.64938579942937846e-01));
            REQUIRE_FL(spbb(lc3,lc4), C(-2.20629630237297425e-01,7.50538949048375204e-02));

            REQUIRE_FL(spab(p1,p2,p3), C(-5.70533035562600266e-01,-2.53498404043103975e-01));
            REQUIRE_FL(spab(p2,q,p3), C(-1.76575901681075109e+00,2.18392987309527564e+00));
            REQUIRE_FL(spab(lc1,q2,p3), C(2.68316795268646047e-02,1.61670888812717850e+00));
            REQUIRE_FL(spab(lc2,q,lc3), C(8.97791706979455384e+00,6.49187494461033587e+00));

            REQUIRE_FL(spaa(p1,q2,q3,p2), C(-5.52579417335576650e+00,-5.86172219683514228e+01));
            REQUIRE_FL(spbb(p1,q2,q3,p2), C(-6.33472797056222969e+00,3.67366291620193053e+01));
            REQUIRE_FL(spaa(lc3,q2,q3,lc2), C(-1.47610408607138801e+01,1.15801736812680716e+01));
            REQUIRE_FL(spbb(lc4,q2,q3,lc1), C(-7.60008982458113707e+01,-4.70774942873785278e+01));

        }

    }
}
