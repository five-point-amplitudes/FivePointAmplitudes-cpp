#include "Utilities.h"
#include "catch.hpp"
/**
 * All targets for these tests were generated from this library itself, so what is checked is only the fact the the evaluation has not changed.
 */


#include <Kin.h>
#include "FunctionsManager.h"
#include "Kinematics.h"

#include "utilities.hpp"

#include "FiniteRenormalizationOperator.h"
#include "ME2/H2Evaluator.h"

TEST_CASE("5g H1 and H2","[5g H2]") {

    using namespace FivePointAmplitudes;
    using namespace FivePointAmplitudes::jjj;

    using T = double;
    //using C = add_complex_t<T>;

    auto check_results = [&](auto result, auto target) {

        for (size_t i = 0; i < result.size(); ++i) {
            for (size_t j = 0; j < result.at(i).size(); ++j) {
                CHECK(check(result.at(i).at(j), target.at(i).at(j), 10.));
            }
        }
    };

    mom_conf<T> mc = {
#include "3j_test_point.incl"
    };

    // leading color in Catani scheme, this is what was computed previously
    {
        H2EvaluatorM0<T> h2("g_g__g_g_g.lc");
        h2.finite_renormalization = FiniteRenormalizationOperator_Add<T>("h2__g_g__g_g_g.lc.MS_to_Catani.dat");


        auto result = h2(mc,1);

        std::vector<std::vector<T>> targets = {{3.7427751006192429854772354363417e+07}, {2.4202881349250672801759577774697e+01, 9.5815223541138717005161596097326e-02}, {1.0450765405129054876022595859993e+03, -4.3587219347371331626115351368946e+01, 1.5019308209849992091593010171718e-01}};

        check_results(result, targets);

    }


    {
        H2EvaluatorM0<T> h2("g_g__g_g_g");

        auto result = h2(mc,2);

        std::vector<std::vector<T>> targets = {{3.3269112005504382093130981656354e+07}, {-6.3979398430099944966452493069359e+01, 3.4284940673093344691707931410306e+00}, {2.0368331136670993157668429629685e+03, -1.5034843192059594816372331651993e+02, 3.0423286694038724471373522779753e+00}};

        check_results(result, targets);

    }

}
