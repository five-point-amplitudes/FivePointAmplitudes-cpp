#pragma once

#include <limits>
#include <type_traits>
#include <iostream>
#include <iomanip>
#include <complex>

#ifdef Li2pp_QD_ENABLED 
#include "qd_suppl.h"
#endif // Li2pp_QD_ENABLED
#ifdef Li2pp_GMP_ENABLED 
#include "gmp_r.h"
#endif // Li2pp_GMP_ENABLED

#include "Utilities.h"



namespace  FivePointAmplitudes {


template <typename T, typename TR = remove_complex_t<T>> TR compute_accuracy(const T& x, const T& target) {
    using std::abs;
    using std::log10;
    if (abs(x - target) == TR(0)) return std::numeric_limits<TR>::digits10;

    if (abs(target) > TR(10)*std::numeric_limits<TR>::epsilon()) {
        return -log10(abs(x - target) / abs(target));
    } else
        return -log10(abs(x - target));
}

template <typename T, typename TR = remove_complex_t<T>, typename... Ts> bool check(const T& x, const T& target, double tol = 12.5) {
    auto acc = compute_accuracy(x, target);
    if (acc >= TR(tol)) return true;

    std::cerr << std::setprecision(std::numeric_limits<TR>::digits10);

    std::cerr << x << "  !=  " << target << "(target)\n";
    std::cerr << "\t accuracy : " << acc;
    std::cerr << "\t tolerance : " << tol;
    std::cerr << std::endl;

    return false;
}


    
} //  Li2pp
