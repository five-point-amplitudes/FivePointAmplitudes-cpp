# FivePointAmplitudes

`FivePointAmplitudes` is a C++ library for fast numerical evaluation of analytic expressions
for finite remainders of two-loop five-point scattering amplitudes.
The library is meant to provide so-called double-virtual contributions for NNLO QCD cross section computations.


If you use this library in your studies, please cite the publication according to the process, and [PentagonFunctions++]

- D. Chicherin, V. Sotnikov,
_Pentagon Functions for Scattering of Five Massless Particles._,
JHEP 12 (2020) 167, https://doi.org/10.1007/JHEP12(2020)167


## Dependencies

The library relies on [PentagonFunctions++] for numerical evaluation of pentagon functions.
If [PentagonFunctions++] is not available in the system, it will be configured and installed automatically.

For quadruple precision evaluations and to use the automated precision rescue system the [qd] library is required.
Make sure that your [qd] installation is configured with `--enable-shared`
and that it is visible in your terminal environment
(environmental variables `$LD_LIBRARY_PATH`, `$PKG_CONFIG_PATH`).




## Installation

The library uses [meson] build system with [ninja] back-end.
Both can be obtained through the standard python package manager *pip*:

```
pip install --user ninja meson
```


To build the library in directory **build** and install it in the standard configuration into **$HOME/local**, do

```
meson setup build -D prefix=$HOME/local -D enabled-processes=2q3y,3j -D high-precision=true  
cd build
ninja install
```

This will configure the support of quadruple-precision evaluations and install to `$HOME/local`.
To enable the required process, it must be explicitly listed in the `-D enabled-processes` configuration option.
The processes that are not listed will not be compiled.
The process names are given below.

To disable [qd] support, either omit `-D high-precision=true` in the setup command, or set it to false explicitly.


## Tests

A number of tests is provided to verify that the library works as intended.
It is recommended to always run the tests after installation. This can be done with

```
meson test
```

from the build directory.



## Usage and examples

One way to use the library is to write a C++ program which links to it.
The library is shipped with a *pkg-config* file **FivePointAmplitudes.pc**,
so the recommended approach is to use *pkg-config* facilities of your favorite build system.
Make sure that the library installation is visible in your terminal environment
(environmental variables `$LD_LIBRARY_PATH`, `$PKG_CONFIG_PATH`).


### C and Fortran interfaces

Since v4.0 an *experimental* C interface for squared finite remainders is available.
It is intended to be as simple as possible to ensure maximal cross-language interoperability.
See the documentation in [src/ME2/C_interface.h](src/ME2/C_interface.h) for details.

A Fortran module built as a wrapper around the C interface is also available.
See [src/ME2/fortran_interface.f90](src/ME2/C_interface.h) for details.

To get an idea about how to use the C interface, one can take a look at the test program in [tests/C_interface_test.c](tests/C_interface_test.c).



### Examples

The C++ interface of the library is presented by example programs in directory **examples/**.
We show examples of evaluation of

* [helicity finite remainders](examples/example_helicity.cpp)
* [squared helicity/color-summed remainders](examples/example_H2.cpp)

The compiled example programs are put by meson in **build/examples/**.


### Precision control and rescue

In addition to evaluations in fixed double and quadruple precision, 
the library implements a precision rescue system which automatically detects all unstable points
and recomputes them in quadruple precision. 
More details about the detection strategy can be found in the [publication][2102.13609].

The system can be used with any finite remainder by "composing" it with the class [CheckedEvaluator](src/CheckedEvaluator.h).
For example, `H2EvaluatorM0<double>` is the double-precision evaluator for three-jet processes,
and `CheckedEvaluator<H2EvaluatorM0>` is an evaluator which uses the rescue system.

The slowdown due to the usage of this system is usually insignificant ([1][2102.13609], [2][2305.17056]), so it is recommended to always use it.




## Available proccesses

Symmetric "all-outgoing" convention is used in the names of all classes
that implement helicity remainders, as well as squared helicity/color-summed remainders (hard functions).
Particles' momenta are expected to sum to zero, and the particles 1,2 are expected to have negative energies.

In PDG codes and C interface on the other hand incoming particles are not converted into the "all-outgoing" convention.

Unless stated otherwise, evaluation results of all implemented classes do not include color factors (e.g. $`N_c, N_f`$) or coupling constants.
Instead, the returned results are vectors of numbers, corresponding to coefficients of different contributions.
It is left for the user to combine these contributions in the desired way.
The user should consult the corresponding paper to verify the conventions.


The following processes are currently available.


###  Triphoton hadroproduction  

Triphoton hadroproduction is available in full color since v3.0.


Process name: 2q3y

PDG codes:  
* 1 -1 22 22 22


Available functions:
* Helicity remainders correspond to `R(1), R(2,0), R(2,1), R(2,Nf), R(2,Nf-tilde)` defined under eq. (18) of [2305.17056], 
    they are normalized by the factors in eq. (85) of [2305.17056] such that the planar remainders match the definitions from [2010.04681].
* Squared remainders correspond to `H(1), H(2,0), H(2,1), H(2,Nf), H(2,Nf-tilde)` in eq. (20) of [2305.17056].


The C++ interface is available through the class [h2__q_qb__y_y_y](src/ME2/h2__q_qb__y_y_y.hpp).



References:

- S. Abreu, B. Page, E. Pascual, and V. Sotnikov,
_Leading-color two-loop QCD corrections for three-photon production at hadron colliders_,
JHEP 01 (2021) 078, https://doi.org/10.1007/JHEP01(2021)078, [2010.15834]

- S. Abreu, G. De Laurentis, H. Ita,  M. Klinkert, B. Page,  and V. Sotnikov,
_Two-Loop QCD Corrections for Three-Photon Production at Hadron Colliders_,
SciPost Phys. 15 (2023) 157, https://doi.org/10.21468/SciPostPhys.15.4.157, [2305.17056]



### Three-jet production in hadron collisions

Three-jet production is available in the leading color approximation since v2.0.
Five-gluon channel in full color is available since v4.0.
All quark channels in full color are available since v4.1.


The implementation based on [2102.13609] has been superseded since v4.0.
Leading-color only evaluation is still available using the new expressions from [2311.10086],[2311.18752]


Process name: 3j

PDG codes:
* 21 21 21 21 21
* -1 1 21 21 21
* -1 21 -1 21 21
* 21 21 1 -1 21
* -1 1 2 -2 21
* -1 2 2 -1 21
* -1 -2 -2 -1 21
* -1 21 -1 2 -2
* -1 1 1 -1 21
* -1 -1 -1 -1 21
* -1 21 -1 1 -1


Available functions:

* All partial helicity remainders defined in eq. (10) of [2311.10086] and eqs. (21,22) of [2311.18752].
* Hard functions H defined in eqs. (21,22) of [2311.10086] and eqs. (32,33) of [2311.18752],
    here Nc is already substituted numerically, and the return vector corresponds to Nf powers.


The C++ interface is available through the class [H2EvaluatorM0](src/ME2/H2Evaluator.h).


References:

- S. Abreu, F. Febres Cordero, H. Ita, B. Page, and V. Sotnikov,
_Leading-Color Two-Loop QCD Corrections for Three-Jet Production at Hadron Colliders_, 
JHEP 07 (2021) 095, https://doi.org/10.1007/JHEP07(2021)095, [2102.13609]

- G. De Laurentis, H. Ita, M. Kinkert, and V. Sotnikov,
_Double-Virtual NNLO QCD Corrections for Five-Parton Scattering: The Gluon Channel_, 
*to be published*, [2311.10086]

- G. De Laurentis, H. Ita, and V. Sotnikov,
_Double-Virtual NNLO QCD Corrections for Five-Parton Scattering: The Quark Channels_, 
*to be published*, [2311.18752]




**Developers:**

Vasily Sotnikov





------------------

This work is licensed under **GNU GPLv3**

------------------

[2311.18752]: https://arxiv.org/abs/2311.18752
[2010.15834]: https://arxiv.org/abs/2010.15834
[2102.13609]: https://arxiv.org/abs/2102.13609
[2305.17056]: https://arxiv.org/abs/2305.17056
[2311.10086]: https://arxiv.org/abs/2311.10086
[PentagonFunctions++]: https://gitlab.com/pentagon-functions/PentagonFunctions-cpp
[meson]: https://mesonbuild.com/
[ninja]: https://ninja-build.org/
[qd]: http://crd-legacy.lbl.gov/~dhbailey/mpdist/
