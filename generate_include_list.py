#!/usr/bin/env python3

import pathlib
import os

to_compile = os.environ['TO_COMPILE_DIR']

flist = [p for p in pathlib.Path('remainders/'+to_compile).glob('**/*.hpp') if not (p.match('*coefficients.hpp') or p.match('*normalizations.hpp'))]

flist = sorted(['#include "'+str(p)+'"' for p in flist])

flist = '\n'.join(flist)

content = '''\
#pragma once

#include "RemainderEvaluator.h"
#include "SpinorRemainderBase.h"
#include "MathList.h"

#include "remainders/'''+to_compile+'''/normalizations.hpp"

namespace FivePointAmplitudes {

''' + flist + '''

}\
'''

cpplist  = [p for p in pathlib.Path('remainders/'+to_compile).glob('**/*.cpp')]

cpplist = sorted(['\''+str(p) + '\',' for p in cpplist])
cpplist = '\n'.join(cpplist)

mesonbuild = '''\
FivePointAmplitudes_sources += [
''' + cpplist + '''
]
'''

with open('remainders/'+to_compile+'/RemaindersList.h','w') as f :
    f.write(content)

with open('remainders/'+to_compile+'/meson.build','w') as f :
    f.write(mesonbuild)
