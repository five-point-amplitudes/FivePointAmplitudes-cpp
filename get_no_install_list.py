#!/usr/bin/env python3

import pathlib
import os

flist = []


flist += [p.relative_to('src') for p in pathlib.Path('src').glob('**/meson.build')]
flist += [p.relative_to('src') for p in pathlib.Path('src').glob('**/*.cpp')]

# here it is assumed that all data files are inside a subdirectory under 'remainders/'
for path in ['remainders/'] :
    flist += [p.relative_to(p.parent) for p in pathlib.Path(path).glob('**/*.cpp')]
    flist += [p.relative_to(p.parent) for p in pathlib.Path(path).glob('**/*.dat')]
    # flist += [p.relative_to(p.parent) for p in pathlib.Path(path).glob('**/*.py')]
    flist += [p.relative_to(p.parent) for p in pathlib.Path(path).glob('**/meson.build')]

for p in flist :
    print(p)
